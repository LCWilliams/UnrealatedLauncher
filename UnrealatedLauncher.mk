##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=UnrealatedLauncher
ConfigurationName      :=Debug
WorkspacePath          :=/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher
ProjectPath            :=/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=lee
Date                   :=14/10/20
CodeLitePath           :=/home/lee/.codelite
LinkerName             :=/usr/bin/g++-10
SharedObjectLinkerName :=/usr/bin/g++-10 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="UnrealatedLauncher.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            := -pthread  $(shell pkg-config --libs gtkmm-3.0)
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)/usr/include/ $(IncludeSwitch)/usr/include/c++/10/ $(IncludeSwitch)/usr/include/webkitgtk-4.0/ $(IncludeSwitch)/usr/include/webkitgtk-4.0/webkit2 $(IncludeSwitch)/usr/include/libsoup-2.4/ $(IncludeSwitch)/usr/include/curlpp/ $(IncludeSwitch)./external/ $(IncludeSwitch)./include/ $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)stdc++ $(LibrarySwitch)stdc++fs $(LibrarySwitch)systemd $(LibrarySwitch)crypto $(LibrarySwitch)z $(LibrarySwitch)ssl $(LibrarySwitch)git2 $(LibrarySwitch)webkit2gtk-4.0 $(LibrarySwitch)curl $(LibrarySwitch)curlpp 
ArLibs                 :=  "libstdc++" "libstdc++fs" "libsystemd" "libcrypto" "libz" "libssl" "libgit2" "libwebkit2gtk-4.0" "libcurl" "libcurlpp" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)lib/ $(LibraryPathSwitch)/usr/lib/ $(LibraryPathSwitch)/usr/include/c++/10/ $(LibraryPathSwitch)/lib/x86_64-linux-gnu/ $(LibraryPathSwitch)/usr/include/ $(LibraryPathSwitch)/usr/include/libsoup-2.4/ $(LibraryPathSwitch)/usr/lib/x86_64-linux-gnu/ 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/gcc-ar
CXX      := /usr/bin/g++-10
CC       := /usr/bin/gcc-10
CXXFLAGS := -std=c++17 -g -O0 -Wall $(shell pkg-config --cflags gtkmm-3.0) $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_engineData.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_launchOptions.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_market_market_item.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_engineProperties.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_webView.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_printer.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_market_market_epicAPI.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/src_market_launcher_market.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_community_community_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix): src/library/launcher_library.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_launcher_library.cpp$(DependSuffix) -MM src/library/launcher_library.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/launcher_library.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_launcher_library.cpp$(PreprocessSuffix): src/library/launcher_library.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_launcher_library.cpp$(PreprocessSuffix) src/library/launcher_library.cpp

$(IntermediateDirectory)/src_library_library_engineData.cpp$(ObjectSuffix): src/library/library_engineData.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_engineData.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_engineData.cpp$(DependSuffix) -MM src/library/library_engineData.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_engineData.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_engineData.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_engineData.cpp$(PreprocessSuffix): src/library/library_engineData.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_engineData.cpp$(PreprocessSuffix) src/library/library_engineData.cpp

$(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(ObjectSuffix): src/library/library_addEngineGUI.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(DependSuffix) -MM src/library/library_addEngineGUI.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_addEngineGUI.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(PreprocessSuffix): src/library/library_addEngineGUI.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_addEngineGUI.cpp$(PreprocessSuffix) src/library/library_addEngineGUI.cpp

$(IntermediateDirectory)/src_library_library_launchOptions.cpp$(ObjectSuffix): src/library/library_launchOptions.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_launchOptions.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_launchOptions.cpp$(DependSuffix) -MM src/library/library_launchOptions.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_launchOptions.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_launchOptions.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_launchOptions.cpp$(PreprocessSuffix): src/library/library_launchOptions.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_launchOptions.cpp$(PreprocessSuffix) src/library/library_launchOptions.cpp

$(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(ObjectSuffix): src/library/library_launchOptionButton.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(DependSuffix) -MM src/library/library_launchOptionButton.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_launchOptionButton.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(PreprocessSuffix): src/library/library_launchOptionButton.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_launchOptionButton.cpp$(PreprocessSuffix) src/library/library_launchOptionButton.cpp

$(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(ObjectSuffix): src/miniGUI/miniGUI_dialogueControls.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(DependSuffix) -MM src/miniGUI/miniGUI_dialogueControls.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/miniGUI/miniGUI_dialogueControls.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(PreprocessSuffix): src/miniGUI/miniGUI_dialogueControls.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_miniGUI_miniGUI_dialogueControls.cpp$(PreprocessSuffix) src/miniGUI/miniGUI_dialogueControls.cpp

$(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(ObjectSuffix): src/miniGUI/miniGUI_destructiveConfirmationButton.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(DependSuffix) -MM src/miniGUI/miniGUI_destructiveConfirmationButton.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/miniGUI/miniGUI_destructiveConfirmationButton.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(PreprocessSuffix): src/miniGUI/miniGUI_destructiveConfirmationButton.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_miniGUI_miniGUI_destructiveConfirmationButton.cpp$(PreprocessSuffix) src/miniGUI/miniGUI_destructiveConfirmationButton.cpp

$(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(ObjectSuffix): src/miniGUI/miniGUI_launchEngineBtn.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(DependSuffix) -MM src/miniGUI/miniGUI_launchEngineBtn.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/miniGUI/miniGUI_launchEngineBtn.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(PreprocessSuffix): src/miniGUI/miniGUI_launchEngineBtn.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_miniGUI_miniGUI_launchEngineBtn.cpp$(PreprocessSuffix) src/miniGUI/miniGUI_launchEngineBtn.cpp

$(IntermediateDirectory)/src_market_market_item.cpp$(ObjectSuffix): src/market/market_item.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_market_market_item.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_market_market_item.cpp$(DependSuffix) -MM src/market/market_item.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/market/market_item.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_market_market_item.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_market_market_item.cpp$(PreprocessSuffix): src/market/market_item.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_market_market_item.cpp$(PreprocessSuffix) src/market/market_item.cpp

$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix): src/launcher_main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_main.cpp$(DependSuffix) -MM src/launcher_main.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_main.cpp$(PreprocessSuffix): src/launcher_main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_main.cpp$(PreprocessSuffix) src/launcher_main.cpp

$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix): src/launcher_settings.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_settings.cpp$(DependSuffix) -MM src/launcher_settings.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_settings.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_settings.cpp$(PreprocessSuffix): src/launcher_settings.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_settings.cpp$(PreprocessSuffix) src/launcher_settings.cpp

$(IntermediateDirectory)/src_library_library_engineProperties.cpp$(ObjectSuffix): src/library/library_engineProperties.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_engineProperties.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_engineProperties.cpp$(DependSuffix) -MM src/library/library_engineProperties.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_engineProperties.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_engineProperties.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_engineProperties.cpp$(PreprocessSuffix): src/library/library_engineProperties.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_engineProperties.cpp$(PreprocessSuffix) src/library/library_engineProperties.cpp

$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix): src/library/library_repoManager.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_repoManager.cpp$(DependSuffix) -MM src/library/library_repoManager.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_repoManager.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_repoManager.cpp$(PreprocessSuffix): src/library/library_repoManager.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_repoManager.cpp$(PreprocessSuffix) src/library/library_repoManager.cpp

$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix): src/library/library_engineBlock.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(DependSuffix) -MM src/library/library_engineBlock.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_engineBlock.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(PreprocessSuffix): src/library/library_engineBlock.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_engineBlock.cpp$(PreprocessSuffix) src/library/library_engineBlock.cpp

$(IntermediateDirectory)/src_launcher_webView.cpp$(ObjectSuffix): src/launcher_webView.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_webView.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_webView.cpp$(DependSuffix) -MM src/launcher_webView.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_webView.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_webView.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_webView.cpp$(PreprocessSuffix): src/launcher_webView.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_webView.cpp$(PreprocessSuffix) src/launcher_webView.cpp

$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix): src/launcher_mainWindow.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(DependSuffix) -MM src/launcher_mainWindow.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_mainWindow.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(PreprocessSuffix): src/launcher_mainWindow.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_mainWindow.cpp$(PreprocessSuffix) src/launcher_mainWindow.cpp

$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix): src/launcher_accountSetup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(DependSuffix) -MM src/launcher_accountSetup.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_accountSetup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(PreprocessSuffix): src/launcher_accountSetup.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_accountSetup.cpp$(PreprocessSuffix) src/launcher_accountSetup.cpp

$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix): src/launcher_settingsGUI.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(DependSuffix) -MM src/launcher_settingsGUI.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_settingsGUI.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(PreprocessSuffix): src/launcher_settingsGUI.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(PreprocessSuffix) src/launcher_settingsGUI.cpp

$(IntermediateDirectory)/src_launcher_printer.cpp$(ObjectSuffix): src/launcher_printer.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_printer.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_printer.cpp$(DependSuffix) -MM src/launcher_printer.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_printer.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_printer.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_printer.cpp$(PreprocessSuffix): src/launcher_printer.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_printer.cpp$(PreprocessSuffix) src/launcher_printer.cpp

$(IntermediateDirectory)/src_market_market_epicAPI.cpp$(ObjectSuffix): src/market/market_epicAPI.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_market_market_epicAPI.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_market_market_epicAPI.cpp$(DependSuffix) -MM src/market/market_epicAPI.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/market/market_epicAPI.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_market_market_epicAPI.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_market_market_epicAPI.cpp$(PreprocessSuffix): src/market/market_epicAPI.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_market_market_epicAPI.cpp$(PreprocessSuffix) src/market/market_epicAPI.cpp

$(IntermediateDirectory)/src_market_launcher_market.cpp$(ObjectSuffix): src/market/launcher_market.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_market_launcher_market.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_market_launcher_market.cpp$(DependSuffix) -MM src/market/launcher_market.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/market/launcher_market.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_market_launcher_market.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_market_launcher_market.cpp$(PreprocessSuffix): src/market/launcher_market.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_market_launcher_market.cpp$(PreprocessSuffix) src/market/launcher_market.cpp

$(IntermediateDirectory)/src_community_community_main.cpp$(ObjectSuffix): src/community/community_main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_community_community_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_community_community_main.cpp$(DependSuffix) -MM src/community/community_main.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/community/community_main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_community_community_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_community_community_main.cpp$(PreprocessSuffix): src/community/community_main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_community_community_main.cpp$(PreprocessSuffix) src/community/community_main.cpp

$(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(ObjectSuffix): src/library/library_repoManagerGUI.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(DependSuffix) -MM src/library/library_repoManagerGUI.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_repoManagerGUI.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(PreprocessSuffix): src/library/library_repoManagerGUI.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_repoManagerGUI.cpp$(PreprocessSuffix) src/library/library_repoManagerGUI.cpp

$(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(ObjectSuffix): src/miniGUI/miniGUI_timerButton.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(DependSuffix) -MM src/miniGUI/miniGUI_timerButton.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/miniGUI/miniGUI_timerButton.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(PreprocessSuffix): src/miniGUI/miniGUI_timerButton.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_miniGUI_miniGUI_timerButton.cpp$(PreprocessSuffix) src/miniGUI/miniGUI_timerButton.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


