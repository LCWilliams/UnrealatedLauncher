var searchData=
[
  ['save',['Save',['../classCSimpleIniTempl.html#a5fea5d590edbb5eef694991c7c355915',1,'CSimpleIniTempl::Save(OutputWriter &amp;a_oOutput, bool a_bAddSignature=false) const '],['../classCSimpleIniTempl.html#af944674fb44473ede150a3bcdc103d63',1,'CSimpleIniTempl::Save(std::string &amp;a_sBuffer, bool a_bAddSignature=false) const ']]],
  ['savefile',['SaveFile',['../classCSimpleIniTempl.html#a1449e083d968790ef7479de24edddba0',1,'CSimpleIniTempl::SaveFile(const char *a_pszFile, bool a_bAddSignature=true) const '],['../classCSimpleIniTempl.html#af3f26b331a0f9d7f071d7b4aa8038758',1,'CSimpleIniTempl::SaveFile(FILE *a_pFile, bool a_bAddSignature=false) const ']]],
  ['setboolvalue',['SetBoolValue',['../classCSimpleIniTempl.html#a48ae136fa20c5d7eb7ab0b75342b27cf',1,'CSimpleIniTempl']]],
  ['setdoublevalue',['SetDoubleValue',['../classCSimpleIniTempl.html#af92ba0b8067553ab693c62a370de6534',1,'CSimpleIniTempl']]],
  ['setlongvalue',['SetLongValue',['../classCSimpleIniTempl.html#ab2238be407232e4bba0f1343e4793e4e',1,'CSimpleIniTempl']]],
  ['setmultikey',['SetMultiKey',['../classCSimpleIniTempl.html#ac3cfaf072a64f960bdcb7ddf2edc52b6',1,'CSimpleIniTempl']]],
  ['setmultiline',['SetMultiLine',['../classCSimpleIniTempl.html#aa7214b76600790053a5c715e9730aab0',1,'CSimpleIniTempl']]],
  ['setspaces',['SetSpaces',['../classCSimpleIniTempl.html#ae3c0eae2dcd84a42c99bb86ae103662c',1,'CSimpleIniTempl']]],
  ['setunicode',['SetUnicode',['../classCSimpleIniTempl.html#aa9a15a66de893571014f661f89cb4d4b',1,'CSimpleIniTempl']]],
  ['setvalue',['SetValue',['../classCSimpleIniTempl.html#aa2014a3dc8fdd638316cf1d3611796ab',1,'CSimpleIniTempl']]],
  ['sizefromstore',['SizeFromStore',['../classSI__ConvertA.html#a30ce0eee2556184d41130311d3c8cc84',1,'SI_ConvertA::SizeFromStore()'],['../classSI__ConvertW.html#aeb7cb9953fd07917c9a049c441add51e',1,'SI_ConvertW::SizeFromStore()']]],
  ['sizetostore',['SizeToStore',['../classSI__ConvertA.html#a39e7a8c49712c295b24ff2ae788c01c5',1,'SI_ConvertA::SizeToStore()'],['../classSI__ConvertW.html#a7619dba0ecb1b6ec28b4d16a89ed89c0',1,'SI_ConvertW::SizeToStore()']]]
];
