#ifndef LAUNCHER_MINIGUI
#define LAUNCHER_MINIGUI

#include <gtkmm-3.0/gtkmm.h>
#include "include/launcher_settings.h"
#include "include/launcher_utilities.h"

/* MINI GUI ELEMENTS TO PREVENT HAVING TO REMAKE THEM A GAZILLION TIMES OVER. */
namespace UnrealatedLauncher{
	/* SUB WINDOW CONTROLS:
	* Provides a buttonbox containing Close/Default/Confirm buttons.
	* @param p_hasDefaults: If true, a default button is shown.
	* @var v_signalConfirm: Connect this to a function for confirmation.
	* @var v_signalDefaults: Connect this to a function that resets to default.
	* @var v_signalCancel: Connect this to a function that cancels the window. */ 
	
	class DialogueControls : public Gtk::ButtonBox{
	public:
		DialogueControls(bool p_hasDefaults);
		virtual ~DialogueControls();
		
		// Signal emitted when Confirm is clicked.
		sigc::signal<void> v_signalConfirm;
		// Signal emitted when Defaults is clicked.
		sigc::signal<void> v_signalDefaults;
		// Signal emitted when Cancel is clicked.
		sigc::signal<void> v_signalCancel;
		
		/* Sets the sensitivity of the confirmation button, defaults true. */
		void setConfirmBtnSensitive(bool p_sensitive);
		
	private:
		Gtk::Button btn_cancel,
					btn_defaults,
					btn_confirm;
		
		void btn_cancel_clicked();
		void btn_defaults_clicked();
		void btn_confirm_clicked();
	}; // END - SubWindowControls
	
	
	
	class DestructiveConfirmationButton : public Gtk::Bin{
	public:
		/* DestructiveConfirmationButton:
		 * @param p_buttonLabel: the label to be displayed on the first button.
		 * @param p_warningText: The text to be displayed when the user presses the first stage button. */
		DestructiveConfirmationButton(Glib::ustring p_buttonLabel, Glib::ustring p_warningText);
		virtual ~DestructiveConfirmationButton();
		
		// Signal emitted when the confirm action button is pressed.
		sigc::signal<void> v_signalConfirm;
		
	private:
		// The outermost grid.
		Gtk::Grid v_mainGrid;
		// The grid held within the revealer.
		Gtk::Grid v_revealerGrid;	
		// The two buttons: first stage is the initial button.
		Gtk::Button btn_firstStage,
					btn_confirm,
					btn_cancel;

		Gtk::Label txt_warningLabel;
		Glib::ustring txt_warningLabelPretext = "<span> <b> ARE YOU SURE? </b> </span>"; // Pre-formatted warning text.
		
		Gtk::Revealer v_revealer; // Hides the confirmation button & text.
		
		// Progress bar to show the time left for the user to confirm the action.
		Gtk::ProgressBar v_timerBar;

		void btn_firstStage_clicked();
		void btn_confirm_clicked();
		void btn_cancel_clicked();
		bool idle_timerUpdate();
		
	}; // END - Confirmation button.
	
	
	/* Timer Button:
	 * A button which will emit v_signalButtonPressed after a delay; 
	 * during which a user can cancel the click.
	 */
	class TimerButton : public Gtk::Bin{
	public:
		TimerButton();
		TimerButton(Glib::ustring p_label);
		virtual ~TimerButton();
		
		// Emitted when the timer button elapses (and is not cancelled).
		sigc::signal<void> v_signalButtonPressed;
		
		/* Set Overlay Visibility:
		 * Sets whether the cancel button & timer bar is visible, and the underlaying button is desensitised.
		 * @param p_isVisible : Should the overlay aspect be made visible? */
		void setOverlayVisibility(bool p_isVisible);
		
		// Sets the label of the main button.
		void setButtonLabel(Glib::ustring p_buttonLabel);
		
	private:
		Gtk::Overlay v_mainOverlay;
		Gtk::Grid v_mainGrid;
		Gtk::Button btn_main;
		Gtk::Button btn_cancel;
		Gtk::ProgressBar v_timerBar;
		
		bool v_cancelled; // Bool; when true the operation was cancelled: should stop the signal being emitted.
		
		// Ensures both constructors are setup properly.
		void setup();
		
		void btn_main_clicked();
		void btn_cancel_clicked();
		
		// Function to reduce the timerBar and emit a signal when completed.
		bool idleCountdown();
		
	}; // END - TimerButton class
	

	/* Launch Engine Button:
	 * Derived button to hold additional information.
	*/
	class LaunchEngineBtn : public TimerButton{
	public:
		/*Launch Engine Button constructor.
		 * @param p_launchDir : The path to the editor executable.
		 * @param p_configFile : The config file that represents this buttons settings. */
		LaunchEngineBtn(Glib::ustring p_configFile);
		virtual ~LaunchEngineBtn();
		
		// Returns the config file associated with this button.
		Glib::ustring getConfigFile();
		
		// Propogates the internal launchString command with the string given.
		void setLaunchString(Glib::ustring p_string);
		
		// Signal emitted when button pressed; passes the launch options as a parameter.
		sigc::signal<void, Glib::ustring> v_signalLaunchBtnPressed;
		
	private:
		// The buttons associated config file.
		Glib::ustring v_configFile;
		
		// The string appended to the command when launched.
		Glib::ustring v_launchString;
		
		// Linked to button pressed event: emits signal with launch parameter.
		void btn_selfPressed();

	}; // END - LaunchEngine Button.


} // END - Unrealated Launcher namespace.


#endif