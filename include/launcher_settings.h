#ifndef LAUNCHER_SETTINGS
#define LAUNCHER_SETTINGS

#include "include/launcher_utilities.h"
#include "include/launcher_printer.h"

#include <external/simpleIni/SimpleIni.h>
#include <iostream>
#include <string>

namespace LauncherSettings{
// STATUSES:

	/* FIRST RUN:
	 * If true, it's considered the first time the program has been run. */
	extern bool firstRun;

	/* SETTINGS FILE STATUS:
	0	No error & file exists.
	-1	Failed to WRITE to file.
	-2	Failed to READ to file. */
	extern int	settingsFileStatus;

	/*  */
//	extern bool
	
	
// GENERAL SETTINGS:
	// The launchers theme. If null, system theme is used.
	extern Glib::ustring	launcherTheme;
	
	/* The default page opened when program is opened.
	0 : Library.
	1 : Market
	2 : Community*/
	extern int defaultPage;

	// Directories are detected and set independantly from the read/write to file functions: during "SETUP SETTINGS".
	// The directory where the settings are stored.
	extern Glib::ustring	settingsDirectory;

	// The file where the global config settings are stored.
	extern Glib::ustring	settingsFile;
	
	// The directory (including the slash) where engine configs are stored.
	extern Glib::ustring	engineConfigs;
	
	// The directory (including the last slash) where project configs are stored.
	extern Glib::ustring 	projectConfigs;
	
	// The default directory to install engines too.
	extern Glib::ustring	defaultInstallDirectory;
	
	//	Bool to determine if timed buttons should be enabled */
	extern bool timedButtonsEnabled;
	
	// Possibly move to new "marketplace" section!
	// The directory for downloaded marketplace items.
	extern Glib::ustring marketAssetDownloadDir;

// Repository Manager:
	/* The directory of the launchers main engine repository. */
	extern Glib::ustring	launcherRepositoryDirectory;
	
	/* Whether to use SSH(True). Use HTTPS if false. */
	extern bool	useSSHAuth;
	
	/* The file of the public key for SSH Authentification */
	extern Glib::ustring sshAuthPubKey;
	
	/* The file of the private key for SSH Authentification */
	extern Glib::ustring sshAuthPrivKey;
	
	/* Whether or not to save the login credentials for Git. */
	extern bool		rememberGitLogin;	// Git login strings are local to the functions and not stored globally.
	
	/* Whether to auto-refresh lists after repoMan task */
	extern bool refreshListsAfterRepoManTask;
	
	/* Whether automatic updates to the repository is enabled: requires SSH. */
	extern bool automaticRepoUpdate;
	
	/* The interval (in minutes) in which the repository is updated  */
	extern int autoRepoUpdateInterval;

// LIBRARY:
	// Bool, used to remember if the engine pane is visible or hidden.
	extern bool enginesVisible;
	
	// Bool, used to remember if the project pane is viisble or hidden.
	extern bool projectsVisible;
	
	// Bool to remember if the engine/project pair view is enabled.
	extern bool engineProjectPairs;

	// Bool, used to remember the direction option.
	extern bool verticalView;

	// Convenience string to hold "/Engine/Binaries/Linux/UE4Editor".
	extern Glib::ustring editorPath;




// SETTINGS FUNCTIONS:

	/*	UPDATE FROM FILE:  READS the values from the config file.
	 * 	Does not write values to GUI.  GUI values should be updated after reading.
	 * 	Returns 0 on success.
	 *  Returns -1 on generic fail.
	 *  Returns -2 on file not existing. */
	static int readFromFile();

	/*	WRITES SETTINGS TO FILE:
	*	Does not write from GUI.  LauncherSettings::Values should be updated using the GUI values first.
	* 	Returns 0 on success.
	*  Returns -1 on failed to write.*/
	static int writeSettingsToFile(); 

	// Sets all values to default.
	static void setDefaults();






	// FUNCTION DEFINITIONS:

static int readFromFile(){ 
	UnrealatedLauncher::LauncherPrinter v_logger;
	// Check file exists:
	if( LauncherUtils::fileExists( LauncherSettings::settingsFile.c_str() ) ){
		v_logger.debug("Reading launcher config file.");
		// read file config...
		CSimpleIniCaseA temp_ini;
		SI_Error temp_iniError = temp_ini.LoadFile( LauncherSettings::settingsFile.c_str() );
		if(temp_iniError < 0){
			v_logger.errorMsg("Failed to load launcher config file!");
			return -1;
		} // END - if iniError.
		
		firstRun = temp_ini.GetBoolValue("firstRun", "firstRun", true);
		
		// GENERAL:
		defaultPage = temp_ini.GetLongValue("general", "defaultPage", 0);
		defaultInstallDirectory = temp_ini.GetValue("general", "defaultInstallDirectory", "./engines");
		timedButtonsEnabled = temp_ini.GetBoolValue("general", "timedButtonsEnabled", true);
		marketAssetDownloadDir = temp_ini.GetValue("general", "marketAssetDownloadDir", "./market");
		
		// REPO MANAGER:
		useSSHAuth = temp_ini.GetBoolValue("repoMan", "useSSHAuth", false);
		sshAuthPubKey = temp_ini.GetValue("repoMan", "sshAuthPubKey", "");
		sshAuthPrivKey = temp_ini.GetValue("repoMan", "sshAuthPrivKey", "");
		launcherRepositoryDirectory = temp_ini.GetValue("repoMan", "launcherRepositoryDirectory", "./launcherRepo");
		refreshListsAfterRepoManTask = temp_ini.GetBoolValue("repoMan", "refreshListsAfterRepoManTask", true);
		automaticRepoUpdate = temp_ini.GetBoolValue("repoMan", "automaticRepoUpdate", false);
		autoRepoUpdateInterval = temp_ini.GetLongValue("repoMan", "autoRepoUpdateInterval", 30);
		
		return 0;
	} else{ // END - If launcher Config file exists.
		v_logger.errorMsg("Launcher config file not found!");
		LauncherSettings::settingsFileStatus = -3;
		return -2;
	} // END - If Launcher Config file doesn't exist.
} // END - Read settings from file.
	
static int writeSettingsToFile(){ 
	UnrealatedLauncher::LauncherPrinter v_logger;

	v_logger.debug("Writing settings to file.");
	
	CSimpleIniCaseA temp_ini;
	SI_Error temp_iniError = temp_ini.LoadFile( settingsFile.c_str() );
	std::string temp_dataString;

// SET VALUES:
	temp_ini.SetBoolValue("firstRun", "firstRun", firstRun, "; Used to modify behaviour based on applications first run.");
	
// GENERAL:
	temp_ini.SetLongValue("general", "defaultPage", defaultPage, "; The default page to display.");
	temp_ini.SetValue("general", "defaultInstallDirectory", defaultInstallDirectory.c_str(), "; The default installation directory for new engines.");
	temp_ini.SetBoolValue("general", "timedButtonsEnabled", timedButtonsEnabled, "; Sets whether to enable or disable the timed buttons.");
	temp_ini.SetValue("general", "marketAssetDownloadDir", marketAssetDownloadDir.c_str(), "; The directory where market place assets are downloaded.");
	
// REPO MANAGER
	temp_ini.SetBoolValue("repoMan", "useSSHAuth", useSSHAuth, "; Whether to use SSH authentification.");
	temp_ini.SetValue("repoMan", "sshAuthPubKey", sshAuthPubKey.c_str(), "; The path to the PUBLIC SSH Key.");
	temp_ini.SetValue("repoMan", "sshAuthPrivKey", sshAuthPrivKey.c_str(), "; The path to the PRIVATE SSH key.");
	temp_ini.SetValue("repoMan", "launcherRepositoryDirectory", launcherRepositoryDirectory.c_str(), "; Directory to the launchers UE4 repository.");
	temp_ini.SetBoolValue("repoMan", "refreshListsAfterRepoManTask", refreshListsAfterRepoManTask, "; Refreshes the commit lists after the repository manager finishes updating");
	temp_ini.SetBoolValue("repoMan", "automaticRepoUpdate", automaticRepoUpdate, "; Automatically updates the launcher repository at a set interval.");
	temp_ini.SetLongValue("repoMan", "autoRepoUpdateInterval", autoRepoUpdateInterval, "; The interval in minutes the repository is updated.");
	
// SAVE VALUES TO FILE:
	temp_iniError = temp_ini.Save( temp_dataString );
	if(temp_iniError < 0 ){
		v_logger.warnErrMsg("Failed to save data to datastring.  Your settings will not be saved after closing!");
		LauncherSettings::settingsFileStatus = -1;
		return -1;
	} // END - If Failed to save data to data string.
	
	temp_iniError = temp_ini.SaveFile( LauncherSettings::settingsFile.c_str() );
	if( temp_iniError < 0 ){
		v_logger.warnErrMsg("Failed to save data to file.  Your settings will not be saved after closing!");
		LauncherSettings::settingsFileStatus = -1;
		return -1;
	} // END - If failed to save file.
	
	v_logger.appendDebug("Settings saved successfully!");
	return LauncherSettings::settingsFileStatus = 0;
} // END - Write settings to file.

static void setDefaults(){
	// Essentially a copy-paste from the .cpp file.

	firstRun = true;
	settingsFileStatus = 0;

	// General:
	launcherTheme = "";
	defaultPage = 0;
	timedButtonsEnabled = true;
	marketAssetDownloadDir = "./market";
	
	
	launcherRepositoryDirectory = "./launcherRepo";
	defaultInstallDirectory = "./engines";
	rememberGitLogin = false;
	
	// Directories are detected and set independantly from the read/write to file functions: during "SETUP SETTINGS".
/*
*	DO NOT UNCOMMENT, ELSE ERRORS WILL INCUR!
*	settingsDirectory = "";	// The directory where the settings are stored.
*	settingsFile = "";		// The file where the global config settings are stored.
*	engineConfigs = "";		// The directory where engine configs are stored.
*	projectConfigs = "";	// The directory where project configs are stored.
*/
	
// REPOSITORY MANAGER:
	useSSHAuth = false;
	sshAuthPubKey = "";
	sshAuthPrivKey = "";
	refreshListsAfterRepoManTask = true;
	automaticRepoUpdate = false;
	autoRepoUpdateInterval = 30;

	
// LIBRARY:
	enginesVisible = true;
	projectsVisible = true;
	engineProjectPairs = true;
	verticalView = false;
//	editorPath = "/Engine/Binaries/Linux/UE4Editor";
} // END- set defaults.

} // END - Unrealated launcher namespace.

#endif