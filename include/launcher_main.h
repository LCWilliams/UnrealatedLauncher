// COPYRIGHT © 2018
// AUTHOR(S):	Lee Connor Williams
// All Rights Reserved

#ifndef LAUNCHER_MAIN
#define LAUNCHER_MAIN


#include <gtkmm-3.0/gtkmm.h>

#include "include/launcher_library.h"
#include "include/launcher_community.h"
#include "include/launcher_market.h"
#include "include/launcher_settingsGUI.h"
#include "include/launcher_settings.h"
#include "include/launcher_accountSetup.h"
#include "include/launcher_printer.h"

#include <iostream>


namespace UnrealatedLauncher{

	class UnrealatedLauncherWindow : public Gtk::ApplicationWindow{
	public:
		UnrealatedLauncherWindow();				// Constructor.
		virtual ~UnrealatedLauncherWindow();	// Destructor.

		// Main Pages:
		LauncherSettingsGUI v_launcherSettings;
		LauncherLibrary v_launcherLibrary;
		LauncherCommunity v_launcherCommunity;
		LauncherMarket v_launcherMarket;
		
		// Popout pages:
		LauncherAccountSetup v_accountSetupBin;

	private:
		LauncherPrinter v_logger;
	
		/* The main grid for the window. */
		Gtk::Grid	v_mainGrid;
		/* A grid for housing the main buttons (mainPageStack & settings). */
		Gtk::Grid	v_mainButtonGrid;
		
		/* @v_mainStack The main stack, to switch between the mainPages stack, and the settings window.
		 * The stack is changed manually and doesn't have a stack switcher.
		 * @v_mainPagesStack The stack for the main pages (library, market, etc). */
		Gtk::Stack	v_mainStack,
					v_mainPagesStack;
					
		/* The Stack Switcher (buttons) for the main pages stack. */
		Gtk::StackSwitcher btn_mainPagesStackSwitcher;
		
		/* The main menu for the launcher. */
		Gtk::Menu		menu_launcherMenu;
		
		/* The button for the launcher menu. */
		Gtk::MenuButton btn_launcherMenu;
		
		/* Launcher Menu items.
		 * @menuItem_quit		Button to quit the launcher.
		 * @menuItem_settings	Button to open the settings.*/
		Gtk::MenuItem	menuItem_quit,
						menuItem_settings,
						menuItem_accountSetup;
		
		
		/* A signal to connect to the cancel/confirm buttons to swap the main stack back to mainPages */
		void settingsSignalClosed();
		void accountSetupSignalClosed();
		void accountSetupSignalCompleted(); // Calls similar named CLOSED function, then opens Add Engine.
		
		void menuItem_quit_clicked();
		void menuItem_settings_clicked();
		void menuItem_accountSetup_clicked();
		
		
	}; // END - UnrealatedLauncher Window class.

} // END - Unrealated launcher namespace

#endif