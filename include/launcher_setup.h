#ifndef LAUNCHER_SETUP
#define LAUNCHER_SETUP

#include "include/launcher_settings.h"
#include "include/launcher_utilities.h"
#include "include/launcher_printer.h"
#include <filesystem>


// THIS HEADER IS TO HOUSE FUNCTIONS USED BEFORE THE MAIN PROGRAM IS INITIALISED.

void launcherSetup_setupSettings(){
	UnrealatedLauncher::LauncherPrinter v_logger;
	std::string temp_configPath;	// Will be set as config path.
	std::string temp_homeDir = getenv("HOME");

// Config directory is determined outside of "UPDATE FROM FILE".
// This ensures the directory/file status is established beforehand.
//	std::cout << "Finding home directory..." << std::endl;
	v_logger.debug("Finding home directory...");

	if( !temp_homeDir.empty() ){
	// Home Directory Environment Variable isn't empty; using it:
		temp_configPath = temp_homeDir + "/.config/unrealatedLauncher";
		LauncherSettings::settingsDirectory = temp_configPath;
		LauncherSettings::settingsFile = temp_configPath + "/config.ini";
//		std::cout << "Home Directory found.	Launcher Settings File is: " << LauncherSettings::settingsFile << std::endl;
		v_logger.appendDebug("Home directory found.");
		v_logger.debug("Launcher settings file is: " + LauncherSettings::settingsFile);
	} else {
	// Home directory environment is empty/not found.
		v_logger.warnErrMsg("Home environment variable was not found. Using subfolder of launcher directory.");
		LauncherSettings::settingsDirectory = "/config";
		LauncherSettings::settingsFile = LauncherSettings::settingsDirectory + "/config.ini";
	} // END - If home directory is not empty.


// If no file is found, the default initialiser variable values are used.
	if( LauncherUtils::fileExists( LauncherSettings::settingsFile ) ){
	// File is found.  Read settings from file.
		LauncherSettings::readFromFile();
	} else { // END - If file exists.
	// File is not found.  Use defaults and create file.
		v_logger.warnMsg("WARN:	Settings File NOT found! Using defaults and creating file at: " + LauncherSettings::settingsDirectory);
		std::filesystem::create_directories( LauncherSettings::settingsDirectory.c_str() );
		LauncherSettings::writeSettingsToFile();
	} // END - If file doesn't exists
} // END - Setup settings.



void launcherSetup_createLauncherDirectories(){	// Creates the default directories.
	// Temporary string for combining strings below.
	Glib::ustring temp_directory;
	// Temporary string to combine the messages prior to debugCout.
	Glib::ustring temp_messageString;
	// Logger
	UnrealatedLauncher::LauncherPrinter v_logger;
	
	v_logger.debug("Directory setup...");

	// CREATE ENGINE SETTINGS DIRECTORY
	try{
	temp_directory = LauncherSettings::settingsDirectory + "/engines";
	temp_messageString =  "Engine Configuration Directory: " + temp_directory + " : " 
	+ std::to_string( std::filesystem::create_directories( temp_directory.c_str() ) );
	v_logger.appendDebug(temp_messageString);
	// Set settings for engine config.
	LauncherSettings::engineConfigs = temp_directory + "/";
	} // END - engine settings try.

	catch(std::exception& v_error){
		temp_messageString = "\nERROR:	Failed to create engine config directory!"; 
		temp_messageString += v_error.what();
		v_logger.errorMsg(temp_messageString);
	} // END - Catch.

	// CREATE PROJECT SETTINGS DIRECTORY
	try{
	temp_directory = LauncherSettings::settingsDirectory + "/projects";
	temp_messageString = "Project Configuration Directory: " + temp_directory + " : " 
	+ std::to_string( std::filesystem::create_directories( temp_directory.c_str() ) );
	v_logger.appendDebug(temp_messageString);
	// Set settings for project configs.
	LauncherSettings::projectConfigs = temp_directory + "/";
	} // END - try create project settings dir.
	catch(std::exception& v_error){
		temp_messageString = "Failed to create project config directory!";
		temp_messageString += v_error.what();
		v_logger.warnMsg(temp_messageString);
	} // END - Catch.
	
	try{
	temp_messageString = "Launcher Repository Directory: " + LauncherSettings::launcherRepositoryDirectory + " : " 
	+ std::to_string( std::filesystem::create_directories( LauncherSettings::launcherRepositoryDirectory.c_str() ) );
	v_logger.appendDebug(temp_messageString);
	} // END - Try crating launcher repo dir.
	catch(std::exception& v_error){
		temp_messageString = "Failed to create launcher repository directory!"; 
		temp_messageString += v_error.what();
		v_logger.warnMsg(temp_messageString);
	} // END - Catch failed to make launcher repo dir.
	
	try{
	temp_messageString = "Default Install Directory: " + LauncherSettings::defaultInstallDirectory + " : " 
	+ std::to_string( std::filesystem::create_directories( LauncherSettings::defaultInstallDirectory.c_str() ) );
	v_logger.appendDebug(temp_messageString);
	} // END - try creating default install directory.
	catch(std::exception& v_error){
		temp_messageString = "Failed to create default install directory!"; 
		temp_messageString += v_error.what();
		v_logger.warnMsg(temp_messageString);
	} // END - Catch.
	
	try{
		temp_messageString = "Market Asset Dir: ./market : " + std::to_string( std::filesystem::create_directories("./market") );
		v_logger.appendDebug(temp_messageString);
	}// END - Try create market asset dir.
	catch (std::exception& v_error){
		temp_messageString = "Failed to create default market asset directory!"; 
		temp_messageString += v_error.what();
		v_logger.warnMsg(temp_messageString);
	}  // END - Catch

	v_logger.appendDebug("Directory Setup complete!");
} // END - Create launcher directories.

#endif