#ifndef LAUNCHER_LIBRARY_ADDENGINE
#define LAUNCHER_LIBRARY_ADDENGINE

#include "include/launcher_settings.h"
#include "include/launcher_utilities.h"
#include "include/library/library_engine.h"
#include "include/library/library_repoManager.h"
#include "external/git2.h"
#include <cctype>
#include <gtkmm-3.0/gtkmm.h>

namespace UnrealatedLauncher{
	
	// FORWARD DECLARATIONS:
	struct EngineData;
	class LauncherRepositoryManager;
	class LauncherRepoCommitColumns;
	
	// The GUI window for adding a new engine.
	class LauncherAddEngine : public Gtk::Bin {
	public:
		LauncherAddEngine(LauncherRepositoryManager* p_repoManagerRef);
		virtual ~LauncherAddEngine();

		// REFERENCES:
		// ref_repoManager - set by constructor.
		LauncherRepositoryManager* ref_repoManager;
		// repomanagerGUI - created in construction.
		LauncherRepoManagerGUI* ref_repoManGUI;
		
		// Set the default options.
		void setDefaults();
		
		// Returns the engine data object.
		EngineData* getEngineData();
		
		// SIGNALS:
		typedef sigc::signal<void> typeSignalAddEngine;
		typeSignalAddEngine signal_addEngine();
		typeSignalAddEngine v_signalAddEngine;
		typeSignalAddEngine v_signalCloseWindow; // Close the window.
		
		LauncherPrinter v_logger;
		
		/* modifies the GUI to deliberately guide the user through downloading the repository,
		  Ran before opening AddEngineGUI and a check to "getRepositoryExists" fails. */ 
		void setupFirstRunGUI();

	private:
		// The engine ID, set within setDefault.
		unsigned int v_engineID;
	
		// The main grid placed inside the bin.
		Gtk::Grid v_mainGrid;
		
		// Main flowbox to allow the GUI elements to move.
		Gtk::FlowBox v_mainFlowbox;
		
		// Scrolled window to house the flowbox.
		Gtk::ScrolledWindow v_mainFlowboxScrWindow;
		
		// The cancel/defaults/confirm controls.
		DialogueControls v_windowControls;

		// An engine data object to be sent to the library.
		EngineData* v_engineData;
		
		// Frame to house the user input options.
		Gtk::Frame v_userInputFrame;
		// Frame to house the repository manager.
		Gtk::Frame v_repoManagerFrame;
		// Revealer to house the repository manager.
		Gtk::Revealer v_repoManRevealer;
		// Frame to house The commit selection.
		Gtk::Frame v_commitSelectionFrame;
		// Frame to house the output information.
		Gtk::Frame v_endOutputFrame; 
		

		
		// Grid for the user input options.  Held inside v_userInputFrame.
		Gtk::Grid v_userInputGrid;
		// Grid to house the commit selection.
		Gtk::Grid v_commitSelectionGrid;
		// Grid for the output information. Held inside v_endOutputFrame.
		Gtk::Grid v_endOutputGrid;
		
		// USER INPUT ITEMS:
		Gtk::CheckButton btn_addExisting;	// Button to toggle logic between adding a new or existing engine.
		
		Gtk::Label txt_labelEngineLabel;	// Label for engine label entry.
		Gtk::Entry btn_engineLabel;			// Entry for engine label.
		
		Gtk::Label txt_labelEngineDesc;		// Label for engine description entry.
		Gtk::Entry btn_engineDescription;	// Entry for engine description.
		
		Gtk::Label txt_labelInstallDir;		// Label for engine install directory.
		Gtk::FileChooserButton btn_installDir;	// Chooser for engine install directory.
		Gtk::Button btn_installDirReset;	// Button to reset the engine install dir to default.
		
		Gtk::Expander btn_advancedOptions;	// Expander to show/hide advanced options.
		Gtk::Grid v_advancedOptsGrid;		// Grid to house advanced options.
		
		Gtk::Label	txt_labelCustomParam;	// Label for custom parameter entry.
		Gtk::Entry btn_customParameters;	// Entry for custom parameters.
		
		// UII: GITHUB/COMMIT SELECTION
		Gtk::TreeView v_commitTreeView;		// Commit Tree viewer.
		Gtk::TreeView v_tagTreeView;		// TAG Tree viewer.
		Gtk::ScrolledWindow v_commitScrollWindow;	// Scroll window for commit tree viewer.
		Gtk::Label txt_labelShowCommitTags;	// Label for button to only show commits with tags (releases).
		Gtk::CheckButton btn_showCommitTags;		// Toggle button, swaps the view between showing TAGS (active) or ALL commits (not active).
		Gtk::SearchEntry btn_commitSearch;	// Search button to search through commits.
		Gtk::Button btn_refreshView;		// Button to refresh the view with new lists.
		Gtk::Spinner v_refreshViewSpinner;	// A Spinner to display activity.
		Gtk::Button btn_showRepoMan;	// Toggle button to show/hide the repository manager.
		// Overlay to house repo manager & commit selection.
		Gtk::Overlay v_commitOverlay;
		
		// END OUTPUT ITEMS:
		Gtk::Label txt_labelOutputEngineID;		// Label to show the engines internal ID.
		Gtk::Label txt_labelOutputLabel;		// Label to show the output engine label.
		Gtk::Label txt_labelOutputDesc;			// Label to show a preview of the description.
		Gtk::Label txt_labelOutputInstallDir;	// Label to show the full installation directory.
		bool v_installDirIsDefault = true;		// Sanity bool: used when checking if the install dir is the default. Since the default folder wont exist until afterwards.
		Gtk::Label txt_labelOutputGitCommit;	// Label to show the git commit used by the engine.
		Gtk::Label txt_labelOutputcustomParams;	// Label to show the custom parameters.
		
		
		Gtk::Image img_outputInstallDir;
		Gtk::Image img_outputGitCommit;


		Gtk::Label txt_outputEngineID;		// Label to show the engines internal ID.
		Gtk::Label txt_outputLabel;			// Label to show the output engine label.
		Gtk::Label txt_outputDesc;			// Label to show a preview of the description.
		Gtk::Label txt_outputInstallDir;	// Label to show the full installation directory.
		Gtk::Label txt_outputGitCommit;		// Label to show the git commit used by the engine.
		Gtk::Label txt_outputcustomParams;	// Label to show the custom parameters.
		
		// Ints to store version & hotfix numbers taken from spliced Tag: -1 indicates not set.
		int	v_outputVersion = -1, 
			v_outputHotfix = -1;
		// Bool to dictate whether the spliced tag is a PREVIEW.
		bool v_isPreview = false;
		
	// STRINGS, Stored here for ease of translation.
		// Text to display when no engine was found at the directory.
		Glib::ustring txt_error_noEngineFound = "NO ENGINE FOUND AT DIRECTORY";
		// Text to display when no git commit was selected.
		Glib::ustring txt_error_noCommitSelected = "NO COMMIT WAS SELECTED";
		
		// (Re)connects listAll data to treeview.
		void reconnectLists();
		
		
		// Runs checks to ensure the details provided 
		bool canConfirm();
		
		// Updates text entry based output.
		void updateTextEntryOutputs();
		
		// Updates the output data with the buttons input.
		void updateButtonOutput();
		
		void btn_installDirReset_clicked();
		void btn_addExisting_clicked();
		void btn_showRepoMan_clicked();
		void btn_closeRepoMan_clicked();
		void btn_refreshView_clicked();
		void btn_showCommitTags_clicked();
		void treeviewUpdated(const Gtk::TreeModel::Path& p_path, Gtk::TreeViewColumn*);	// Special required function for connecting to signal changed.
		void treeviewSelectionChanged();
		
		// Ran when repoManger task finishes- auto refreshes the lists.
		void repoManTaskFinished();
		
		void btn_cancel_clicked();
		void btn_confirm_clicked();
		
	};
	
} // END - Unrealated launcher namespace.

#endif