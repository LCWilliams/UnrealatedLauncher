#ifndef LAUNCHER_REPOMANAGER
#define LAUNCHER_REPOMANAGER

#include "include/launcher_settings.h"

#include <string>
#include "external/git2.h"
#include <gtkmm-3.0/gtkmm.h>
#include <iostream>
#include <thread>

namespace UnrealatedLauncher{
	
	class LauncherRepoCommitColumns : public Gtk::TreeModel::ColumnRecord{
	public:
		LauncherRepoCommitColumns(){
			add(v_commitID);
			add(v_commitTitle);
			add(v_tagVersion);
			add(v_tagHotfix);
			add(v_tagType);
		} // END - Constructor.

		
		Gtk::TreeModelColumn<Glib::ustring> v_commitID;		// The Commit ID.
		Gtk::TreeModelColumn<Glib::ustring> v_commitTitle;	// The title/description a commit.
		Gtk::TreeModelColumn<unsigned int>	v_tagVersion;	// The major iteration of a tag.
		Gtk::TreeModelColumn<unsigned int>	v_tagHotfix;	// The minor iteration of a tag.
		Gtk::TreeModelColumn<Glib::ustring> v_tagType;		// The type of tag (release/preview)
		
		
	}; // END - Launcher Repo Commit columns.
	
	// A DATA ONLY OBJECT for managing the launchers repository.
	class LauncherRepositoryManager{
		public:
		LauncherRepositoryManager();
		virtual ~LauncherRepositoryManager();
		
		// Bool to determine if a task is already in progress, if true, new tasks will not be started!
		bool v_taskInProgress = false;
		
		enum class lastThreadTask : int{
			noTask = 0,
			download = 1,
			updateRepo = 2,
			updateLists = 3,
			deleteRepo = 4
		};
		
		lastThreadTask v_lastThreadTask;
		
		
		// Called from THREAD functions, emits from dispatcherUpdateProgress;
		void notifyUpdateProgress();
		// Called from THREAD functions, emits from dispatcherUpdateLists;
		void notifyUpdateLists();
		void notifyPromptLogin();
		void notifyTaskFinished();

		// Returns the stored user email.
		Glib::ustring getUserEmail();
		// Returns the stored user password.
		Glib::ustring getUserPassword();
		
		// Instigates a thread to download the launcher repository.
		void instigateDownloadRepository();
		// Instigates a thread to update the launcher repository.
		void instigateUpdateRepository();
		// Instigates a thread to delete the launcher repository.
		void instigateRepoDeletion();
		
		// Instigates a thread to update the tree models for tagged & all commits.
		void instigateUpdateRepoLists();

		
		typedef Glib::RefPtr<Gtk::ListStore> typeListStore;
		/* Returns one of the two commit tree models.
		 * @param p_returnCommits : returns the tree model showing all tags when TRUE,
		 * 		or tree model showing just releases/tags when FALSE.
		 */
		typeListStore getCommitTreeModel(bool p_returnCommits);
		
		// Sets the Authentificiation method: if true, SSH is used.
		void setAuthMethod(bool p_useSSH);
		// Returns whether or not SSH is the chosen authentification method.
		bool getUseSSH();
		/*	Returns whether a repository exists at the option-specified ocation.
			Will return FALSE if repo doesn't exist or error occured. */
		bool getRepoExists();
		
		// Bool: true if user Authentification passed, false if not- should prompt login again.
		bool v_gitAuthPassed;
		// Bool to check if first time provided details have been used:
		bool v_triedFirstDetails = false;
		
		// Bool to determine if waiting on user input (used for thread/callback!).
		bool v_waitingOnUserInput = false;
		// Bool to stop a signal being emitted a gazillion times!
		bool v_promptUserInputCalled = false;
		
		// Connected to GUI to link button presses.
		void btn_userLoggedIn();
		
		// Sets the two internal login credentials for HTTP connections (email:password).
		void setHTTPLoginCredentials(Glib::ustring, Glib::ustring);
		
		LauncherRepoCommitColumns v_repoCommitColumns;

		// ThreadComm- task data items:
		bool threadComm_taskFailed = false;
		double threadComm_taskPercent = 0;
		Glib::ustring threadComm_taskMessage = "";

		// The thread for repository manager tasks: if valid pointer, a task is in progress.
		// Delcaring it as a member allows for re-joining/task cancelling.
		std::thread* v_repoManagerThread;
				
		// GET taskdata, convenience function to return a copy of the taskdata that uses mutex locking.
		void getThreadComm_taskData(Glib::ustring& p_message, double& p_percentage);

		// SET taskdata, convenience function that includes mutex locking and calls notify.
		void setThreadComm_taskData(bool, double, Glib::ustring);
		
		// Use Dispatcher for inter-thread communication between workers.
		Glib::Dispatcher dispatcherUpdateProgress;
		Glib::Dispatcher dispatcherPromptLogin;
		Glib::Dispatcher dispatcherUpdatedLists;
		Glib::Dispatcher dispatcherTaskEnded;

		// Mutex to lock during thread communication
		std::mutex threadComm_mutex_taskData;
		
	protected:
		typeListStore v_listTags;		// A list-store containing only tagged/released commits.
		typeListStore v_listAll;		// A list store containing ALL commits.
		
	private:
		LauncherPrinter v_logger;
	
		// The repository URL, using HTTP.
		std::string v_repoURLHTTP = "https://github.com/EpicGames/UnrealEngine.git";
		std::string v_repoURLSSH = "git@github.com:EpicGames/UnrealEngine.git";
		// The repository URL in use, defaults to HTTP URL.
		std::string v_repoURL = v_repoURLHTTP;
		// Whether or not to use SSH or HTTP method.
		bool v_SSHLogin = false;
		
		// The login credentials for HTTPS connections.
		Glib::ustring v_gitHTTPEmail = "";
		Glib::ustring v_gitHTTPPass = "";
		
		// Convenience function, handles printing errors and emitting information.
		// failedInfo is emitted, debugInfo is printed.
		// If task that failed is git related, print git error.
		void taskFailed(Glib::ustring p_failedInfo, Glib::ustring p_debugInfo, bool p_isGitRelated);
		
		// Convenience function, returns the git error number.
		// Returns 1 if gitErr doesn't exist.
		int gitTaskFailed();
		
		// Deletes the thread, effectively allowing a new task to start.
		void deleteThread();
		// Joins the thread, then deletes.
		void closeThread();
		

		// The thread function for downloading the repository.
		void downloadRepository();
		// The thread function to update the repository.
		void updateRepository();
		//The thread function for deleting the repository.
		void deleteRepository();
		// The thread function to update the two repository lists.
		void updateRepoLists();
		
		// Int to store the number of files between launcherRepoFile functions.
		unsigned int v_launcherRepoFilesFound = 0;
		// Int to store how many files have been removed.
		unsigned int v_launcherRepoFilesRemoved = 0;
		
		// Recursively opens the launcher repo directory and counts the files.
		unsigned int launcherRepoFileCount(Glib::ustring p_path);
		
		// Recursively opens the launcher repo directory and DELETES the files.
		unsigned int launcherRepoDeleteFiles(Glib::ustring p_path);
		
		
	// GIT objects:
		// The local github repository for the launcher.
		git_repository* v_repoMan_repository;
		// The remote repository object.
		git_remote* v_repoMan_remote;
		// the Github credential object.
		git_cred* v_repoMan_credential;
		// The github transport object.
		git_transport* v_repoMan_transport;
		
	}; // END - LauncherRepositoryManager




	// The Graphical user interface for interacting with launcher repository manager object.
	class LauncherRepoManagerGUI : public Gtk::Bin {
		// Constructor; requires a launcher repo data object.
	public: 
		LauncherRepoManagerGUI(LauncherRepositoryManager* p_repoManagerData);
		virtual ~LauncherRepoManagerGUI();

		// Button to close the repo manager page.
		Gtk::Button btn_closeManager;
		
		// Signal for a user confirming a login. 
		typedef sigc::signal<void, Glib::ustring, Glib::ustring > typeSignalUserLoggedIn;
		typeSignalUserLoggedIn signal_userLoggedIn();
		
		// Signal to emit whether or not the manager is currently busy.
		typedef sigc::signal<void, bool> typeSignalManagerBusy;
		typeSignalManagerBusy v_signalManagerBusy;
		
	protected:
//		typeSignalPromptLogin v_signalPromptLogin;
		typeSignalUserLoggedIn v_signalUserLoggedIn;
	private:
		LauncherPrinter v_logger;
	
		// Reference to the repo data manager object.
		LauncherRepositoryManager* ref_repoManager;
		
		// Glib Dispatcher for cross thread signaling:
		Glib::Dispatcher v_signalDispatcher;
	
		// The main grid for the bin.
		Gtk::Grid v_mainGrid;
		
		// The overlay for login prompts.
		Gtk::Overlay v_overlay;
		
		// Buttons related to connection options.
		Gtk::Button btn_gitHTTPLogin;
		// Entry fields for http connection
		Gtk::Entry	btn_gitAccountEmail,
					btn_gitAccountPass;
		
		// Button for downloading the repository.
		Gtk::Button btn_downloadRepo;
		// Button for updating the repository.
		Gtk::Button btn_updateRepo;
		// Button for clearing the repository (toggle button, shows confirmation button afterwards).
		Gtk::ToggleButton btn_clearRepo;
		// Button to confirm deletion.
		Gtk::Button btn_confirmDelete;
		
		// Status revealer, to show/hide the status frame when tasks are in progress.
		Gtk::Revealer v_statusRevealer;
		// HTTP login revealer to show/hide login fields
		Gtk::Revealer v_loginRevealer;
		
		// Frames to house the seperate areas of the repo manager.
		Gtk::Frame	v_controlsFrame,
					v_statusFrame,
					v_connectFrame;
		// Grids to arrange items within the frames.
		Gtk::Grid	v_controlsGrid,
					v_statusGrid,
					v_connectGrid,
					v_connectHTTPLoginGrid;
		
	// Status items:
		// Progress bar
		Gtk::ProgressBar v_statusProgressBar;
		// Label for displaying the current active task:
		Gtk::Label txt_activeTask;
		// Button to confirm task completion/failed.
		Gtk::Button btn_closeStatus;
		// Spinner to show the UI hasn't frozen on long operations.
		Gtk::Spinner v_statusSpinner;
	
		// Idle function.  Makes sure that the update progress is called and pulses the bar for long operations.
		bool idleUpdateProgress();
	
		// Dispatcher connection function:
		// Updates the progress GUI.
		void notifyUpdateProgress();
		
		void updateProgress(Glib::ustring p_text, double p_progress);
		// Called when a task is completed.
		void taskFinished();
		// Called to update buttons based on repo status.
		void updateControls();
		// Connected to prompt login 
		void promptLogin();
		
	// Button Signals:
		// Button to connect using the provided information clicked.
		void btn_gitAccountConnect_clicked();
		// Button to connect using HTTPS clicked; opens revealer for user to provide options.
		void btn_gitHTTPSConnect_clicked();
		// Button to LOGIN using the details provided.
		void btn_gitHTTPLogin_clicked();
		// Button to disconnect clicked.
		void btn_gitDisconnect_clicked();
		// button to close status revealer.
		void btn_closeStatus_clicked();
		
		// Button to instigate downloading the repo; uses repoManager reference.
		void btn_downloadRepo_clicked();
		// Button to instigate updating the repository; uses repoManager reference.
		void btn_updateRepo_clicked();
		// Button to show/hide the confirm deletion button:
		void btn_clearRepo_clicked();
		// Button to instigate clearing the repo: uses repoManager reference.
		void btn_confirmDelete_clicked();
		
		
	}; // END - GUI Repo manager class.
	
} // END - Unrealated Launcher namespace.

#endif