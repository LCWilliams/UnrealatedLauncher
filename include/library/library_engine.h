#ifndef LAUNCHER_ENGINE
#define LAUNCHER_ENGINE

#include "include/launcher_utilities.h"
#include "include/launcher_settings.h"
#include "include/launcher_miniGUI.h"
#include "external/git2.h" // For git related engine block items.
#include <filesystem>
#include <thread>
#include <gtkmm-3.0/gtkmm.h> 
#include <assert.h>

namespace UnrealatedLauncher{
	
	// Forward declarations.
	class LauncherEngine;
	class EngineLaunchOpts;

	/* ENUMERATOR FOR ENGINE INSTALL STATUS.
	 * Make sure to cast to int when using.
	 * @var deleting: Engine installation was in the process of deletion.  
	 * 	If this status exists, deletion was interrupted and should be resumed.
	 * @var newBlock - The default status for a new block; no installation steps have commenced (or succeeded).
	 * @var ready - The engine is ready to be used.
	 * @var sourceCopied - The initial copy from the launcher repository has been copied.
	 * @var makeScriptRan - The make script was ran and completed.
	 * @var buildScriptRan - The build script was ran and completed.
	 */
	enum class EngineInstallStatusEnum : int{
		deleting = -2,
		newBlock = -1,
		ready = 0,
		sourceCopied = 1,
		revertedToCommit = 2,
		setupScriptRan = 3,
		makeScriptRan = 4
	}; // END - Install Status Enum.
	
	
	/* LIBRARY ENGINE BLOCK DATA:
		* A struct containing the data for engine installations.
	*/
	struct EngineData{
		// The Graphical User Interface block representing this data.
		LauncherEngine* ref_engineGUI;
		// Location of the config file for the engine.
		Glib::ustring configFile = "";
		// Is the data sane (externally modified).
		bool isSane = true;
		// Lists each elemenmt that wasn't sane.
		Glib::ustring notSaneMessage = "";
		// The internal identifier for the engine.
		unsigned int identifier = 0;
		// The displayed, user defined label for the engine.
		Glib::ustring label = "";
		// The installation directory for the engine.
		Glib::ustring installDir = "";
		// The directory to a user defined image for the engine.
		Glib::ustring imageDir = "";
		// The displayed, user defined description.
		Glib::ustring description = "";
		// The git commit for the engine, if available.
		Glib::ustring gitCommit = "";
		/* The main engine version: 
		 * If negative, it is assumed a specific commit was used and the 
		 * finished install engine data file was not yet read. */
		int version = 0;
		// The hotfix iteration of the engine.
		int hotfix = 0;
		/* The status of the installation of the engine.
			* -2 | Being deleted. Prompts continuation if interrupted.
			* -1 | New block.
			*  0 | Installed/Ready to be used.
			*  1 | Source Copied.
			*  2 | Setup scripts ran.
			*  3 | Make script ran.
		* An Enum* exists with convenient names for better readability.
		* Consider using it over direcly using values.
		*  *EngineInstallStatusEnum
		*/
		EngineInstallStatusEnum installStatus = EngineInstallStatusEnum::ready;
		/* Vector of launch option CONFIG FILES.  One launch button generated per option.
		* First entry is always default. */
		std::vector<Glib::ustring> launchOptions{""};
		
		/* SanityCheck: makes sure that the values loaded/given are sane(valid/true/useable).
		 * Returns TRUE if data is sane. */
		bool sanityCheck();
		
		/*Load Data:
		 * Loads the data from the config file.*/
		void loadData();
		
		/* Save data:
		 * Saves the data to a file (creates if not already existing).*/
		void saveData();
		
		/*Add new Launch Option:
		 * Adds a new entry to the launch options vector.
		 * @return Convenicene: The position of the new item within the vector.
		 * */
		unsigned int addNewLaunchOpt();
	}; // END - Engine Data struct
	
	/* LAUNCH OPTION TYPES */
	enum class LaunchOptionType : int{
		toggle = 0, // User input is a toggle (default, on, off).
		number = 1, // User input is numerical (spinButton).
		text = 2, // User input is textual.
		comboBox = 3, // User input is from a list.
		directory = 4, // User input is a directory selection
		file = 5 // Usr input is a file selection
	}; // END - Launch option type enum class
	
	
	/* LAUNCHER ENGINE:
	 * The GUI block representing an installed engine configuration.
	 * @param p_dataRef : REQUIRES a valid EngineData object reference! */
	class LauncherEngine : public Gtk::Bin{
	public:
		/* GUI Block representing an engine configuration.
		 * @param p_dataRef: An EngineData reference.
		 * Constructs the EngineBlock using the data in the object reference given.*/
		LauncherEngine(EngineData* p_dataRef);
		virtual ~LauncherEngine();
		
		// Returns the engine data reference for this block.
		EngineData* getEngineData();
		
		/* Public function, mainly used by threads to emit the dispatcher of the same name.
		* Notify's exist on the GUI thread. Dispatcher ensures that code is ran on GUI thread; not worker.*/
		void notifyUpdateProgress();
		void notifyTaskCompleted();
		void notifyIncrementTaskLevel();
		void notifyDeletionComplete();
		
		// Updates the objects internal repoManager status boolean: used to stop operations if the repo manager is busy. 
		void updateRepoManagerStatus(bool p_status);
		
		// Dispatcher/Signal for updating progress.
		Glib::Dispatcher v_dispatcherUpdateProgress;
		// Dispatcher/Signal for signifying a task is completed (entire install, or removal).
		Glib::Dispatcher v_dispatcherTaskCompleted;
		// Dispatcher/Signal for incrementing the level bar on SUB-TASK completions.
		Glib::Dispatcher v_dispatcherIncrementTask;
		// Signal to indicate deletion has finished and emits the deleted(this) engine GUI.
		typedef sigc::signal<void, LauncherEngine*> typeSignalDeletionComplete;
		typeSignalDeletionComplete v_signalDeletionComplete;

		typedef sigc::signal<void, LauncherEngine*> v_signalTypeEngineData;
		v_signalTypeEngineData v_signalEngineProperties;

		// Instigates the installation task based on the current installStatus.
		void instigateInstall();

		// Instigates the removal of the engine.
		void instigateRemoval();
		
		// Removes JUST the config file & Engine GUI.
		void instigateConfigRemoval();
		
		/* Update Launch Buttons:
		 * Generates two vectors, holding the labels and commands for each launch command.
		 * @param p_newLabels: The NEW labels to be applied to the launch buttons.
		 * @param p_newCommands: The NEW commands to be applied to the launch buttons.*/
		void updateLaunchButtons(std::vector<Glib::ustring> &p_newLabels, std::vector<Glib::ustring> &p_newCommands); 

		/* Update Labels from Data:
		* Updates the displayed engine label and the description using the strings from engineData.
		* Typically used by EngineProperties to instigate an update. */
		void updateLabelsFromData();

		// Updates the GUI for the current status.
		void updateGUIFromStatus();
		
		// Sets the status.
		void setInstallStatus(EngineInstallStatusEnum p_status);
		// Convenience function: sets the status and updates the GUI.
		void setInstallStatusAndUpdateGUI(EngineInstallStatusEnum p_status);
		
	private:
		LauncherPrinter v_logger;
		
		EngineData* ref_engineData;
		
		Gtk::Grid v_mainGrid;	// The main grid; contained inside v_mainOverlay.
		Gtk::Overlay v_mainOverlay;	// The main overlay. [REDUNDANT]
		Gtk::Grid v_subGrid;	// A subgrid for micromanagement of objects. [REDUNDANT]

		
		std::vector<LaunchEngineBtn*> v_launchBtnArray; // Array to hold launch buttons.
		Gtk::Grid v_launchBtnGrid; // Grid to store launch buttons.
		Gtk::ScrolledWindow v_launchBtnScrWindow; // Scrolled window for launch button grid.
//		Gtk::Button btn_launch;	// Button to launch the engine.
		Gtk::Button btn_options;	// Button to open options.
		Gtk::Label txt_labelEngineLabel;	// The displayed engine label.
		Gtk::Label txt_labelDescription;	// The displayed engine description.
		Gtk::ProgressBar v_taskProgressbar;	// A progress bar to display any task progress.
		Gtk::LevelBar v_taskLevelbar;		// A level bar to show how many steps are completed/remaining.
		
		// The mutex that should be used when dealing with threadComm_taskText & _taskPercent.
		std::mutex threadComm_taskProgress_mutex;
		// The thread communication variable for sharing task text.
		Glib::ustring threadComm_taskText;
		// The thread communication variable for sharing task percentage.
		double threadComm_taskPercent;
		// The thread for the engine block.
		std::thread* v_engineBlockThread;
		
		// TRUE when the repo manager is performing a task.
		bool v_repoManagerBusy = false;
		
		// Button functions:
		void btn_launch_clicked(Glib::ustring p_params);
		void btn_options_clicked();
		
		/* Creates and configures launch buttons.  
		 * Ensure removal of old buttons first! */
		void createLaunchButtons();
		
		// Removes the launch buttons (except the first/default).
		void removeLaunchButtons();

		// Convenience function to update progress; try_lock's mutex, then uses the values to update the progress bar.
		// Should be called from within notifyUpdateProgress.
		void updateProgress();
		
		/* Convenience function to update progress data & mutex locks.
		 * @param p_text : The text to display in the progress bar.
		 * @param p_percent: the percentage of the task completed. */
		void setUpdateProgressData(Glib::ustring p_text, double p_percent);
		
		/* bool idleWatchProgress:
		 * uses an Idle signal to watch the progress and update the progress bar.*/
		 bool idleWatchProgress();
		
		/* Convenience function: checks if the thread is currently busy. */
		bool getThreadIsBusy();
		
		/* THREAD FUNCTION: Instigates the installation process. 
		 * Uses the installStatus as a starting point, then executes the remaining steps.*/
		 void threadTask_install();
		
		/* threadTask_install related variable:
		 * Used to display how many errors occured.*/
		unsigned int v_installErrorCount = 0;
		
		/* threadTask_install related variable:
		* Text to display in the event of an error during install. */
		Glib::ustring v_installErrorMessage = "";
		
		/* threadTask_install related variable:
		* An error occured during the copying stage. */
		 bool v_errorOccuredDuringCopy = false;

		/* THREAD FUNCTION: Copies the engine source code from the launcher repository.
		// This function should only be called when creating, or within, a thread!
		 * @return Returns false if error occured.*/
		bool threadTask_copySource();
		
		/* THREAD FUNCTION: Reverts the copied source code to the specified commit.
		// This function should only be called when creating, or within, a thread!
		 * @return Returns false if error occured.*/
		bool threadTask_revertToCommit();

		/* THREAD FUNCTION: Invokes the build script(2nd step) for the engine.
		// This function should only be called when creating, or within, a thread!
		 * @return Returns false if error occured.*/
		bool threadTask_runSetupScript();
		
		/* THREAD FUNCTION: Invokes the make script (1st step) for the engine.
		// This function should only be called when creating, or within, a thread!
		 * @return Returns false if error occured.*/
		bool threadTask_runMakeScript();

		
		// THREAD FUNCTION: Removes the engine installation from disk.
		// This function should only be called when creating, or within, a thread!
		void threadTask_removeEngine();
		
		/* THREAD FUNCTION: Removes the files within the directory while providing
		* a way to update the GUI to inform the user of the progress.
		* Recursively deletes by calling the function again if a directory is detected.
		* @param p_path : the path to instigate deletion loop on. */ 
		void threadTask_deleteEngineDir(Glib::ustring p_path);

		
		/* THREAD FUNCTION: Launches the UE4 editor.*/
		void threadTask_launchEditor(Glib::ustring p_params = "");

		
		// The number of files within the Engine install directory.
		// Used exclusively for threadTask_deleteEngineDir
		unsigned int v_deleteTotalFiles = 0;
		// The number of deleted files.
		// Used exclusively for threadTask_deleteEngineDir
		unsigned int v_deleteFilesRemoved = 0;
		
		// THREAD RELATED FUNCTION: Called when a thread task is completed: 
		// This function should NOT be used on installation sub-task functions.
		void threadTask_completed();
		
		// THREAD & NONTHREAD FUNCTION: Called when a thread Subtask is completed;
		// Increments the level bar to show tasks completed/remaining.
		void incrementTaskLevelBar();
		
		/* THREAD RELATED FUNCTION:  Called when a thread task fails. 
		   A return should typically be called either immediately or soon after this.
		 * @param p_guiDebugInfo : The text information to be passed to the GUI.
		 * @param p_consoleInfo : the text information to be passed to the console.
		*/ 
		void threadTask_failed(Glib::ustring p_guiDebugInfo, Glib::ustring p_consoleInfo);


	}; // END - LauncherEngine class.
	
	/* ENGINE PROPERTIES:
	 * A GUI bin representing the settings of an installed engine configuration,
	 * allowing the user to view/modify the settings. */
	class EngineProperties : public Gtk::Bin{
	public:
		EngineProperties();
		virtual ~EngineProperties();
		
		// Signal emitted when settings are to be closed.
		typedef sigc::signal<void> v_typeCloseSignal;
		v_typeCloseSignal v_closeSignal;
		
		// Sets the enginedata reference and propogates the GUI.
		void propogateData(EngineData* p_engineData);
		
	private:
		EngineData* ref_engineData; // Reference to the engine data object for the engine.
		Gtk::Grid v_mainGrid;	// The main grid of the bin; houses the mainNotebook and main buttons (save/close/cancel).
		Gtk::Notebook v_mainNotebook;	// The main notebook to seperate the sections.
		/* Grids representing the pages of the notebook. */ 
		Gtk::Grid	v_pageGeneral,
					v_pageLaunchOpts,
					v_pagePlugins,
					v_pageManage;

		// The main buttons for the window, for the user to cancel (and close); or save (and close) the properties window.
//		Gtk::Button btn_cancel, btn_save;
		DialogueControls v_windowControls;
			
		// The LABELS for the related fields in the GENERAL tab.
		Gtk::Label	txt_general_installStatusLabel,
					txt_general_configFileLabel,
					txt_general_installDirLabel,
					txt_general_customImgDirLabel,
					txt_general_labelLabel,
					txt_general_descriptionLabel,
					txt_general_gitCommitLabel,
					txt_general_versionLabel;
					
		Gtk::Image img_general_installStatus; // Image to add graphical clarity to install status.
		
		// GENERAL tab text items.
		Gtk::Label	txt_general_installStatus,
					txt_general_configFile,
					txt_general_installDir,
					txt_general_customImgDir,
//					txt_general_labelLabel,
//					txt_general_descriptionLabel,
					txt_general_gitCommit,
					txt_general_version;
		
		Gtk::Entry v_general_label;
		Gtk::Entry v_general_description;
		
		
// LAUNCH OPTS TAB

		// Button to add/remove the active tab.
		Gtk::Button btn_launchOpts_addNew,
					btn_launchOpts_remove,
					btn_launchOpts_save;
					
		// Notebook to display all the launch options.
		Gtk::Notebook v_launchOpts_notebook;
		
		// Array to store pointers to launchOpt objects.
		std::vector<EngineLaunchOpts*> v_launchOpts_array;

		void btn_launchOpts_addNew_clicked();
		void btn_launchOpts_remove_clicked();
		void btn_launchOpts_save_clicked();
		void launchOpts_updateLabel(Gtk::Widget* p_page, Glib::ustring p_newLabel);
		void updateLaunchButtons(); // Proxy.
		
	// FUNCTIONS
		// Removes the launch option pages.
		void clearLaunchOpts();
		// Prompts launch options to be saved.
		void saveLaunchOpts();
		// Prompts launch options to be loaded.
		void loadLaunchOpts();

// PLUGINS TAB


// MANAGE TAB
		// Labels for related fields in the MANAGE tab.
		Gtk::Label txt_manage_deleteLabel;
					
		// Button to delete JUST the config file (and consequently remove it from the launcher).
		DestructiveConfirmationButton btn_manage_deleteConfig;
		
		// Button to delete the engine.
		DestructiveConfirmationButton btn_manage_deleteEngine;
		
		
		// BUTTON FUNCTIONS.
		void btn_manage_deleteEngine_clicked();
		void btn_manage_deleteConfig_clicked();
		
		Gtk::FlowBox v_pageGeneralFlowbox,	// The main flowbox which houses the section grids.
					 v_pageLaunchOptsFlowbox;
		
		// BUTTON FUNCTIONS
		void btn_cancel_clicked();
		void btn_save_clicked();
		void btn_general_label_updated(); 
		
	 }; // END - Engine Properties class. 
	

	/* Base launch option. */
	class EngineLaunchOptBase : public Gtk::Grid{
	public:
		EngineLaunchOptBase();
		virtual ~EngineLaunchOptBase();
		
		// When true, v_signalUpdateCommand is NOT emitted.
		bool v_bDontUpdate = false;
		
		Glib::ustring getHeader(){return v_settingHeader;}
		void setIniReference(CSimpleIniCaseA* p_ref);
		
		// Returns the command for this option; or empty if this option is disabled.
		virtual Glib::ustring getCommand();
		
		// Reads the setting from file and updates the GUI appropriately.
		virtual void readSettings() =0;
		// Reads from the GUI and writes to file.
		virtual void writeSettings() =0;
		
		// Signal emitted when a button is updated: use to trigger an update to get the new command string.
		sigc::signal<void> v_signalUpdateCommand;
		
	protected:
		// Call the signal from inside here!
		virtual void buttonUpdated();
		
		//Reference to an ini loader object. 
		CSimpleIniCaseA* ref_ini;
		
		// The type of setting.
		LaunchOptionType v_type;
		
		// Strings to hold the setting header/entry name.
		Glib::ustring	v_settingHeader,
						v_settingName;
		
		// String to hold textual setting command.
		Glib::ustring v_stringSetting;
		
		// Int to hold numerical settings.
		int v_intSetting;
		
		// Writes the setting to file.
		void writeStrSetting(Glib::ustring p_setting);
		void writeIntSetting(int p_setting);
		void writeBoolSetting(bool p_setting);
		
		// Reads the setting from file.
		Glib::ustring readStrSetting();
		int readIntSetting();
		bool readBoolSetting();
	}; // END - Base launch option.



	/* OPT COMBOBOX.
	 * Each Combobox entry is a COMPLETE command.
	 */
	class EngineLaunchOptCombobox : public EngineLaunchOptBase{
	public:
		EngineLaunchOptCombobox( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_displayedLabel );
		virtual ~EngineLaunchOptCombobox();
		
		void addNewOption( Glib::ustring p_command, Glib::ustring p_displayedLabel );
		
		void readSettings();
		void writeSettings();
		Glib::ustring getCommand();

	private:
		Gtk::ComboBoxText btn_option;
		Gtk::Label txt_displayedLabel;
		// Used to update the command when a user changes the selection.
		void btn_option_changed();
//		void buttonUpdated();
		
	}; // END - COMBOBOX launch option.

	// OPT TOGGLE
	class EngineLaunchOptToggle : public EngineLaunchOptBase{
	public:
		EngineLaunchOptToggle( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_boolCommand, Glib::ustring p_displayedLabel );
		virtual ~EngineLaunchOptToggle();
		void readSettings();
		void writeSettings();
		Glib::ustring getCommand();
	private:
		Gtk::CheckButton btn_check;
//		void buttonUpdated();
	}; // END - TOGGLE launch option

	// OPT SPINBUTTON
	class EngineLaunchOptSpin : public EngineLaunchOptBase{
	public:
		EngineLaunchOptSpin( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_command, Glib::ustring p_displayedLabel );
		virtual ~EngineLaunchOptSpin();
		void readSettings();
		void writeSettings();
		Glib::ustring getCommand();
	private:
		Gtk::Label txt_displayedLabel;
		Gtk::SpinButton btn_spinEntry;
//		void buttonUpdated();
	}; // END - SPINBUTTON launch option

	// OPT ENTRY
	class EngineLaunchOptEntry : public EngineLaunchOptBase{
	public:
		EngineLaunchOptEntry( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_command, Glib::ustring p_displayedLabel );
		virtual ~EngineLaunchOptEntry();
		void readSettings();
		void writeSettings();
		Glib::ustring getCommand();
	private:
		Glib::ustring v_preCommand; // The command passed via parameter, and NOT the user given strSetting.
		Gtk::Label txt_displayedLabel;
		Gtk::Entry btn_entry;
//		void buttonUpdated();
	}; // END - ENTRY launch option


	/* ENGINE LAUNCH OPTIONS:
	 * A GUI object representing all the launch options available.
	 * They're seperated here in order to keep maintenance of options easier.*/
	class EngineLaunchOpts : public Gtk::Bin{
	public:
		/* Engine Launch Options: 
		 * @param p_dataRef: The engine data reference parent.
		 * @param p_indexPos: The index position of "launchOptions" within p_dataRef this set of options represents.*/
		EngineLaunchOpts( EngineData* p_dataRef, unsigned int p_indexPos );
		virtual ~EngineLaunchOpts();
		
		Gtk::Button btn_debug; // Debugging button.
		void btn_debug_clicked();
		
		// Returns the internal engine data reference.
		EngineData* getRefEngineData();
		
		/* Get Launch Options:
		 * @return Constructs the launch options string used when launching the Engine by recursively
		 * obtaining the results from each button.*/
		Glib::ustring getLaunchOptions();
		
		/* Get launch Options QUICK:
		 * Assuming "getLaunchOptions" has already been called, getLaunchOptQUICK simply
		 * gets text from the Gtk::Label (and truncates the "Command: " prependature.
		 * @return The constructed launch option string.*/
		Glib::ustring getLaunchOptionsQuick();
		
		/* Get Launch Label:
		 * @return The specified launch button label*/
		 Glib::ustring getLaunchOptLabel();
		 
		 // Get Config file path.
		 Glib::ustring getConfigFile();
		
		// Read configuration
		// Reads the ini file and sets the buttons accordingly.
		void readConfigFile();
		
		// Writes the config to a file and appends the entry in the EngineData ini.
		void writeConfigFile();
		
		// Signal to connect loading of settings to buttons.
		sigc::signal<void> v_signalLoadSettings;
		// Signal to connect saving of settings to buttons.
		sigc::signal<void> v_signalSaveSettings;
		
		// Signal for updating the tab label of a launchOpt.
		sigc::signal<void, Gtk::Widget*, Glib::ustring> v_signalUpdateTabName;
		
		// Function to disable the label: used for the default launch opts
		void disableLabel();
		void setDefaultFocus(); 
	private:
	
		// Updates the launch options and the command text fields.
		void updateLaunchOptions();

		// The Engine data these launch options are tied too. || UNUSED
		EngineData* ref_engineData;
		
		// Path to the config file these options are associated with.
		Glib::ustring v_configFile;
		
		// The ini object for this option object; passed by reference to each button.
		CSimpleIniCaseA v_ini;
		
		// Label for similar named entry.
		Gtk::Label	txt_optionLabel,
					txt_optionManual,
					txt_outputLabel,
					txt_command;
		// Entry for user to specify the name of the options.
		Gtk::Entry	btn_optionLabel,
					btn_optionManual;

		
		// Scrolled window for the window in order to maintain a clean UI
		// regardless of where the LaunchOpts are.
		Gtk::ScrolledWindow v_mainScrollWindow;
		
		// The main grid.
		Gtk::Grid v_mainGrid;
		
		// Expanders for the sections.
		Gtk::Expander	v_expander_modes,
						v_expander_url,
						v_expander_dev,
						v_expander_render,
						v_expander_network,
						v_expander_user,
						v_expander_server,
						v_expander_gameStats,
						v_expander_configFiles,
						v_expander_misc;
		
		// Grid for the sections.
		Gtk::Grid	v_grid_modes,
					v_grid_url,
					v_grid_dev,
					v_grid_render,
					v_grid_network,
					v_grid_user,
					v_grid_server,
					v_grid_gameStats,
					v_grid_configFiles,
					v_grid_misc;
		
		// Convenience function for applying consistent grid settings.
		void setGridSettings(Gtk::Grid* p_grid);
		
		// Vector array to hold references to all buttons, so that operations can be iterated over all of them.
		std::vector<EngineLaunchOptToggle*> v_arrayToggleButtons; 
		std::vector<EngineLaunchOptEntry*> v_arrayEntryButtons;
		std::vector<EngineLaunchOptCombobox*> v_arrayComboButtons;
		std::vector<EngineLaunchOptSpin*> v_arraySpinButtons;
		
	// MAIN FUNCTIONS
	
		void btn_optionLabel_changed();
	
		// Recursively sets up all buttons.
		void setupButtons();
		
		// Adds a button to a grid based on its header
		void addButtonToGrid(EngineLaunchOptBase* p_button);
		
		// [INTERNAL] Prompts all buttons to load their settings.
		void loadSettings();
		
		// [INTERNAL] Saves all button settings to file.
		void saveSettings();
		
		// Reads the settings of all buttons and returns the generated the launch command.
		Glib::ustring generateLaunchCommand();
		bool v_isReadingData; // Bool used by generateLaunchCommand & loadSettings: returns from function if true to avoid unessecery repetative calls.
		
	// MODES
		EngineLaunchOptCombobox btn_modes_renderer,
								btn_modes_type;
	// URL
		EngineLaunchOptEntry	btn_url_game,
								btn_url_name;
		
		EngineLaunchOptToggle	
			btn_url_listen,
			btn_url_isLanMatch,
			btn_url_isFromInvite,
			btn_url_spectatorOnly;
		
	// DEV
		EngineLaunchOptToggle	
			btn_dev_log,
			btn_dev_allusers,
			btn_dev_auto,
			btn_dev_autoCOPackages,
			btn_dev_biasCompression,
			btn_dev_buildMachine,
			btn_dev_bulkImportingSound,
			btn_dev_nativeClassSize,
			btn_dev_coderMode,
			btn_dev_compatScale,
			btn_dev_cookDemo,
			btn_dev_cookPakages,
			btn_dev_crashReports,
			btn_dev_d3dDebug,
			btn_dev_devCon,
			btn_dev_dumpFileStats,
			btn_dev_fixedSeed,
			btn_dev_fixupTangents,
			btn_dev_forceLogFlush,
			btn_dev_forcePVRTC,
			btn_dev_forceSoundRecook,
			btn_dev_genericBrowser,
			btn_dev_installed,
			btn_dev_lightmassDebug,
			btn_dev_lightmassStat,
			btn_dev_noConform,
			btn_dev_noContentBrowser,
			btn_dev_noInnerException,
			btn_dev_noLoadStartupPackages,
			btn_dev_noPause,
			btn_dev_noPauseOnSuccess,
			btn_dev_noRC,
			btn_dev_noVerifyGC,
			btn_dev_noWrite,
			btn_dev_seekFreeLoading,
			btn_dev_sekFreePackageMap,
			btn_dev_seekFreeLoadingPC,
			btn_dev_seekFreeLoadingServer,
			btn_dev_setThreadNames,
			btn_dev_showMissingLoc,
			btn_dev_silent,
			btn_dev_traceAnimUsage,
			btn_dev_loadWarnAsErrors,
			btn_dev_unattended,
			btn_dev_useUnpublished,
			btn_dev_vaDebug,
			btn_dev_verbose,
			btn_dev_verifyGC,
			btn_dev_warnsAsErrors;

		EngineLaunchOptEntry
			btn_dev_automatedMapBuild,
			btn_dev_conformDir,
			btn_dev_cultureForCook;
		
		EngineLaunchOptCombobox 
			btn_dev_firewall,
			btn_dev_logTimes, // Default, force on, force off.
			btn_dev_installGE; // Install or uninstall GE from game explorer.


	// RENDERING:
		EngineLaunchOptSpin	
			btn_render_consoleX,
			btn_render_consoleY,
			btn_render_winX,
			btn_render_winY,
			btn_render_gameResX,
			btn_render_gameResY,
			btn_render_benchmarkFPS,
			btn_render_seconds;
			
		EngineLaunchOptToggle
			btn_render_benchmark,
			btn_render_dumpMovie;
			
		EngineLaunchOptCombobox
			btn_render_vsync,
			btn_render_windowMode;
			
		EngineLaunchOptEntry btn_render_exec;
		
	// NETWORK
		EngineLaunchOptToggle
			btn_network_lanPlay,
			btn_network_limitClientTicks,
			btn_network_multiHome,
			btn_network_networkProfiler,
			btn_network_noSteam,
			btn_network_primaryNet;
		EngineLaunchOptSpin btn_network_port;
		
	// USER
		EngineLaunchOptToggle
			btn_user_noHomeDir,
			btn_user_noForceFeedback,
			btn_user_noSound,
			btn_user_noSplash,
			btn_user_noTextureStream,
			btn_user_oneThread,
			btn_user_useAllAvailableCores;
		
		EngineLaunchOptEntry
			btn_user_paths,
			btn_user_preferredProcessor;
			
		// SERVER
		EngineLaunchOptEntry
			btn_server_login,
			btn_server_pass;
			
		// GAME STATS
		EngineLaunchOptToggle
			btn_gameStat_noDatabase,
			btn_gameStat_noLiveTags;
			
		// CONFIGS
		EngineLaunchOptToggle
			btn_config_englishLocal,
			btn_config_noAutoIniUpdate,
			btn_config_noIni,
			btn_config_regenerateInis;
			
		EngineLaunchOptEntry
			btn_config_iniDefaultEd,
			btn_config_iniEditor,
			btn_config_iniDefaultEdSettings,
			btn_config_iniEditorUserSettings,
			btn_config_iniDefaultCompat,
			btn_config_iniCompat,
			btn_config_iniDefaultLightmass,
			btn_config_iniLightmass,
			btn_config_iniDefaultEngine,
			btn_config_iniEngine,
			btn_config_iniDefaultGame,
			btn_config_iniGame,
			btn_config_iniDefaultInput,
			btn_config_iniInput,
			btn_config_iniDefaultUI,
			btn_config_iniUI;
			
		// DEBUGGING
		EngineLaunchOptToggle
			btn_debug_cpuProfileTrace,
			btn_debug_statNamedEvents,
			btn_debug_fileTrace;
		
		EngineLaunchOptEntry btn_debug_traceHost;
	}; // END - Engine launch options.


} // END - Unrealated launcher namespace.


#endif