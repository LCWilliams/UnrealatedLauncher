#ifndef LAUNCHER_COMMUNITY
#define LAUNCHER_COMMUNITY

#include <gtkmm-3.0/gtkmm.h>

namespace UnrealatedLauncher{
	
	class LauncherCommunity : public Gtk::Bin{
	public:
		LauncherCommunity();
		virtual ~LauncherCommunity();
		
	private:
		
		
	}; // END - Community tab bin.
	
} // END - Unrealated launcher namespace.

#endif