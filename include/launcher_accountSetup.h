#ifndef LAUNCHER_ACCOUNTSETUP
#define LAUNCHER_ACCOUNTSETUP

#include <iostream>
#include <webkit2/webkit2.h>
#include <gtkmm-3.0/gtkmm.h>

namespace UnrealatedLauncher{
	
	// Bin containing the launchers account setup.
	class LauncherAccountSetup : public Gtk::Bin{
	public:
		LauncherAccountSetup();
		virtual ~LauncherAccountSetup();
		
		// The account setup bin's main grid.
		Gtk::Grid v_mainGrid;
		// Close button public so its signals can be externaly connected.
		Gtk::Button btn_close;
		
		// Progress bar for the webpage; public for callback functions.
		Gtk::ProgressBar v_webProgressbar;
		
		// Signal emitted when user reaches final step and presses continue.
		sigc::signal<void> v_signalCompleted;
		
	private:
		// Shows the stages of the setup wizard.
		Gtk::LevelBar v_statusProgressBar;
		
		// Checkbuttons for the three stages of account setup, used to show/hide the relevent steps
		Gtk::CheckButton	btn_stepEpicAccount,
							btn_stepGithubAccount,
							btn_stepAccountLinkage;
							
		// Buttonbox to hold the step checkbuttons.
		Gtk::ButtonBox v_stepButtonbox;
		// Window buttons to move back, forward and close the wizard.
		Gtk::Button btn_back,
					btn_next;
		// Buttonbox for the back/next/close navigation buttons.
		Gtk::ButtonBox v_navigationButtonbox;
		
		// Wrapper widget for attaching the webpage view to the program.
		Gtk::Widget* v_webpageWidget;

		// The webkit view object.  Webkit calls should reference this object.
		WebKitWebView* v_webkitWebview;
		/* Current active page:
		 * 0 - Information
		 * 1 - Epic account setup.
		 * 2 - Github account setup.
		 * 3 - Epic & Github linking.
		*/
		unsigned int v_activePage = 0;
		// Boolean to determine if step-skipping increments or decrements the value, set during button clicks.
		bool v_incrementingSkips = true;
		

		
		// Text view to display the helper text buffer.
		Gtk::TextView v_helpTextView;
		// Text buffer.
		Glib::RefPtr<Gtk::TextBuffer> v_helpTextBuffer;
		
		// Updates the active page URI and navigation buttons.
		void updateWizard();
		
		// Convenience function: updates the "nextPage" counter based on the direction bool.
		void updateNextPageCounter();
		
		
		// Signal functions for the forward/backward navigation buttons.
		void btn_back_clicked();
		void btn_next_clicked();
		
		// Signal functions for account creation checkboxes.
		void btn_accountCreation_clicked();
		
	}; // END - Launcher Account setup class.
	
} // END - unrealated Launcher namespace.

#endif