#ifndef LAUNCHER_PRINTER
#define LAUNCHER_PRINTER
#include <gtkmm-3.0/gtkmm.h>
#include <iostream>
#include <ctime>

namespace UnrealatedLauncher{
	
	/* LAUNCHER PRINTER.
	 * A convenience class for providing pre-formatted output in std::cout
	 * and std::cerr.
 */
class LauncherPrinter{
	public:
		LauncherPrinter();
		virtual ~LauncherPrinter();
		
		/*debug:
		 * Normal debugging output.
		 * @param p_debugMsg: The message to print. */
		void debug(Glib::ustring p_debugMsg);
		
		/* Append Debug:
		 * Appends the message without precurser text.
		 * @param p_debugMsg: The message to print. */
		void appendDebug(Glib::ustring p_debugMsg);
		
		/* Error Message 
		 * Prints an error message to std::cerr.
		 * @param p_errorMsg: the message to print.*/
		void errorMsg(Glib::ustring p_errorMsg);
		
		/* Warn Message 
		 * Prints a warning message to std::cout.
		 * Ideal for non-breaking warnings.
		 * @param p_warnMsg: the message to print.*/
		void warnMsg(Glib::ustring p_warnMsg);
		
		/* Warn Error Message 
		 * Prints a warning message to std::cerr.
		 * Ideal for more problematic warnings that may inhibit behaviour.
		 * @param p_warnMsg: the message to print.*/
		void warnErrMsg(Glib::ustring p_warnMsg);
		
	private:
	
		time_t v_time; // Time.
		
		/* Gets the current local time. */
		Glib::ustring getTime();
		
}; // END - LauncherPrinter class.
	
} // END- ULauncher namespace.

#endif