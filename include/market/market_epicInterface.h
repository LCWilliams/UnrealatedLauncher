#ifndef MARKET_EPICINTERFACE
#define MARKET_EPICINTERFACE

#include "gtkmm-3.0/gtkmm.h"
#include "launcher_utilities.h"
#include <curlpp/cURLpp.hpp>

namespace UnrealatedLauncher{
	
	class LauncherEpicInterface{
	public:
		LauncherEpicInterface();
		virtual ~LauncherEpicInterface();
		
		// Test function, remove when done.
		void LEI_TESTFUNC();
		
		/* Set User Credentials:
		 * @param p_email : The users email.
		 * @param p_password : The users Password.*/
		void setUserCredentials(Glib::ustring p_email, Glib::ustring p_password);

		// Get Credentials - USER: Returns the users email.
		Glib::ustring getCredentialsUser();

		// Get Credentials - PASS: Returns the users password.
		Glib::ustring getCredentialsPass();
		
		/* Authenticate User
		 * Triggers authentication; includes triggering login prompt. 
		 * @return True when user is succesfully authenticated.*/
		bool authenticateUser();
		
		/* Cancel Authenticate User
		 * Used to cancel the authentication wait loop.*/
		void cancelAuthenticateUser();
		
		/* SIGNAL: Update Progress 
		 * Signal called after thread-tasks internally update the task progress variables. 
		 * @param double : The progress percent, where applicable. 
		 * @param Glib::ustring : Progress text, where applicable. */
		sigc::signal<void, double, Glib::ustring> sig_updateProgress;

		/* SIGNAL: Prompt Login. 
		 * Signal called when a task requires authentification and the current
		 * auth details are incorrect/not yet supplied. */
		Glib::Dispatcher sig_promptLogin;



	protected:
	
		const std::string v_arkoseLabsPublicKey = "37D033EB-6489-3763-2AE1-A228C04103F5";
	// URLs
		const std::string 
			v_url_loginFormSubmit = "https://www.unrealengine.com/id/api/login/",
			v_url_loginForm = "https://www.unrealengine.com/id/login",
			v_url_authenticate = "https://www.unrealengine.com/id/api/authenticate",
			v_url_csrf = "https://www.unrealengine.com/id/api/csrf",
			v_url_refreshCsrf = "https://www.epicgames.com/account/v2/refresh-csrf",
			v_url_getAccInfo = "https://www.unrealengine.com/marketplace/api/getAccountInfo",
			v_url_authorise = "https://accounts.unrealengine.com/authorize/index",
			v_url_captcha = "https://epic-games-api.arkoselabs.com/fc/gt2/public_key/" + v_arkoseLabsPublicKey;


	private:
		LauncherPrinter v_printer; // Debug/Console printer.
		Glib::ustring v_username, v_password;
		
//		curl::curl_easy v_easyCurl;
//		curl::curl_easy_exception *v_easyCurlExcption;
		
		// When true, thread-tasks requiring login are halted until this is false.
		bool v_waitingOnLoginInput = true;
		// When false, the last login attempt failed: assuming login info is incorrect.
		bool v_lastLoginSuccessful = false;
		// While loop timeout; when true- the user has cancelled authentication.
		bool v_userCancelledAuth = false;

		// INTERNAL FUNCTIONS
	}; // END - Launcher Epic interface.
	
	class LauncherMarketInterface : LauncherEpicInterface{
		
	}; // END - Launcher market Interface.

} // END - Unrealated Launcher namespace.

#endif
