#include "include/library/library_repoManager.h"
#include "include/library/library_addEngineGUI.h"
#include "include/library/library_engine.h"
#include "include/launcher_miniGUI.h"
#include "include/launcher_printer.h"

#include <gtkmm-3.0/gtkmm.h>
#include <webkit2/webkit2.h>

namespace UnrealatedLauncher{

	/* LAUNCHER LIBRARY:
	 * Class containing the launcher's library page and its functions.
	*/
	class LauncherLibrary : public Gtk::Bin{
	public:
		LauncherLibrary();
		virtual ~LauncherLibrary();

	// A repository manager object (NOT THE GUI!).
	LauncherRepositoryManager* ref_repositoryManager;
	
		/* Function to take the addEngineGUI.EngineData reference and create a new engine using it.
		 * Calls returnToLibrary to hide AddEngine GUI.*/
		void addNewEngine();
		
		/* Open Add new Engine:
		 * Public function to open the Add Engine GUI.*/
		void openAddNewEngine();
		
		void removeEngine(LauncherEngine* ref_engineBlock);
		

	private:
		// log printer.
		LauncherPrinter v_logger;
		// GUI repo manager:
		LauncherRepoManagerGUI* v_repoManagerGUI;
		// GUI for adding a new engine.
		LauncherAddEngine* ref_addEngineGUI;
		// GUI for engine properties.
		EngineProperties* ref_enginePropertiesGUI;
	
		// Stack for the Library window, to house the various library views (main library, repo manager, add engine, etc)
		Gtk::Stack v_libraryStack;

		// The main grid of the library window.
		Gtk::Grid v_mainGrid;
		
		// Decorative frame for installed engines.
		Gtk::Frame v_enginesFrame;
		// The grid housing all elements relating to enines; held within the decorative frame.
		Gtk::Grid v_enginesGrid;
		// An actionbar housing buttons relating to engines.
		Gtk::ActionBar v_enginesActionbar;
		// The flowbox element to hold installed engine blocks.
		Gtk::FlowBox v_enginesFlowbox;
		// A scrolled window to contain the flowbox.
		Gtk::ScrolledWindow v_enginesScrollWindow;
		
		Gtk::ProgressBar v_repoManagerProgress;	// A progress bar for the manager repo, to display progress while repomanager is closed.
		
		// Button to add a new engine.  Opens a wizard.
		Gtk::Button btn_addEngine;
		// Button to open the repository manager window.
		Gtk::Button btn_repoManager;

		// Button signal functions:
		void btn_repoManager_clicked();
		void btn_addEngine_clicked();
		// Returns the stack switcher to the main library.
		void returnToLibrary();
		
		/* Show Engine Properties:
		 Swaps display to show engine properties. */
		void showEngineProperties(LauncherEngine* p_engineGUI);
		
		// Updates the repomanager progressbar- displayed on the library page when the manager is closed.
		void updateRepoManagerProgress(Glib::ustring p_text, double p_progress);
		
		// Convenience function to add the engineblock to the flowbox.
		void addEngine(EngineData* ref_engineBlock);
		
		// Reads the config files and adds the found engines.
		void addExistingEngines();
		
		
		
	}; // END - Launcher Library.
	


} // END - UnrealatedLauncher namespace.