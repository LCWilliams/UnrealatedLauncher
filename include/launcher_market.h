#ifndef LAUNCHER_MARKET
#define LAUNCHER_MARKET

#include <gtkmm-3.0/gtkmm.h>
#include "include/launcher_webView.h"
#include "market/market_epicInterface.h"
#include "launcher_utilities.h"
#include "json.hpp"

namespace UnrealatedLauncher{
	
	/* LAUNCHER MARKET:
	 * The Launchers' market page GUI. */
	class LauncherMarket : public Gtk::Bin{
	public:
		LauncherMarket();
		virtual ~LauncherMarket();
		
	private:
	
	Gtk::Grid v_mainGrid;
	
	LauncherWebview v_webView;
	
	Gtk::ScrolledWindow v_itemScrollWin,
						v_fullViewScrollWin;
	
	// A flowbox to house the marketplace items.
	Gtk::FlowBox v_itemFlowbox;
	
	// Revealer to show/hide the full view dialogue.
	Gtk::Revealer v_fullViewRevealer;
		
	// Just a temp stuff for testing.
	Gtk::Button btn_testStuff;
	void btn_testStuff_clicked();
	
	}; // END - Launcher market class.



	/* MARKET ITEM DATA:
	 * An object representing a market items DATA.*/
	class MarketItemData{
	};


	/* LAUNCHER MARKET ITEM:
	 * The GUI object responsible for displaying individual market items.*/
	class LauncherMarketItem : public Gtk::FlowBoxChild{
	public:
		LauncherMarketItem(MarketItemData* p_itemDataRef);
		virtual ~LauncherMarketItem();
		
		// A pointer reference to the item data being displayed.
		MarketItemData* ref_itemData;
		
		// Resets the displayed data.
		void refreshData();
		
	private:

		// The marketItems main grid.
		Gtk::Grid v_mainGrid;

		// preview image widget 
		Gtk::Image v_previewImage;
		
		Gtk::Label txt_title,
					txt_description;
		
		// Level bar to represent ratings.
		Gtk::LevelBar v_ratingBar;
		
		// Button to quickly download the item.
		Gtk::Button btn_quickDownload;
	}; // END - Launcher market Item
	


} // END - UnrealatedLauncher namespace.

#endif