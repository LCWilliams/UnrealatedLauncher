#ifndef LAUNCHER_UTILITIES
#define LAUNCHER_UTILITIES

// Global functions that serve as utility, such as repeated tasks.
// Launcher Utilities are ONLY for functions!

#include "include/launcher_printer.h"
#include <gtkmm-3.0/gtkmm.h>
#include <string>
#include <filesystem>
#include <cctype>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <errno.h>


namespace LauncherUtils{

	

// FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  FORWARD DECLARATIONS  

// NonVoid returns:
	// File Exists:  Returns true if a file is found at the given path.
	static bool fileExists(std::string p_path);

//	static void setLabelParameters(Gtk::Label* p_label, Gtk::Align, );
	
	/* Remove Element from String:
	 * Recursively removes any "element" from the given string.
	 * Element to remove: the elements of the string to be removed.
	 * Replacement: What the removed items are replaced with.*/
	static Glib::ustring removeElementFromString(Glib::ustring p_string, Glib::ustring p_elementToRemove, Glib::ustring p_replacement);
	

	/* GetRealPath:
	 * Returns the real path of a string:
	 * Returns NULL("") if real path not found or not given. */
	static Glib::ustring getRealPath(Glib::ustring p_path);

	/* Get File Count.
	 * Returns the number of files within a directory (recursive).
	 * @param p_path: The path to file count in. */
	static int getFileCount(Glib::ustring p_path);
	
	/* is Digits Only:
	 * Used to determine whether the passed string contains only UNSIGNED integer digits.
	 * @return True when there are only numerical digits. */
	static bool isOnlyDigits(Glib::ustring p_string);
	
	/* Remove File:
	 * Convenience function to include error handling.
	 * @param p_file : The file to be removed.
	 * @return : True if successful; false if errors occured.*/
	 static bool removeFile(Glib::ustring p_file);

// VOID returns:
	/* SetLabelTextParameters:
	 * Sets a series of parameters on a label entry in one line.
	 * @param p_label : The Gtk::Label object.
	 * @param p_alignment : the Gtk::Align to use (HORIZONTAL).
	 * @param p_padTop : how much padding to assign to the top.
	 * @param p_padBot : how much padding to assign to the bottom. */
	static void setLabelTextParameters(Gtk::Label* p_label, Gtk::Align p_alignment, unsigned int p_padTop, unsigned int p_padBot);

	/* SetLongLabelTextParameters:
	 * Sets a series of parameters on a label entry in one line. 
	 * See SetLabelTextParameters.
	 * @param p_ellipsize : sets the ellipsize mode to use.
	 * @param p_wrap : Sets whether the text wraps.
	 * @param p_lines : sets the number of lines. */
	 
	static void setLongLabelTextParameters(Gtk::Label* p_label, Pango::EllipsizeMode p_ellipsize, bool p_wrap, unsigned int p_lines, Gtk::Align p_alignment, unsigned int p_padTop, unsigned int p_padBot, unsigned int p_maxWidth);


// DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  DEFINITIONS  

// Non VOID returns:
	bool fileExists( std::string p_path ){
		std::ifstream temp_file( p_path.c_str() );
		return (bool)temp_file;
	} // END - File Exists.


	Glib::ustring removeElementFromString(Glib::ustring p_stringToChange, Glib::ustring p_elementToRemove, Glib::ustring p_replacement){
		// Local variable to store changes.
		Glib::ustring temp_stringToChange = p_stringToChange;
		// For loop: iterate through the string on positions where the elementToRemove is found.
		for(std::size_t positionIndex = temp_stringToChange.find( p_elementToRemove.c_str() ); positionIndex != Glib::ustring::npos; positionIndex = temp_stringToChange.find( p_elementToRemove.c_str(), positionIndex ) ){
				temp_stringToChange.replace(positionIndex, p_elementToRemove.length(), p_replacement);
		} // END - For loop.
		return temp_stringToChange;
	} // END - remove file from string.


	Glib::ustring getRealPath(Glib::ustring p_path){
		UnrealatedLauncher::LauncherPrinter v_logger;
		if(p_path.empty()){
			v_logger.warnErrMsg("Empty path was given!  Returning function with empty string!");
			return "";
		} // END - If path is empty.
		
		if(!fileExists(p_path)){
			v_logger.warnErrMsg("Path given does not exist! Returning function with empty string!");
			return "";
		}// END - If path doesn't exist
		
//		std::cout << "Getting real path of: " << p_path << " | ";
		
		// Create variable to hold the resolved path:
		char *temp_resolvedPath;
		// Variable to hold the path as a string:
		Glib::ustring temp_path = "";
		
		// Get real path:
		temp_resolvedPath = realpath(p_path.c_str(), NULL);
		
		
		if(temp_resolvedPath != NULL){
			temp_path = temp_resolvedPath;
			// Free up memory before leaving function, since realPath uses Malloc.
			free(temp_resolvedPath);
			return temp_path;
		} else {
			// Output Error information
			Glib::ustring temp_errorMsg = "An error occured getting the real path. ";
			temp_errorMsg += std::to_string(errno);
			v_logger.warnErrMsg(temp_errorMsg);

			// Free up memory before leaving function, since realPath uses Malloc.
			free(temp_resolvedPath);
			
			// Return an empty string:
			return "";
		}
	} // END - Get Real Path.
	
	
	int getFileCount(Glib::ustring p_path){
		UnrealatedLauncher::LauncherPrinter v_logger;
		// Safety check; ensure path exists.
		if(p_path.empty()){
			v_logger.warnErrMsg("Path provided to getFileCount is empty!");
			return 0;
		} // END - If path is empty
		
		if( !fileExists(p_path) ){
			v_logger.warnErrMsg("Path provided to getFileCount does not exist!");
			return 0;
		} // END - if path doesn't exist.
		
		// Int to hold files found.
		int v_filesFound = 0;

		DIR *v_directory = opendir(p_path.c_str());
		struct dirent *v_nextFile;
		char v_filePath[PATH_MAX];

		while( (v_nextFile = readdir(v_directory)) != NULL ){
			
			if( strcmp(v_nextFile->d_name, ".") && strcmp(v_nextFile->d_name, "..") ){
				sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

				// If next file is a directory, call function again
				if(v_nextFile->d_type == DT_DIR){
					v_filesFound += getFileCount( v_filePath );
					++v_filesFound; // Directories count as something to remove; for accurate file removal percentage it's incrmented here.
				} // END - if next file is a DIRECTORY
				else{
				// increment the file found counter.
				++v_filesFound;
				}// END - If file is NOT a directory.
			} // END - if next file name.

			sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);
		} // END - while directory isn't null

		closedir(v_directory);
	
		return v_filesFound;
	} // END - Get file count.
	
	
	bool isOnlyDigits(Glib::ustring p_string){
		for(unsigned int index = 0; index < p_string.size(); ++index){
			// Use boolean isDigit to do detecting int digits.
			// Return false if NOT a digit.
			if ( !isdigit(p_string[index]) )return false;
		} // END - Iterate over the digits within the word.
		return true; // Returns true if only digits were found.
	}// END - Is only digits.
	
	bool removeFile(Glib::ustring p_file){
		UnrealatedLauncher::LauncherPrinter v_logger;
		v_logger.debug("Removing file: " + p_file);
		
		if(fileExists(p_file)){
			try{
				std::filesystem::remove( p_file.c_str() );
				return true;
			} // END - Try removal.
			
			catch( const std::exception &error ){
				Glib::ustring temp_message = "Unable to remove file: " + p_file + " | ";
				temp_message += error.what();
				v_logger.errorMsg(temp_message);
				return false;
			} // END- Catch exception
			
		} else{
			v_logger.appendDebug("File does not exist.");
			return true;
		} // END - file doesn't exist. return true.
	} // END - remove file
	
// VOID RETURNS:
	void setLabelTextParameters(Gtk::Label* p_label, Gtk::Align p_alignment = Gtk::ALIGN_END, unsigned int p_padTop = 0, unsigned int p_padBot = 0){
		p_label->set_halign(p_alignment);
		p_label->set_margin_top(p_padTop);
		p_label->set_margin_bottom(p_padBot);
	} // END _ set label text parameters.
	
	void setLongLabelTextParameters(	Gtk::Label* p_label, 
											Pango::EllipsizeMode p_ellipsize = Pango::ELLIPSIZE_MIDDLE, 
											bool p_wrap = true, unsigned int p_lines = 1, 
											Gtk::Align p_alignment = Gtk::ALIGN_START, 
											unsigned int p_padTop = 0, 
											unsigned int p_padBot = 0,
											unsigned int p_maxWidth = 50){
		setLabelTextParameters(p_label, p_alignment, p_padTop, p_padBot);
		p_label->set_ellipsize(p_ellipsize);
		p_label->set_line_wrap(p_wrap);
		p_label->set_lines(p_lines);
		if(p_lines > 1){
			p_label->set_single_line_mode(false);
			p_label->set_max_width_chars(p_maxWidth);
		} // END - if p_lines is greater than 1.
	} // END - Set long label text parameters.

} // END - Launcher Utils namespace.


#endif