#ifndef LAUNCHER_SETTINGS_GUI
#define LAUNCHER_SETTINGS_GUI

#include "include/launcher_settings.h"
#include "include/launcher_miniGUI.h"
#include <gtkmm-3.0/gtkmm.h>


namespace UnrealatedLauncher{

	class LauncherSettingsGUI : public Gtk::Bin{
	public:
		LauncherSettingsGUI();
		virtual ~LauncherSettingsGUI();
		
		sigc::signal<void> v_signalCloseSettingsGUI;

		
		/* Updates the GUI elements using the LauncherSettings:: values. */
		void updateGuiFromSettings();
		
		/* Proxy to save settings to file. */
		void updateSettingsFromGui();
		
	private:
		/* main settings buttons:
		 * @btn_defaults - Button to reset settings to default values. Does not invoke save.
		 * @btn_cancel - Button to cancel any changes.  Invokes a read from LauncherSettings to reset gui values to those in use.
		 * @btn_confirm - Button to confirm changes.  Invokes a write to the config file AND LauncherSettings.
		*/
		DialogueControls v_windowControls;

		// The main grid for the settings window.
		Gtk::Grid v_mainGrid;
	
		Gtk::Notebook v_settingsNotebook;

		/* A label used to display updates and messages. */
		Gtk::Label	txt_settingsHelp;
		
		/* A spinner used to display activity. */
		Gtk::Spinner v_settingsHelpSpinner;
		
		// Logger for printing debug, warning and errors.
		LauncherPrinter v_logger;
		
		
	// GENERAL:
		/* A scrolled window to house the general settings grid. */
		Gtk::ScrolledWindow v_settingsGeneral_scrolledWindow;
		// Needs to be a reference, otherwise will cause segfault.
		Gtk::Grid	v_settings_general;
		
		/* Combo boxes.
		@btn_theme : theme selector.  0 indicates to use system theme.
		@btn_defaultPage: the default page the launcher opens onto.*/
		Gtk::ComboBoxText	btn_theme,
							btn_defaultPage;
							
		/* Labels for the same named buttons */
		Gtk::Label	txt_themeLabel,
					txt_defaultPageLabel,
					txt_general_defaultInstallDir,
					txt_general_marketDownloadDir;
					
		/* Button to allow choosing the folder for the default install directory. */
		Gtk::FileChooserButton btn_general_defaultInstallDir;
		
		// Button to enable or disable the timed buttons.
		Gtk::CheckButton btn_general_timedButtonEnabled;

		// Button to choose which folder downloaded market assets are stored in.
		Gtk::FileChooserButton btn_general_marketDownloadDir;

		
	// REPOSITORY MANAGER
		// Scrolled window to house the repo manager grid.
		Gtk::ScrolledWindow v_settingsRepoMan_scrWindow;
		
		// Grid for repo manager settings.
		Gtk::Grid v_settings_repoMan;
		
		// Labels for all the buttons/options.
		Gtk::Label	txt_repoMan_directory,
					txt_repoMan_authMethod,
					txt_repoMan_sshPublicDir,
					txt_repoMan_sshPrivDir,
					txt_repoMan_autoRepoUpdInterval;
		
		Gtk::ToggleButton btn_repoMan_authMethod;
		// Function called when button of same name is clicked; shows/hides SSH revealer.
		void btn_repoMan_authMethod_changed();
		
		// File chooser for the repo directory.
		Gtk::FileChooserButton btn_repoman_repoDir;
		
		// Revealer to show/hide the ssh sub-grid: keeps the code & GUI itself clean.
		Gtk::Revealer v_repoMan_sshRevealer;
		
		// The grid for the SSH suboptions.
		Gtk::Grid v_repoMan_sshGrid;
		
		// File choosers to allow the user to select their public & private SSH files.
		Gtk::FileChooserButton	btn_repoMan_sshPublicDir,
								btn_repoMan_sshPrivDir;
		
		// Button to toggle auto refreshing lists after repoman task.
		Gtk::CheckButton btn_repoMan_autoRefreshLists;
		
		// Button to en/disable automatic updating of launcher repository. 
		Gtk::CheckButton btn_repoMan_autoRepoUpdate;
		// The option to specify in minutes the time between update operations.
		Gtk::SpinButton btn_repoMan_autoRepoUpdateInterval;
		
		// Emits close signal.
		void btn_cancel_clicked();
		// Saves settings, then calls cancel to close the window.
		void btn_confirm_clicked();
		// Resets buttons to defualt.
		void btn_defaults_clicked();
	}; // END - Launcher Settings GUI
	
} // END - Unrealated Launcher Namespace.


#endif