#ifndef LAUNCHER_WEBVIEW
#define LAUNCHER_WEBVIEW

#include "include/launcher_utilities.h"
#include <gtkmm-3.0/gtkmm.h>
#include <webkit2/webkit2.h>
#include "launcher_settings.h"
#include <json.hpp>

namespace UnrealatedLauncher{
	
	/* Launcher Web View
	* An object for displaying and managing a website.*/
	class LauncherWebview : public Gtk::Bin{
	public:
	
		LauncherWebview();
		/* Launcher WebView 
		 * @param p_defaultURL The Default webpage to be displayed.*/
		LauncherWebview(Glib::ustring p_defaultURL);
		virtual ~LauncherWebview();

		LauncherPrinter v_logger;
		
		/* Load Page:
		 * Instigates the loading of a new page.
		 * @param p_newURL The URL of the webpage to load.*/
		void loadPage(Glib::ustring p_newURL);
		
		void setDefaultURL(Glib::ustring p_newURL);
		
	private:
	
		Glib::ustring v_defaultURL;

		/* Setup:
		 * Sets up the layout.  Called by the constructors for consistency.*/
		void setup();
		
		Gtk::Grid v_mainGrid;
		
		// Wrapper widget for attaching the webpage view to the program.
		Gtk::Widget* v_webpageWidget;

		// The webkit view object.  Webkit calls should reference this object: NOT its wrapper.
		WebKitWebView* v_webkitViewer;
		
		// Progres bar to show current operations in progres.
		Gtk::ProgressBar v_webProgress;
		
		// Only displayed when a default link is provided.
		Gtk::Button btn_home;
		
		/* Update Load Status:
		 * Updates the webview based on the status of the load event. */
		static void updateLoadStatus(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData);

		/* Update Load Progress:
		 * Updates the progres bar with the current load percentage. */
		static void updateLoadProgress(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData);
		
		void btn_home_clicked();

	}; // END - LauncherWebView.
	
} // END - ULauncher namespace.

#endif
