##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=ULauncherMainProgram
ConfigurationName      :=Debug
WorkspacePath          :=/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher
ProjectPath            :=/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=lee
Date                   :=27/06/19
CodeLitePath           :=/home/lee/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="ULauncherMainProgram.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            := -pthread -lstdc++ $(shell pkg-config --libs gtkmm-3.0)
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)/usr/include/ $(IncludeSwitch)/usr/include/webkitgtk-4.0/ $(IncludeSwitch)/usr/include/webkitgtk-4.0/webkit2 $(IncludeSwitch)/usr/include/libsoup-2.4/ $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)systemd $(LibrarySwitch)boost_system $(LibrarySwitch)boost_filesystem $(LibrarySwitch)crypto $(LibrarySwitch)z $(LibrarySwitch)ssl $(LibrarySwitch)webkit2gtk-4.0 
ArLibs                 :=  "libsystemd" "boost_system" "boost_filesystem" "libcrypto" "libz" "libssl" "libwebkit2gtk-4.0" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)lib/ $(LibraryPathSwitch)/usr/lib/ $(LibraryPathSwitch)/lib/x86_64-linux-gnu/ $(LibraryPathSwitch)/usr/include/ $(LibraryPathSwitch)/usr/include/libsoup-2.4/ $(LibraryPathSwitch)/usr/lib/x86_64-linux-gnu/ 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS := -std=c++17 -g -Wall -O0 $(shell pkg-config --cflags gtkmm-3.0) $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix): src/launcher_settings.cpp $(IntermediateDirectory)/src_launcher_settings.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_settings.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_settings.cpp$(DependSuffix): src/launcher_settings.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_settings.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_settings.cpp$(DependSuffix) -MM src/launcher_settings.cpp

$(IntermediateDirectory)/src_launcher_settings.cpp$(PreprocessSuffix): src/launcher_settings.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_settings.cpp$(PreprocessSuffix) src/launcher_settings.cpp

$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix): src/library/launcher_library.cpp $(IntermediateDirectory)/src_library_launcher_library.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/launcher_library.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_launcher_library.cpp$(DependSuffix): src/library/launcher_library.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_launcher_library.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_launcher_library.cpp$(DependSuffix) -MM src/library/launcher_library.cpp

$(IntermediateDirectory)/src_library_launcher_library.cpp$(PreprocessSuffix): src/library/launcher_library.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_launcher_library.cpp$(PreprocessSuffix) src/library/launcher_library.cpp

$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix): src/library/library_repoManager.cpp $(IntermediateDirectory)/src_library_library_repoManager.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_repoManager.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_repoManager.cpp$(DependSuffix): src/library/library_repoManager.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_repoManager.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_repoManager.cpp$(DependSuffix) -MM src/library/library_repoManager.cpp

$(IntermediateDirectory)/src_library_library_repoManager.cpp$(PreprocessSuffix): src/library/library_repoManager.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_repoManager.cpp$(PreprocessSuffix) src/library/library_repoManager.cpp

$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix): src/launcher_main.cpp $(IntermediateDirectory)/src_launcher_main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_main.cpp$(DependSuffix): src/launcher_main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_main.cpp$(DependSuffix) -MM src/launcher_main.cpp

$(IntermediateDirectory)/src_launcher_main.cpp$(PreprocessSuffix): src/launcher_main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_main.cpp$(PreprocessSuffix) src/launcher_main.cpp

$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix): src/launcher_settingsGUI.cpp $(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_settingsGUI.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(DependSuffix): src/launcher_settingsGUI.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(DependSuffix) -MM src/launcher_settingsGUI.cpp

$(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(PreprocessSuffix): src/launcher_settingsGUI.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_settingsGUI.cpp$(PreprocessSuffix) src/launcher_settingsGUI.cpp

$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix): src/launcher_mainWindow.cpp $(IntermediateDirectory)/src_launcher_mainWindow.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_mainWindow.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(DependSuffix): src/launcher_mainWindow.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(DependSuffix) -MM src/launcher_mainWindow.cpp

$(IntermediateDirectory)/src_launcher_mainWindow.cpp$(PreprocessSuffix): src/launcher_mainWindow.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_mainWindow.cpp$(PreprocessSuffix) src/launcher_mainWindow.cpp

$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix): src/launcher_accountSetup.cpp $(IntermediateDirectory)/src_launcher_accountSetup.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/launcher_accountSetup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(DependSuffix): src/launcher_accountSetup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(DependSuffix) -MM src/launcher_accountSetup.cpp

$(IntermediateDirectory)/src_launcher_accountSetup.cpp$(PreprocessSuffix): src/launcher_accountSetup.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_launcher_accountSetup.cpp$(PreprocessSuffix) src/launcher_accountSetup.cpp

$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix): src/library/library_engineBlock.cpp $(IntermediateDirectory)/src_library_library_engineBlock.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/UnrealatedLauncher/src/library/library_engineBlock.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(DependSuffix): src/library/library_engineBlock.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(DependSuffix) -MM src/library/library_engineBlock.cpp

$(IntermediateDirectory)/src_library_library_engineBlock.cpp$(PreprocessSuffix): src/library/library_engineBlock.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_library_library_engineBlock.cpp$(PreprocessSuffix) src/library/library_engineBlock.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


