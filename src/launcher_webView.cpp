#include "include/launcher_webView.h"

using namespace UnrealatedLauncher;

LauncherWebview::LauncherWebview(){
	setup();
	v_mainGrid.attach(*v_webpageWidget, 0, 0, 1, 1);
	v_mainGrid.attach(v_webProgress, 0, 1, 1, 1);
} // END - Constructor.

LauncherWebview::LauncherWebview(Glib::ustring p_defaultURL){
	setup();
	v_defaultURL = p_defaultURL;
	loadPage(p_defaultURL);
} // END - Constructor: Default URL

LauncherWebview::~LauncherWebview(){} // Deconstructor.


void LauncherWebview::setup(){
	add(v_mainGrid);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_row_homogeneous(false);
	
	btn_home.set_halign(Gtk::ALIGN_START);
	btn_home.set_label("Home");
	
	v_webProgress.set_halign(Gtk::ALIGN_FILL);
	v_webProgress.set_hexpand(true);
	v_webProgress.set_show_text(true);
	
	// Create new webkit webview object:
	v_webkitViewer = WEBKIT_WEB_VIEW( webkit_web_view_new() );
	// Wrap webkit object into widget:
	v_webpageWidget = Glib::wrap( GTK_WIDGET( v_webkitViewer ) );
	
	v_webpageWidget->set_hexpand(true);
	v_webpageWidget->set_vexpand(true);
	
	v_mainGrid.attach(*v_webpageWidget, 0, 0, 2, 1);
	v_mainGrid.attach(btn_home, 0, 1, 1, 1);
	v_mainGrid.attach(v_webProgress, 0, 1, 1, 1);

// Normal Signals:
	btn_home.signal_clicked().connect(sigc::mem_fun(*this, &LauncherWebview::btn_home_clicked));

// Webkit signals:
	g_signal_connect( G_OBJECT(GTK_WIDGET(v_webkitViewer)), "load-changed", G_CALLBACK(updateLoadStatus), this );
	g_signal_connect( G_OBJECT(GTK_WIDGET(v_webkitViewer)), "notify::estimated-load-progress", G_CALLBACK(updateLoadProgress), this );

} // END - Setup.


void LauncherWebview::loadPage(Glib::ustring p_newURL = ""){
	webkit_web_view_load_uri( v_webkitViewer, p_newURL.c_str() );
} // END - Load page.


void LauncherWebview::setDefaultURL(Glib::ustring p_newURL = ""){
	if(p_newURL.empty()){
		btn_home.set_sensitive(false);
	} else{ // END - If empty
		btn_home.set_sensitive(true);
	} // END - If new URL is NOT empty
	v_defaultURL = p_newURL;
} // END - Set default URL


void LauncherWebview::updateLoadProgress(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData){
	double temp_progress = webkit_web_view_get_estimated_load_progress(p_webView);
	
	((LauncherWebview*)p_userData)->v_webProgress.set_fraction(temp_progress);
	((LauncherWebview*)p_userData)->v_webProgress.set_text("Loading page....");
} // END - Update load progress.

void LauncherWebview::updateLoadStatus(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData){
	double temp_progress = webkit_web_view_get_estimated_load_progress(p_webView);
	((LauncherWebview*)p_userData)->v_webProgress.set_fraction(temp_progress);
	
	switch (p_loadEvent){
		case WEBKIT_LOAD_STARTED:
			((LauncherWebview*)p_userData)->v_webProgress.set_text("Page load initialised.");
		break;
		
		case WEBKIT_LOAD_COMMITTED:
			((LauncherWebview*)p_userData)->v_webProgress.set_text("Communication established.");
		break;
		
		case WEBKIT_LOAD_REDIRECTED:
			((LauncherWebview*)p_userData)->v_webProgress.set_text("Page redirected.");
		return;
		
		case WEBKIT_LOAD_FINISHED:
			((LauncherWebview*)p_userData)->v_webProgress.set_text("Load complete.");
		break;
	} // END - Switch event.
} // END - Update load status.


void LauncherWebview::btn_home_clicked(){
	loadPage(v_defaultURL);
} // END - Home button clicked.