#include "include/launcher_printer.h"
#include "include/launcher_utilities.h"

using namespace UnrealatedLauncher;

LauncherPrinter::LauncherPrinter(){
} // Constructor.

LauncherPrinter::~LauncherPrinter(){} // Destructor.

Glib::ustring LauncherPrinter::getTime(){
	v_time = time(0);
	Glib::ustring temp_convertedTime = ctime(&v_time);
	temp_convertedTime = "[" + LauncherUtils::removeElementFromString(temp_convertedTime, "\n", "") + "]";
	return temp_convertedTime;
} // END - getTime.

void LauncherPrinter::debug(Glib::ustring p_debugMsg){
	std::cout << getTime() << " \e[1m: \e[0m" << p_debugMsg << std::endl;
} // END - Debug message.

void LauncherPrinter::appendDebug(Glib::ustring p_message){
	std::cout << "			   > " << p_message << std::endl;
} // END - append debug message

void LauncherPrinter::warnMsg(Glib::ustring p_warnMsg){
	std::cout << getTime() << " \e[1;35mWARN:  \e[0m" << p_warnMsg << std::endl;
} // END - Warn message

void LauncherPrinter::warnErrMsg(Glib::ustring p_warnMsg){
	std::cerr << getTime() << " \e[1;5;33mWARN:  \e[0m" << p_warnMsg << std::endl;
} // END - Warn error message

void LauncherPrinter::errorMsg(Glib::ustring p_errorMsg){
	std::cerr << getTime() << " \e[1;31mERROR:  \e[0m" << p_errorMsg << std::endl;
} // END - Error message