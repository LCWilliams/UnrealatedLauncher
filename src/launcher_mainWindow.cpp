#include "include/launcher_main.h"

using namespace UnrealatedLauncher;

UnrealatedLauncherWindow::UnrealatedLauncherWindow():
menuItem_quit("_Quit", true),
menuItem_settings("_Settings", true),
menuItem_accountSetup("_Account Setup", true){

	// Set default size of window to (Width, Height).
	set_default_size(1000,500);
	set_title("UNREALATED LAUNCHER");
	
//	LauncherGlobal::ref_launcherMainWindow = dynamic_cast<Gtk::Window *>(this->get_ancestor(GTK_TYPE_WINDOW));
	
	// Adds the main grid to the window.
	add(v_mainGrid);
	
//	set_titlebar(v_mainButtonGrid); // testing things.
	// Attach the button grid.
	v_mainGrid.attach(v_mainButtonGrid, 0, 0, 1, 1);
		// Attach stack switcher to main button grid.
		v_mainButtonGrid.attach(btn_mainPagesStackSwitcher, 0, 0, 1, 1);
		v_mainButtonGrid.attach(btn_launcherMenu, 1, 0, 1, 1);
		btn_mainPagesStackSwitcher.set_size_request(-1, 50);
		
	// Attach the main stack to the grid.  This stack changes between the main pages stack, and the settings window.
	v_mainGrid.attach(v_mainStack, 0, 1, 1, 1);
		// Attach the two stacks (main pages and settings).
		v_mainStack.add(v_mainPagesStack);
		v_mainStack.add(v_launcherSettings); // Add settings page here.
		v_mainStack.set_transition_type(Gtk::STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT);
		v_mainStack.set_transition_duration(500);
		
	// Attach the account setup to the main stack.
		v_mainStack.add(v_accountSetupBin, "", "ACCOUNTSETUP");
		

	// Attach main pages to the main page stack:
	v_mainPagesStack.add(v_launcherLibrary, "launcherLibrary", "LIBRARY");
	v_mainPagesStack.add(v_launcherMarket, "launcherMarket", "MARKET");
	v_mainPagesStack.add(v_launcherCommunity, "launcherCommunity", "COMMUNITY");
		v_mainPagesStack.set_transition_duration(150);
		v_mainPagesStack.set_transition_type(Gtk::STACK_TRANSITION_TYPE_CROSSFADE);
	
	// Connect stack switcher to the main page stack.
	btn_mainPagesStackSwitcher.set_stack(v_mainPagesStack);
		// Hexpand + Homogenous ensures the buttons evenly span accross all available space.
		btn_mainPagesStackSwitcher.set_hexpand(true);
		btn_mainPagesStackSwitcher.set_homogeneous(true);


	// Launcher Menu:
	btn_launcherMenu.set_menu(menu_launcherMenu);
	menu_launcherMenu.append(menuItem_settings);
	menu_launcherMenu.append(menuItem_quit);
	menu_launcherMenu.prepend(menuItem_accountSetup);
		menuItem_accountSetup.set_tooltip_text("Opens a wizard to set up your Github and Epic accounts in order to download the Unreal Engine.");
	
	menu_launcherMenu.show_all();
	
	menuItem_accountSetup.signal_activate().connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::menuItem_accountSetup_clicked));
	menuItem_settings.signal_activate().connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::menuItem_settings_clicked));
	menuItem_quit.signal_activate().connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::menuItem_quit_clicked));
	
//	v_launcherSettings.btn_cancel.signal_clicked().connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::settingsSignalClosed));
	v_launcherSettings.v_signalCloseSettingsGUI.connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::settingsSignalClosed));
	v_accountSetupBin.btn_close.signal_clicked().connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::accountSetupSignalClosed));
	v_accountSetupBin.v_signalCompleted.connect(sigc::mem_fun(*this, &UnrealatedLauncherWindow::accountSetupSignalCompleted));

	show_all();

// Must occur after showing, else it doesn't work!
	switch (LauncherSettings::defaultPage) {
		case 0: v_mainPagesStack.set_visible_child("launcherLibrary");
		break;
		
		case 1: v_mainPagesStack.set_visible_child("launcherMarket");
		break;
		
		case 2: v_mainPagesStack.set_visible_child("launcherCommunity");
		break;

		default: //v_mainPagesStack.set_visible_child(v_launcherLibrary);
		v_logger.warnErrMsg("Invalid default page set!");
		break;
	} // END - Default page switch.
} // END - Unrealated Launcher Window

UnrealatedLauncherWindow::~UnrealatedLauncherWindow(){
} // END - Unrealated launcher window destructor.

// BUTTONS:

void UnrealatedLauncherWindow::settingsSignalClosed(){
	set_title("UNREALATED LAUNCHER");
	v_mainStack.set_visible_child(v_mainPagesStack);
	btn_mainPagesStackSwitcher.set_sensitive(true);
} // END - Settings signal closed.

void UnrealatedLauncherWindow::accountSetupSignalClosed(){
	v_mainStack.set_visible_child(v_mainPagesStack);
	btn_mainPagesStackSwitcher.set_sensitive(true);
} // End - Account setup signal closed.

void UnrealatedLauncherWindow::accountSetupSignalCompleted(){
	accountSetupSignalClosed();
	v_launcherLibrary.openAddNewEngine();
} // END - Account setup completed.

void UnrealatedLauncherWindow::menuItem_quit_clicked(){
	v_logger.debug("Attempting close!");
	
	// FUTURE:
	// Check if inhibited/busy task is in progress, ask to:
	// Force close anyway: messy.
	// Continue running threadded tasks in the background.
	// Wait to close. (and cancel).
	
	// If first run is true, set to false & save.
	if(LauncherSettings::firstRun){
		LauncherSettings::firstRun = false;
		LauncherSettings::writeSettingsToFile();
	} // END - if first run is true.
	
	// Get the "application" and release.
	this->get_application()->release();
	
} // END - menu item quit.

void UnrealatedLauncherWindow::menuItem_settings_clicked(){
	set_title("UNREALATED LAUNCHER - Settings");
	v_mainStack.set_visible_child(v_launcherSettings);
	btn_mainPagesStackSwitcher.set_sensitive(false);
	// Call to read settings (in the event the user sets default: then cancels).
	v_launcherSettings.updateGuiFromSettings();
} // END - Menu Item settings clicked.

void UnrealatedLauncherWindow::menuItem_accountSetup_clicked(){
	v_mainStack.set_visible_child(v_accountSetupBin);
	btn_mainPagesStackSwitcher.set_sensitive(false);
}