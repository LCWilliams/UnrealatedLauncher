#include "include/launcher_accountSetup.h"

using namespace UnrealatedLauncher;

// CALLBACK FUNCTIONS:
void webpageLoaded(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData){
	switch (p_loadEvent){
		
		case WEBKIT_LOAD_STARTED:
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_fraction(0);
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_text("Initialised page load!");
		break;
		
		case WEBKIT_LOAD_COMMITTED:
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_text("Communication established!");
		break;
		
		case WEBKIT_LOAD_REDIRECTED:
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_text("PAGE WAS REDIRECTED!");
		return;
		
		case WEBKIT_LOAD_FINISHED:
//			double temp_progress = webkit_web_view_get_estimated_load_progress(p_webView);
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_fraction(1);
			((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_text("Load complete!");
		break;
	} // END - Switch event.
} // END - Callback webpage Loaded.

void webpageProgress(WebKitWebView *p_webView, WebKitLoadEvent p_loadEvent, gpointer p_userData){
		double temp_progress = webkit_web_view_get_estimated_load_progress(p_webView);
		((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_fraction(temp_progress);
		((LauncherAccountSetup*)p_userData)->v_webProgressbar.set_text("Loading page....");
	} // END - Webpage progress callback.

LauncherAccountSetup::LauncherAccountSetup():
	btn_close("Close"),
	btn_stepEpicAccount("created an epic games account"),
	btn_stepGithubAccount("created a github account"),
	btn_stepAccountLinkage("linked the two accounts"),
	
	btn_back("Back"),
	btn_next("Next")
{
	// Add the main grid to the bin:
	add(v_mainGrid);
	v_mainGrid.set_hexpand(true);
	v_mainGrid.set_vexpand(true);
	
	// Create webkit webview object:
	v_webkitWebview = WEBKIT_WEB_VIEW( webkit_web_view_new() );
	// Wrap webkit object into widget:
	v_webpageWidget = Glib::wrap( GTK_WIDGET( v_webkitWebview ) );
	v_webpageWidget->set_hexpand(true);
	v_webpageWidget->set_vexpand(true);
	v_webpageWidget->set_margin_top(15);
	v_webpageWidget->set_margin_left(15);
	v_webpageWidget->set_margin_right(15);
//	v_webpageWidget->set_margin_bottom(15);
	// Prevents the webpage widget from showing on the first page.
	v_webpageWidget->set_no_show_all(true);

	// Add steps to buttonbox:
	v_stepButtonbox.pack_start(btn_stepEpicAccount, true, true, 0);
	v_stepButtonbox.pack_start(btn_stepGithubAccount, true, true, 0);
	v_stepButtonbox.pack_start(btn_stepAccountLinkage, true, true, 0);
	v_stepButtonbox.set_orientation(Gtk::ORIENTATION_VERTICAL);
	
	// Add navigation buttons to buttonbox:
	v_navigationButtonbox.pack_start(btn_back,true, true, 0);
	v_navigationButtonbox.pack_start(btn_close, true, true, 0);
	v_navigationButtonbox.pack_start(btn_next, true, true, 0);



	// Setup status progress bar.
	v_mainGrid.attach(v_statusProgressBar, 0, 0, 1, 1);
	v_statusProgressBar.set_mode(Gtk::LEVEL_BAR_MODE_DISCRETE);
	
	// Setup help text buffer.
	v_helpTextBuffer = Gtk::TextBuffer::create();
	// Attach text view:
	v_mainGrid.attach(v_helpTextView, 0, 1, 1, 1);
	// Setup text view options:
	v_helpTextView.set_can_focus(false);
	v_helpTextView.set_buffer(v_helpTextBuffer);
	v_helpTextView.set_wrap_mode(Gtk::WRAP_WORD);
	v_helpTextView.set_justification(Gtk::JUSTIFY_CENTER);
	v_helpTextView.set_hexpand(true);
	v_helpTextView.set_vexpand(false);
	// setting insensitive makes the text from being editable & selectable.
	v_helpTextView.set_sensitive(false);
	
	
	v_mainGrid.attach(v_stepButtonbox, 0, 2, 1, 1);
	v_mainGrid.attach(*v_webpageWidget, 0, 3, 1, 1);
	v_mainGrid.attach(v_webProgressbar, 0, 4, 1, 1);
		v_webProgressbar.set_show_text(true);
		v_webProgressbar.set_margin_bottom(15);
		// Prevents progressbar from appearing on the first page.
		v_webProgressbar.set_no_show_all(true);
	v_mainGrid.attach(v_navigationButtonbox, 0, 20, 1, 1);
		v_navigationButtonbox.set_margin_top(20);
		v_navigationButtonbox.set_valign(Gtk::ALIGN_END);
	
	btn_next.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAccountSetup::btn_next_clicked));
	btn_back.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAccountSetup::btn_back_clicked));
	
	btn_stepEpicAccount.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAccountSetup::btn_accountCreation_clicked));
	btn_stepGithubAccount.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAccountSetup::btn_accountCreation_clicked));
	
	
	g_signal_connect( G_OBJECT(GTK_WIDGET(v_webkitWebview)), "load-changed", G_CALLBACK(webpageLoaded), this );
	g_signal_connect( G_OBJECT(GTK_WIDGET(v_webkitWebview)), "notify::estimated-load-progress", G_CALLBACK(webpageProgress), this );
	
	updateWizard();
} // END - Constructor.

LauncherAccountSetup::~LauncherAccountSetup(){}

void LauncherAccountSetup::btn_back_clicked(){
	// Decline active page count by 1.
	--v_activePage;
	// Set direction bool:
	v_incrementingSkips = false;
	
	// If page count is 0, deactivate back button, enable forward.
	if(v_activePage == 0){
		btn_back.set_sensitive(false);
		btn_next.set_sensitive(true);
	} // END - If active page is 0.
	// Run update wizard function.
	updateWizard();
} // END - Button back clicked.

void LauncherAccountSetup::btn_next_clicked(){
	int temp_canContinue = btn_stepAccountLinkage.get_active() + btn_stepEpicAccount.get_active() + btn_stepGithubAccount.get_active();
	// Single line: return from function if none of the buttons are clicked.
	if( temp_canContinue == 0 ) return;
	
	// If active page count is 3 (last possible step), send user to add engine
	if(v_activePage == 3){
		// Emit signal.
		v_signalCompleted.emit();
	} // END - If active page is max.

	// Incline active page count by 1.
	++v_activePage;
	// Set direction bool.
	v_incrementingSkips = true;
	
	
	// Run update function:
	updateWizard();
} // END - btn next clicked.


void LauncherAccountSetup::btn_accountCreation_clicked(){
	// If either account options are checked, set linkage option to true and de-sensitise to prevent user disabling.
	if( btn_stepEpicAccount.get_active() || btn_stepGithubAccount.get_active() ){
		btn_stepAccountLinkage.set_active(true);
		btn_stepAccountLinkage.set_sensitive(false);
	} else{
		btn_stepAccountLinkage.set_sensitive(true);
	} // END -if/else either stepoptions are active.
} // END - Account creation button clicked.


void LauncherAccountSetup::updateNextPageCounter(){
	if(v_incrementingSkips){
		// SKIPS ARE INCREMENTAL:
		if(v_activePage < 3) ++v_activePage;
		
	} else {
		// SKIPS ARE DECREMENTAL:
		if(v_activePage > 0) --v_activePage;
	} // END if/else step-skipping is increment/decrement.
} // END - update next page counter.



void LauncherAccountSetup::updateWizard(){
//	std::cout << "DEBUG:	active page is: " << std::to_string(v_activePage) << std::endl;
	// Hide the step buttonbox & webpage widget, so required steps only have to show.
	v_stepButtonbox.hide();
	v_webpageWidget->hide();
	v_webProgressbar.hide();
	// Default value for next button:
	btn_next.set_label("Next");
	
	// Set the progress bar to represent the steps
	v_statusProgressBar.set_max_value(
										btn_stepGithubAccount.get_active() +
										btn_stepEpicAccount.get_active() +
										btn_stepAccountLinkage.get_active());
										
	// Set the progress bar to match the current step:
	v_statusProgressBar.set_value(v_activePage);
	
	switch (v_activePage){
		case 0:
			btn_back.set_sensitive(false);
			v_helpTextBuffer->set_text("Check the boxes below in correspondance to the statement:\n"
			"I have NOT..."
			);
			v_stepButtonbox.show();
		break;
		
		case 1:
			btn_back.set_sensitive(true);
			if (btn_stepGithubAccount.get_active()){
				v_webpageWidget->show();
				v_webProgressbar.show();
				webkit_web_view_load_uri(v_webkitWebview, "https://github.com/join?source=login");
				v_helpTextBuffer->set_text("Create an account on Github.  \nAfter doing so, press NEXT!");
				
			} // END - if step is active.
			else{
				// Increment to next active page and re-run wizard.
//				++v_activePage;
				updateNextPageCounter();
				updateWizard();
			} // END Step not active.
		break;
		
		case 2:
		btn_next.set_sensitive(true);
			if(btn_stepEpicAccount.get_active()){
				v_webpageWidget->show();
				v_webProgressbar.show();
				webkit_web_view_load_uri(v_webkitWebview, "https://accounts.unrealengine.com/register?");
				v_helpTextBuffer->set_text("Create an Epic Games account and agree to the license.  \nAfter doing so, press NEXT (not the 'download' button on the webpage!)!");
			} // END - If epic account creation step is active.
			else{
//				++v_activePage;
				updateNextPageCounter();
				updateWizard();
			} // END - If step is NOT active.
		break;
		
		case 3:
//			btn_next.set_sensitive(false);
			if(btn_stepAccountLinkage.get_active()){
				v_webpageWidget->show();
				v_webProgressbar.show();
				webkit_web_view_load_uri(v_webkitWebview, "https://www.unrealengine.com/account/connected");
				v_helpTextBuffer->set_text("Scroll down and select 'CONNECT' below 'GITHUB', then follow the instructions to link the accounts!  \n"
				"You may have to log in if you did not create an account in earlier steps.\n"
				"Click NEXT to Add your first engine!");
				btn_next.set_label("Add Engine!");
			} // END - If step is active
			else{
				// No active options were selected, reset:
				v_activePage = 0;
				updateWizard();
				btn_next.set_sensitive(true);
			} // END - if step is NOT active.
		
	} // END - Active page switch.
} // END - Update Wizard.