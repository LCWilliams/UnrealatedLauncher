#include <include/launcher_main.h>
#include <include/launcher_setup.h>
#include <include/launcher_settings.h>

#include <string>
// MAIN LOOP.
// FOR MAIN WINDOW, OPEN "launcher_window"

int main(int p_arcgumentCount, char *p_arguments[]){
	
	launcherSetup_setupSettings();
	launcherSetup_createLauncherDirectories();
	
//	LauncherSettings::readFromFile();
	Glib::RefPtr<Gtk::Application> temp_UnrealatedLauncher = Gtk::Application::create(p_arcgumentCount, p_arguments, "LCWilliams.UnrealatedLauncher");
	UnrealatedLauncher::UnrealatedLauncherWindow temp_UnrealatedLauncherMainWindow;
//	LauncherGlobal::ref_launcherApplication = temp_UnrealatedLauncher;
	
	return temp_UnrealatedLauncher->run(temp_UnrealatedLauncherMainWindow);
} // END - Main loop