#include "include/library/library_addEngineGUI.h"

using namespace UnrealatedLauncher;

UnrealatedLauncher::LauncherAddEngine::LauncherAddEngine(LauncherRepositoryManager* p_repoManagerRef):
//btn_closeWindow("C_lose", true),
//btn_confirm("_Confirm", true),
//btn_defaults("_Defaults", true),
v_windowControls(true),

v_userInputFrame("Settings"),
v_repoManagerFrame("Repository Manager"),
v_commitSelectionFrame("Commit/Version Selection"),
v_endOutputFrame("Output"),

btn_addExisting("Add _existing engine", true),

txt_labelEngineLabel("Engine Label"),
txt_labelEngineDesc("Description"),
txt_labelInstallDir("Install Folder"),

btn_advancedOptions("Advanced Options"),

txt_labelCustomParam("Custom Params:"),

btn_showCommitTags("Only show releases"),
btn_refreshView("Refresh Lists"),
btn_showRepoMan("Repository Manager"),

txt_labelOutputEngineID("Engine ID: "),
txt_labelOutputLabel("Engine Label: "),
txt_labelOutputDesc("Engine Description:"),
txt_labelOutputInstallDir("Installation Directory:"),
txt_labelOutputGitCommit("Git Commit:"),
txt_labelOutputcustomParams("Custom Parameters:")

{
	// SET REFERENCES:
	ref_repoManager = p_repoManagerRef;
	ref_repoManGUI = Gtk::manage(new LauncherRepoManagerGUI(ref_repoManager));


	set_hexpand(true);
	set_vexpand(true);
	// v/halign causes issues with v/hexpand & min child properties of flowbox.

	// Add grid to main window & set options.
	this->add(v_mainGrid);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_row_homogeneous(false);
	v_mainGrid.set_column_spacing(15);
	v_mainGrid.set_row_spacing(20);
	v_mainGrid.set_hexpand(true);
	v_mainGrid.set_vexpand(true);
	v_mainGrid.set_column_homogeneous(false);
	
	// Add flowbox to main grid & set options.
	v_mainGrid.attach(v_mainFlowboxScrWindow, 0, 0, 2, 1);
	v_mainFlowboxScrWindow.add(v_mainFlowbox);
	v_mainFlowboxScrWindow.set_min_content_height(300);
	v_mainFlowboxScrWindow.set_vexpand(true);
	v_mainFlowbox.set_homogeneous(false);
	v_mainFlowbox.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
	v_mainFlowbox.set_row_spacing(5);
	v_mainFlowbox.set_selection_mode(Gtk::SELECTION_NONE);
	v_mainFlowbox.set_vexpand(false);
	
	// Add grids to frames, then frames to main ~grid~  Flowbox.
	v_userInputFrame.add(v_userInputGrid);
		v_userInputGrid.set_border_width(10);
		v_userInputFrame.set_vexpand(false);
	v_commitSelectionFrame.add(v_commitOverlay); // Add overlay to frame.
		v_commitOverlay.add(v_commitSelectionGrid); // Add grid to overlay as main element.
		v_commitOverlay.add_overlay(v_repoManRevealer); // Add revealer as overlay.
		v_repoManRevealer.add(*ref_repoManGUI);
		v_repoManRevealer.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
		v_repoManRevealer.set_reveal_child(false);
		v_repoManRevealer.set_transition_duration(250);
		v_repoManRevealer.set_halign(Gtk::ALIGN_CENTER);
		v_repoManRevealer.set_valign(Gtk::ALIGN_CENTER);
		v_commitSelectionGrid.set_border_width(10);
		v_commitSelectionFrame.set_vexpand(false);
	v_endOutputFrame.add(v_endOutputGrid);
		v_endOutputGrid.set_border_width(10);
		v_endOutputFrame.set_vexpand(false);

	v_mainFlowbox.add(v_userInputFrame);
	v_mainFlowbox.add(v_commitSelectionFrame);
	v_mainFlowbox.add(v_endOutputFrame);
	

	// Add button to grid within Buttonbox.
	v_windowControls.set_halign(Gtk::ALIGN_END);
	v_mainGrid.attach(v_windowControls, 1, 1, 1, 1);
	

	// SET SPECIFIC OPTIONS OF BUTTONS:
	btn_installDir.set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
	btn_installDir.set_create_folders(true);
	btn_engineLabel.set_placeholder_text("An optional, custom display label");
		btn_engineLabel.set_max_length(30);
	btn_engineDescription.set_placeholder_text("An optional short description...");
		btn_engineDescription.set_max_length(100);
	
	
	// ATTACH USER INPUT ITEMS TO GRID:
	v_userInputGrid.set_row_spacing(7);
	v_userInputGrid.set_column_spacing(5);
	v_userInputGrid.set_column_homogeneous(false);
	v_userInputGrid.set_row_homogeneous(false);
	
	v_userInputGrid.attach(btn_addExisting, 0, -1, 3, 1);
		btn_addExisting.set_tooltip_text("If adding an existing engine already installed on your computer, check this box.");
		
	v_userInputGrid.attach(txt_labelEngineLabel, 0, 0, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_labelEngineLabel, Gtk::ALIGN_START, 10, 3);
	v_userInputGrid.attach(btn_engineLabel, 1, 0, 2, 1);
		btn_engineLabel.set_tooltip_text("A custom label for the engine.  If not given, the version is displayed.");
		
	v_userInputGrid.attach(txt_labelEngineDesc, 0, 1, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_labelEngineDesc, Gtk::ALIGN_START, 10, 3);
	v_userInputGrid.attach(btn_engineDescription, 1, 1, 2, 1);
		btn_engineDescription.set_tooltip_text("A short description for the engine.");
	
	v_userInputGrid.attach(txt_labelInstallDir, 0, 2, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_labelInstallDir, Gtk::ALIGN_START, 10, 3);
	v_userInputGrid.attach(btn_installDir, 1, 2, 1, 1);
		btn_installDir.set_can_focus(true);
		// Tooltip text set in btn_addExisting_clicked function.
		btn_addExisting_clicked(); // Call it manually to set tooltips.
	v_userInputGrid.attach(btn_installDirReset, 2, 2, 1, 1);
		btn_installDirReset.set_hexpand(false);
		btn_installDirReset.set_image_from_icon_name("view-refresh", Gtk::ICON_SIZE_BUTTON);
	
	v_userInputGrid.attach(btn_advancedOptions, 0, 3, 3, 1);
		btn_advancedOptions.set_margin_top(10);
	btn_advancedOptions.add(v_advancedOptsGrid);
		v_advancedOptsGrid.set_hexpand(true);
		v_advancedOptsGrid.set_column_homogeneous(false);
		v_advancedOptsGrid.set_row_homogeneous(false);
		v_advancedOptsGrid.set_row_spacing(10);
		v_advancedOptsGrid.set_column_spacing(5);

//	v_advancedOptsGrid.attach(txt_labelCustomParam, 0, 0, 1, 1);
//		LauncherUtils::setLabelTextParameters(&txt_labelCustomParam, Gtk::ALIGN_START, 10, 3);
//	v_advancedOptsGrid.attach(btn_customParameters, 1, 0, 1, 1);

//	EngineLaunchOpts *v_launchOpts = Gtk::manage(new EngineLaunchOpts(v_engineData));
//	v_advancedOptsGrid.add(*v_launchOpts);
	

	// ATTACH GIT COMMIT ITEMS TO GRID:
	v_commitSelectionGrid.attach(btn_showCommitTags, 0, 0, 1, 1);
	v_commitSelectionGrid.attach(btn_commitSearch, 1, 0, 1, 1);
	v_commitSelectionGrid.attach(v_commitScrollWindow, 0, 1, 2, 1);
		v_commitScrollWindow.set_vexpand(true);
		v_commitScrollWindow.set_hexpand(true);
		v_commitScrollWindow.set_min_content_width(400);
		v_commitScrollWindow.set_min_content_height(150);
	// Treeview is added to scrollwindow via function.
	btn_showCommitTags.set_active(true); // Signal not connected yet.
	btn_showCommitTags_clicked();	// Manually call function.
	
	
	// Treeview is added to scrollwindow via function. 
	// FUNCTION CALLED ABOVE!
	v_commitSelectionGrid.attach(btn_refreshView, 1, 4, 1, 1);
		btn_refreshView.set_hexpand(false);
	v_commitSelectionGrid.attach(btn_showRepoMan, 0, 4, 1, 1);
	v_commitSelectionGrid.set_row_homogeneous(false);
	v_commitSelectionGrid.set_column_homogeneous(false);
	
//	v_commitSelectionGrid.attach(v_refreshViewSpinner, 0, 4, 1, 1);
	
	// SETUP COMMIT TREEVIEW.
	v_commitTreeView.set_search_entry(btn_commitSearch);
	v_commitTreeView.property_enable_search() = true;
	v_commitTreeView.set_activate_on_single_click(true);
	v_commitTreeView.set_grid_lines(Gtk::TREE_VIEW_GRID_LINES_BOTH);

	// Set Collumns:
	v_commitTreeView.append_column("Git Commits", ref_repoManager->v_repoCommitColumns.v_commitTitle);
	v_commitTreeView.get_column(0)->set_expand(true);
	v_commitTreeView.get_column(0)->set_min_width(100);
	v_commitTreeView.get_column(0)->set_max_width(300);
	

	// SETUP TAG TREEVIEW:
	v_tagTreeView.set_search_entry(btn_commitSearch);
	v_tagTreeView.set_grid_lines(Gtk::TREE_VIEW_GRID_LINES_BOTH);
	v_tagTreeView.property_enable_search() = true;
	v_tagTreeView.set_activate_on_single_click(true);

	// Tag Version Column:
	v_tagTreeView.append_column_numeric("Version", ref_repoManager->v_repoCommitColumns.v_tagVersion, "%2d");
	v_tagTreeView.get_column(0)->set_expand(false);
	v_tagTreeView.get_column(0)->set_min_width(50);
	v_tagTreeView.get_column(0)->set_max_width(75);
	v_tagTreeView.get_column(0)->set_sort_indicator(true);
	v_tagTreeView.get_column(0)->set_sort_column(ref_repoManager->v_repoCommitColumns.v_tagVersion);
	
	// Tag Hotfix column:
	v_tagTreeView.append_column_numeric("Hotfix", ref_repoManager->v_repoCommitColumns.v_tagHotfix, "%1d");
	v_tagTreeView.get_column(1)->set_expand(false);
	v_tagTreeView.get_column(1)->set_min_width(50);
	v_tagTreeView.get_column(1)->set_max_width(75);
	v_tagTreeView.get_column(1)->set_sort_indicator(true);
	v_tagTreeView.get_column(1)->set_sort_column(ref_repoManager->v_repoCommitColumns.v_tagHotfix);
	
	// Tag type column:
	v_tagTreeView.append_column("Type", ref_repoManager->v_repoCommitColumns.v_tagType);
	v_tagTreeView.get_column(2)->set_expand(true);
	v_tagTreeView.get_column(2)->set_min_width(100);
//	v_tagTreeView.get_column(2)->set_max_width(300);
	v_tagTreeView.get_column(2)->set_sort_indicator(true);
	v_tagTreeView.get_column(2)->set_sort_column(ref_repoManager->v_repoCommitColumns.v_tagType);
	
	
	
	// ATTACH OUTPUT ITEMS TO GRID:
	double temp_labelOpacity = 0.4;
	v_endOutputGrid.set_column_homogeneous(false);
	v_endOutputGrid.set_column_spacing(7);
	
	v_endOutputGrid.attach(txt_labelOutputEngineID, 0, 0, 1, 1);
		txt_labelOutputEngineID.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputEngineID, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputEngineID, 0 , 1, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_outputEngineID, Gtk::ALIGN_START);
	
	v_endOutputGrid.attach(txt_labelOutputLabel, 0, 2, 1, 1);
		txt_labelOutputLabel.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputLabel, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputLabel, 0, 3, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_outputLabel);

	v_endOutputGrid.attach(txt_labelOutputDesc, 0, 4, 1, 1);
		txt_labelOutputDesc.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputDesc, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputDesc, 0, 5, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_outputDesc, Pango::ELLIPSIZE_END, true, 3, Gtk::ALIGN_START, 0,0, 35);

	v_endOutputGrid.attach(txt_labelOutputInstallDir, 0, 6, 1, 1);
		txt_labelOutputInstallDir.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputInstallDir, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputInstallDir, 0, 7, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_outputInstallDir, Pango::ELLIPSIZE_MIDDLE, true, 2);
		v_endOutputGrid.attach(img_outputInstallDir, -1, 7, 1, 1);
		img_outputInstallDir.set_halign(Gtk::ALIGN_END);
	
	v_endOutputGrid.attach(txt_labelOutputGitCommit, 0, 8, 1, 1);
		txt_labelOutputGitCommit.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputGitCommit, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputGitCommit, 0, 9, 1, 1);
		LauncherUtils::setLongLabelTextParameters( &txt_outputGitCommit);
		v_endOutputGrid.attach(img_outputGitCommit, -1, 9, 1, 1);
		img_outputGitCommit.set_halign(Gtk::ALIGN_END);
	

	v_endOutputGrid.attach(txt_labelOutputcustomParams, 0, 10, 1, 1);
		txt_labelOutputcustomParams.set_opacity(temp_labelOpacity);
		LauncherUtils::setLabelTextParameters(&txt_labelOutputcustomParams, Gtk::ALIGN_START, 10, 5);
	v_endOutputGrid.attach(txt_outputcustomParams, 0, 11, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_outputcustomParams);

	// BUTTON: Set add existing to false as default.
	btn_addExisting.set_active(false);

// BUTTON SIGNALS:
	v_windowControls.v_signalDefaults.connect(sigc::mem_fun(*this, &LauncherAddEngine::setDefaults));
	btn_installDir.signal_selection_changed().connect(sigc::mem_fun(*this, &LauncherAddEngine::updateButtonOutput ));
	btn_engineLabel.signal_changed().connect(sigc::mem_fun(*this, &LauncherAddEngine::updateTextEntryOutputs ));
	btn_engineDescription.signal_changed().connect(sigc::mem_fun(*this, &LauncherAddEngine::updateTextEntryOutputs ));
	btn_customParameters.signal_changed().connect(sigc::mem_fun(*this, &LauncherAddEngine::updateTextEntryOutputs));
	btn_installDirReset.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_installDirReset_clicked));
	btn_addExisting.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_addExisting_clicked));
	btn_showRepoMan.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_showRepoMan_clicked));
	ref_repoManGUI->btn_closeManager.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_closeRepoMan_clicked));
	btn_refreshView.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_refreshView_clicked));
	btn_showCommitTags.signal_clicked().connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_showCommitTags_clicked));
	v_commitTreeView.signal_row_activated().connect(sigc::mem_fun(*this, &LauncherAddEngine::treeviewUpdated));
	v_tagTreeView.signal_row_activated().connect(sigc::mem_fun(*this, &LauncherAddEngine::treeviewUpdated ));
	v_windowControls.v_signalConfirm.connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_confirm_clicked));
	v_windowControls.v_signalCancel.connect(sigc::mem_fun(*this, &LauncherAddEngine::btn_cancel_clicked));
	
	ref_repoManager->dispatcherTaskEnded.connect(sigc::mem_fun(*this, &LauncherAddEngine::repoManTaskFinished));

	
// OTHER SIGNALS:
	ref_repoManager->dispatcherUpdatedLists.connect(sigc::mem_fun(*this, &LauncherAddEngine::reconnectLists ));

	setDefaults();
	
} // END - Constructor.

UnrealatedLauncher::LauncherAddEngine::~LauncherAddEngine(){}

LauncherAddEngine::typeSignalAddEngine LauncherAddEngine::signal_addEngine(){
	return v_signalAddEngine;
} // END - signal add engine.


EngineData* LauncherAddEngine::getEngineData(){
	return v_engineData;
} // END - Get engine data.



void LauncherAddEngine::setDefaults(){
	// Create a new engine ID: uses current time.
	v_engineID = std::time(0);
	txt_outputEngineID.set_text( std::to_string(v_engineID) );
	
	btn_engineLabel.set_text("");
	
	btn_engineDescription.set_text("");
	
	v_commitTreeView.get_selection()->unselect_all();
	v_tagTreeView.get_selection()->unselect_all();
	
	txt_outputGitCommit.set_text("");
	
	btn_customParameters.set_text("");
	
	img_outputGitCommit.clear();
	img_outputInstallDir.clear();
	
	// Must be called after outputEngineID text is set.
	btn_installDirReset_clicked();

//	updateButtonOutput();
} // END - Set defaults.


void LauncherAddEngine::updateButtonOutput(){
	txt_outputInstallDir.set_text( LauncherUtils::removeElementFromString( btn_installDir.get_uri(), "file://", "" ) );
	
	// Set sanity bool to false before performing check.
	v_installDirIsDefault = false;
	if(txt_outputInstallDir.get_text().empty() || txt_outputInstallDir.get_text() == LauncherSettings::defaultInstallDirectory ){
		txt_outputInstallDir.set_text( LauncherSettings::defaultInstallDirectory + "/" + std::to_string(v_engineID) );
		v_installDirIsDefault = true;
		img_outputInstallDir.set_from_icon_name("emblem-default-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
	} // END - If outputtext installdir is empty or outputtext IS default install directory.
	
	canConfirm();
} // END - Update output.



void LauncherAddEngine::updateTextEntryOutputs(){
	txt_outputLabel.set_text(btn_engineLabel.get_text());
	txt_outputDesc.set_text(btn_engineDescription.get_text());
	txt_outputcustomParams.set_text(btn_customParameters.get_text());
} // END - update text entry output.



void LauncherAddEngine::btn_installDirReset_clicked(){
//	btn_installDir.unselect_all();
	btn_installDir.set_uri( LauncherSettings::defaultInstallDirectory + "/" + std::to_string(v_engineID));
	v_installDirIsDefault = true;
} // END - install dir reset button clicked.



void LauncherAddEngine::btn_addExisting_clicked(){
	// Set item visibility to the oposite of the addExisting button state.
	// Use get_parent to get the flowbox child, otherwise just hiding the frame leaves behind unwanted spaces.
	v_commitSelectionFrame.get_parent()->set_visible( !btn_addExisting.get_active() );
	
	txt_labelOutputGitCommit.set_visible( !btn_addExisting.get_active() );
	txt_outputGitCommit.set_visible( !btn_addExisting.get_active() );
	img_outputGitCommit.set_visible( !btn_addExisting.get_active() );
	
	// Set the reset directory button sensitvity to the oposite of the add Existing button state.
	btn_installDirReset.set_sensitive( !btn_addExisting.get_active() );
	
	// Set install dir button tooltip text based on requirements.
	if(btn_addExisting.get_active()){
		btn_installDir.set_tooltip_text("Select the root folder, or the folder containing UE4Editor of the existing installation.");
	}else{
		btn_installDir.set_tooltip_text("Select the folder where you wish to install the UE4Editor.");
	} // END - if addexisting-getactive.
	
	updateButtonOutput();
} // END - Add existing clicked.



void LauncherAddEngine::btn_showRepoMan_clicked(){
	v_commitSelectionGrid.set_opacity(0.2);
	// Prevent background clicking.
	v_commitSelectionGrid.set_sensitive(false);
	// When using fade, unrevealed items stupidly exist over others.
	v_repoManRevealer.set_reveal_child(true);
} // END - button show repo manager clicked.

void LauncherAddEngine::btn_closeRepoMan_clicked(){
	// reset selection grid opacity
	v_commitSelectionGrid.set_opacity(1);
	// Reset sensitivity.
	v_commitSelectionGrid.set_sensitive(true);
	// Hide the revealer.
	v_repoManRevealer.set_reveal_child(false);
} // END _ button hide repo man clicked.


void LauncherAddEngine::btn_refreshView_clicked(){
	if( !ref_repoManager->v_taskInProgress ){
		// Set the button elements for the commit view to unsensitive: cleaner semiotics.
		txt_outputGitCommit.set_text("");
		btn_refreshView.set_sensitive(false);
		btn_showCommitTags.set_sensitive(false);
		btn_commitSearch.set_sensitive(false);
		btn_showRepoMan.set_sensitive(false);
		v_commitTreeView.unset_model();
		v_tagTreeView.unset_model();

		// Add spinner to scroll window.
		v_commitScrollWindow.remove();
		v_commitScrollWindow.add(v_refreshViewSpinner);
		v_commitScrollWindow.show_all();
		v_refreshViewSpinner.start();
		
		ref_repoManager->instigateUpdateRepoLists();
	} // END - if repoamanger isn't busy.
} // END - refresh view button clicked.


void LauncherAddEngine::btn_showCommitTags_clicked(){
	// Remove any item from the scrollwindow:
	v_commitScrollWindow.remove();
	
	// Deselect any item from both trees, as swapping between them lies to the user regarding an "active" selection.
	v_commitTreeView.get_selection()->unselect_all();
	v_tagTreeView.get_selection()->unselect_all();
	// Set commit output text to represent changes.
	txt_outputGitCommit.set_text("");
	img_outputGitCommit.set_from_icon_name("dialog-error-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
	
	// IF show commits is active, show the TAG treeview, else show the COMMIT treeview.
	if(btn_showCommitTags.get_active()){
		v_commitScrollWindow.add( v_tagTreeView );
		btn_commitSearch.set_sensitive(false);
	} else{
		v_commitScrollWindow.add( v_commitTreeView );
		v_commitTreeView.set_search_entry(btn_commitSearch);
		v_commitTreeView.set_search_column(ref_repoManager->v_repoCommitColumns.v_commitTitle);
		btn_commitSearch.set_sensitive(true);
	} // END - if show commits is active.
	
	// Show all children, otherwise treeviews don't show!
	v_commitScrollWindow.show_all();
	
} // END - Show commit tags clicked.



void LauncherAddEngine::treeviewUpdated(const Gtk::TreeModel::Path& p_path, Gtk::TreeViewColumn*){	// Special required function for connecting to signal changed.
	treeviewSelectionChanged();
} // END - treeview updated.



void LauncherAddEngine::treeviewSelectionChanged(){
	// Get current active treeview:
	if( btn_showCommitTags.get_active() ){
		// Showing tag view:
		Glib::RefPtr<Gtk::TreeSelection> ref_commitRow = v_tagTreeView.get_selection();
		if( ref_commitRow ){
			Gtk::TreeModel::iterator temp_rowIterator = ref_commitRow->get_selected();
			Glib::ustring temp_commit = (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_commitID];
			txt_outputGitCommit.set_text( temp_commit );
			v_outputVersion = (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_tagVersion];
			v_outputHotfix = (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_tagHotfix];
			v_isPreview = false; // Cleaner to set false here, and change to true using the following IF statement
			if( (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_tagType] != "release" ){
				v_isPreview = true;
//				std::cout << "DEBUG:	Tag type: "  << (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_tagType] << std::endl;
			} // END - if tagType is not release.
		} // END - if ref commit row exists.
	} else{
		// Showing commit view:
		Glib::RefPtr<Gtk::TreeSelection> ref_commitRow = v_commitTreeView.get_selection();
		if( ref_commitRow ){
			Gtk::TreeModel::iterator temp_rowIterator = ref_commitRow->get_selected();
			Glib::ustring temp_commit = (*temp_rowIterator)[ref_repoManager->v_repoCommitColumns.v_commitID];
			txt_outputGitCommit.set_text( temp_commit );
			img_outputGitCommit.set_from_icon_name("emblem-default-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		} // END - if ref commit row exists.
	} // END - if/else get active.
} // End - treeview selection changed.


void LauncherAddEngine::reconnectLists(){
	v_refreshViewSpinner.stop();
	
		v_commitTreeView.set_model(ref_repoManager->getCommitTreeModel(true));
		v_tagTreeView.set_model(ref_repoManager->getCommitTreeModel(false));
	
	// Call showCommiTags_Clicked to remove spinner and set appropriate view:
	btn_showCommitTags_clicked();
	
	// Set button sensitivity back to true.
	btn_refreshView.set_sensitive(true);
	btn_showRepoMan.set_sensitive(true);
	btn_commitSearch.set_sensitive();
	btn_showCommitTags.set_sensitive();
} // END - Reconnect list variables to tree view.


void LauncherAddEngine::btn_cancel_clicked(){
	v_signalCloseWindow.emit();
} /// END - Cancel button clicked.


void LauncherAddEngine::btn_confirm_clicked(){
	
	// Runs CanConfirm.  If any problems are detected, they are hilighted and the window will not continue.
	if( canConfirm() ){
		v_logger.debug("Confirming add engine...");
		// Input options are valid.  Create a data object and pass back to library.
		v_engineData = new UnrealatedLauncher::EngineData;

		v_engineData->identifier = v_engineID;
		v_engineData->description = txt_outputDesc.get_text();
		
		v_engineData->configFile = LauncherSettings::engineConfigs + std::to_string(v_engineData->identifier) + ".ini";
		
		if(v_installDirIsDefault){
			v_logger.appendDebug("Using default install directory!");
			std::cout << "DEBUG:	Using default." << std::endl;
			// Get realpath of default directory.
			Glib::ustring temp_string =  LauncherUtils::getRealPath( LauncherSettings::defaultInstallDirectory );
			v_engineData->installDir = temp_string + "/" + std::to_string(v_engineID);
		}else{
			v_logger.appendDebug("Using specified install directory!");
			v_engineData->installDir = txt_outputInstallDir.get_text();
		} // END - if/else install Directory is default.
		v_logger.appendDebug("Install directory to save is: " + v_engineData->installDir);

		v_engineData->gitCommit = txt_outputGitCommit.get_text();
		v_engineData->launchOptions[0] = txt_outputcustomParams.get_text();


		// Set engine label depending on if a custom one was given:
		if(txt_outputLabel.get_text().empty()){
			// If version is still not -1 (user used specific commit), use values from treeview change.
			if(v_outputVersion != -1){
				v_engineData->label = std::to_string(v_outputVersion) + "." + std::to_string(v_outputHotfix);
				// Check if release or preview & append label where required.
				if(v_isPreview){
					v_engineData->label += " [PREVIEW]";
				} // END - if is preview.
			}else{ // END - output version is not -1
				// User Specified commit OR existing engine.  Get Major/Minor/Patch from build.version
			
				// Check to see if build.version exists.
				Glib::ustring temp_filePath = v_engineData->installDir + "/Engine/Build/Build.version";
				if(LauncherUtils::fileExists( temp_filePath )){
					Glib::ustring temp_ustring = temp_filePath;
					std::ifstream temp_fileStr( temp_ustring );
					std::string temp_fileContent( (std::istreambuf_iterator<char>(temp_fileStr)), (std::istreambuf_iterator<char>()) );

					// Remove commas in order to detect numbers properly
					temp_fileContent = LauncherUtils::removeElementFromString(temp_fileContent, ",", " ");

					std::locale temp_loc;
					int temp_numbersFound = 0;  // Count the number of numbers found.  We're only interested in 3.
					std::stringstream temp_extracted;
					temp_extracted << temp_fileContent;
					
					std::string temp_currentWord = "";

					while(!temp_extracted.fail() && temp_numbersFound < 3){
						temp_extracted >> temp_currentWord;
//						std::cout << "DEBUG: Current Word: " << temp_currentWord << std::endl;
						if( LauncherUtils::isOnlyDigits(temp_currentWord) ){
							++temp_numbersFound;
//							std::cout << "FOUND NUMBER: " << temp_currentWord << std::endl;
							try{

							switch (temp_numbersFound){
								case 1:// Major number (4).  Ignore.
								break;
								
								case 2: // Launchers major number, UEs minor number.
								v_outputVersion = std::stoi(temp_currentWord);
								break;
								
								case 3:
								v_outputHotfix = std::stoi(temp_currentWord);
								break;

							} // END - Switch
							} // END - Try switch
							catch(std::exception &error){
								Glib::ustring temp_message = "ERROR DURING FILE PARSE! ";
								temp_message += error.what();
								v_logger.errorMsg( temp_message );
							} // END - Catch
						} // END - Only digits in extracted line.
					} // END - While words are still available.

					// Set engineData label.
					v_engineData->label = std::to_string(v_outputVersion) + "." + std::to_string(v_outputHotfix);
				} // END - Build version file exists.
			} // END - If/else user specified commit/existing engine.
		} else{// END - label is empty.
			v_engineData->label = txt_outputLabel.get_text();
		} // END - label is not empty.

		v_engineData->version = v_outputVersion;
		v_engineData->hotfix = v_outputHotfix;

		// Set install status 
		if(btn_addExisting.get_active()){
			v_engineData->installStatus = EngineInstallStatusEnum::ready;
		} else{
			v_engineData->installStatus = EngineInstallStatusEnum::newBlock;
		} // END - If/else add existing is true.
		
		
		// Set default launch options file: must be after SAVE- as that sets configFile.
		Glib::ustring temp_launchOptDefaultPath = LauncherUtils::removeElementFromString(v_engineData->configFile, ".ini", "");
		// Make the directory first:
		std::filesystem::create_directories(temp_launchOptDefaultPath.c_str());
		// Append default to end of path:
		temp_launchOptDefaultPath += "/default.ini";
	
		v_engineData->launchOptions[0] = temp_launchOptDefaultPath;


		// SAVE the data.
		v_engineData->saveData();


		// Emit signal to confirm and add new engine.
		v_signalAddEngine.emit();
		
		setDefaults();
		v_logger.appendDebug("Engine added!");
	} // END - if can confirm.
	
} // END - confirm button clicked.


void LauncherAddEngine::repoManTaskFinished(){
	// If option is enabled, close the repository manager, and refresh lists after repoMan task is completed.
	// Additionally, force auto-update on first-run (incase the user has disabled this option).
	if( (LauncherSettings::firstRun && ref_repoManager->v_lastThreadTask != LauncherRepositoryManager::lastThreadTask::updateLists ) || 
		(LauncherSettings::refreshListsAfterRepoManTask && ref_repoManager->v_lastThreadTask != LauncherRepositoryManager::lastThreadTask::updateLists ) ){
		btn_closeRepoMan_clicked(); // Close the manager
		btn_refreshView_clicked(); // Run refresh view.
	} // END - if auto refresh is enabled.
	
} // END - repo manager task finished.


bool LauncherAddEngine::canConfirm(){
	// Check Install directory:
	// If adding existing, check for the existance of UE4 binary.
	if(btn_addExisting.get_active()){
		Glib::ustring temp_userInstallDir = LauncherUtils::removeElementFromString( btn_installDir.get_uri(), "file://", "" );
		bool temp_fullPath = LauncherUtils::fileExists( temp_userInstallDir + "/UE4Editor" ); 
		bool temp_shortPath =  LauncherUtils::fileExists( temp_userInstallDir + "/Engine/Binaries/Linux/UE4Editor" );
		if( temp_fullPath + temp_shortPath != 1 ){
			btn_installDir.grab_focus();
			txt_outputInstallDir.set_markup( "<span fgcolor='red'>" + txt_error_noEngineFound + "</span>" );
			img_outputInstallDir.set_from_icon_name("dialog-error-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
			return false;
		} // END - check short & fullpath for engine.
	} // END - if adding existing.
	img_outputInstallDir.set_from_icon_name("emblem-default-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
	
	// Check there's an active GIT commit.
	if( !btn_addExisting.get_active() ){
		
		// Get iterators for both tag & commit rows.  If a commit from either tree was selected, an iterator will exist.
		Glib::RefPtr<Gtk::TreeSelection> ref_tagRow = v_tagTreeView.get_selection();
		Glib::RefPtr<Gtk::TreeSelection> ref_commitRow = v_commitTreeView.get_selection();
		Gtk::TreeModel::iterator temp_tagIterator = ref_tagRow->get_selected();
		Gtk::TreeModel::iterator temp_commitIterator = ref_commitRow->get_selected();
		
		// LOGIC: Check the appropriate iterator against the showCommitTag state.
		if( ( btn_showCommitTags.get_active() && !temp_tagIterator) ||  ( !btn_showCommitTags.get_active() && !temp_commitIterator) ){
			txt_outputGitCommit.set_markup( "<span fgcolor='red'>" + txt_error_noCommitSelected + "</span>" );
			img_outputGitCommit.set_from_icon_name("dialog-error-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
			v_commitSelectionFrame.grab_focus();
			return false;
		} // END - If either git commit row or git tag row fail.
	} // END - If add existing is false.
	img_outputGitCommit.set_from_icon_name("emblem-default-symbolic", Gtk::ICON_SIZE_SMALL_TOOLBAR);
	
	// END OF CHECKS:
	return true;;
	
} // END - Can Confirm.


void LauncherAddEngine::setupFirstRunGUI(){
	// Prompt the repo manager to show.
	btn_showRepoMan.clicked();
	// De-sensitise the close manager button.
	ref_repoManGUI->btn_closeManager.set_sensitive(false);
} // END - SEtup first run GUI.