#include "include/library/library_engine.h"


using namespace UnrealatedLauncher;
namespace stdFileSys = std::filesystem;

LauncherEngine::LauncherEngine(EngineData* p_dataRef){
if(p_dataRef != nullptr){ // Not indented, as it's a first/last line IF statement.
	// Set references, else watch the world burn:
	ref_engineData = p_dataRef;
	ref_engineData->ref_engineGUI = this;
	
	// Set name of engine block so it's themeable.
	set_name("LauncherEngineBlock");
	
	// Set bin size requests:
	set_size_request(300, 285);
	set_hexpand(false);
	set_vexpand(false);
	set_valign(Gtk::ALIGN_CENTER);
	set_border_width(15);
	
	
	// Add main grid & set options.
	add(v_mainGrid);
	v_mainGrid.set_halign(Gtk::ALIGN_CENTER);
	v_mainGrid.set_valign(Gtk::ALIGN_START);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_row_homogeneous(false);
	
	// Attach items to main grid.
	
	v_mainGrid.attach(txt_labelEngineLabel, 0, 0, 2, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_labelEngineLabel, Pango::ELLIPSIZE_NONE, true, 2, Gtk::ALIGN_CENTER, 0, 30, 20);
		txt_labelEngineLabel.set_hexpand(true);
		txt_labelEngineLabel.set_valign(Gtk::ALIGN_START);
	
	v_mainGrid.attach(v_launchBtnScrWindow, 0, 1, 1, 1);
		v_launchBtnScrWindow.set_hexpand(true);
		v_launchBtnScrWindow.set_min_content_height(75);
		v_launchBtnScrWindow.set_min_content_width(100);
	v_launchBtnScrWindow.add(v_launchBtnGrid);
		v_launchBtnGrid.set_orientation(Gtk::ORIENTATION_VERTICAL);
		
	createLaunchButtons();
		
		
	v_mainGrid.attach(btn_options, 1, 1, 1, 1);
		btn_options.set_halign(Gtk::ALIGN_END);
		btn_options.set_image_from_icon_name("emblem-system-symbolic", Gtk::ICON_SIZE_BUTTON);
		
	
	v_mainGrid.attach(v_taskProgressbar, 0, 2, 2, 1);
		v_taskProgressbar.set_no_show_all(true);
	
	v_mainGrid.attach(v_taskLevelbar, 0, 3, 2, 1);
		v_taskLevelbar.set_tooltip_text("Indicates how many tasks have been complied / left remaining");
		v_taskLevelbar.set_mode(Gtk::LEVEL_BAR_MODE_DISCRETE);
		v_taskLevelbar.set_min_value(0);
		v_taskLevelbar.set_max_value(5);
		v_taskLevelbar.set_no_show_all(true);
	
	v_mainGrid.attach(txt_labelDescription, 0, 5, 2, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_labelDescription, Pango::ELLIPSIZE_END, true, 5, Gtk::ALIGN_CENTER, 15, 15, 30);
	txt_labelDescription.set_hexpand(false);
	txt_labelDescription.set_size_request(250, -1);
	txt_labelDescription.set_max_width_chars(2); // Needed, else label wont wrap.
	txt_labelDescription.set_opacity(0.4);
	txt_labelDescription.set_valign(Gtk::ALIGN_END);
	
	
	// Set labels:
	// Set style in CSS file; avoids dealing with HTML sanity checking.
//	txt_labelEngineLabel.set_markup("<span size='x-large' weight='ultrabold'>" + ref_engineData->label + "</span>");
	txt_labelEngineLabel.set_text(ref_engineData->label);
	txt_labelDescription.set_text(ref_engineData->description);
	
	
	// Connect signals:
	btn_options.signal_clicked().connect(sigc::mem_fun(*this, &LauncherEngine::btn_options_clicked));

	v_dispatcherTaskCompleted.connect(sigc::mem_fun(*this, &LauncherEngine::threadTask_completed));
	v_dispatcherUpdateProgress.connect(sigc::mem_fun(*this, &LauncherEngine::updateProgress ));
	v_dispatcherIncrementTask.connect(sigc::mem_fun(*this, &LauncherEngine::incrementTaskLevelBar));
	
	updateGUIFromStatus();
	
	// If engine install status is DELETING, instigate removal.
	if( ref_engineData->installStatus == EngineInstallStatusEnum::deleting ){
		v_logger.debug("Engine removal incomplete! Continuing removal of: " + ref_engineData->label);
		instigateRemoval();
	} // END - If install status.
	
	// Run sanity check on engine Data.
	if(!ref_engineData->sanityCheck()){
		v_logger.errorMsg("Sanity check failed on engine: " + ref_engineData->label + "[" + ref_engineData->identifier + "]");
		txt_labelDescription.set_text( ref_engineData->notSaneMessage );
		set_sensitive(false);
	}// END - Sanity check failed.
	
	show_all();
	
	v_engineBlockThread = nullptr;
} // END - If engineData ref is a nullptr. : Not indented, since it's a first/last line IF.
else{
	v_logger.errorMsg("Engine data is null!");
} // END - Else.
} // END - Constructor.

LauncherEngine::~LauncherEngine(){}


void LauncherEngine::notifyUpdateProgress(){
	v_dispatcherUpdateProgress.emit();
} // END - Notify update progress.


void LauncherEngine::notifyTaskCompleted(){
	v_dispatcherTaskCompleted.emit();
} // END - Notify task completed.

void LauncherEngine::notifyIncrementTaskLevel(){
	v_dispatcherIncrementTask.emit();
} // END - Notify increment task level.

void LauncherEngine::notifyDeletionComplete(){
	v_signalDeletionComplete.emit(this);
} // END - Notify deletion complete.

void LauncherEngine::updateProgress(){
	// Try locking mutex.  If successful; update GUI.
	// TryLock is used to prevent updating the GUI slowing down any operation;
	// Task completion is handled by a seperate dispatcher.
	if( threadComm_taskProgress_mutex.try_lock() ){
		v_taskProgressbar.set_text( threadComm_taskText );
		
		// If the task percentage is currently 0, pulse the bar, else set to the value.
		if( threadComm_taskPercent == 0 ){
			v_taskProgressbar.pulse();
		} else{
			v_taskProgressbar.set_fraction( threadComm_taskPercent );
		} // END - if/else task percent is 0.
		
		// Unlock mutex or suffer the wrath of mutex locking.
		threadComm_taskProgress_mutex.unlock();
	} // END - Try locking mutex.
} // END - Update progress.


void LauncherEngine::incrementTaskLevelBar(){
	v_taskLevelbar.set_value( v_taskLevelbar.get_value() + 1 );
//	std::cout << "DEBUG:	TaskLevel: " << v_taskLevelbar.get_value() << std::endl;	
} // END - Increment task level bar.



void LauncherEngine::setUpdateProgressData(Glib::ustring p_text, double p_percent){
	// LOCK the mutex before continuing.
	threadComm_taskProgress_mutex.lock();
	// Set variables.
	threadComm_taskText = p_text;
	threadComm_taskPercent = p_percent;
	// UNLOCK mutex.
	threadComm_taskProgress_mutex.unlock();
	
	notifyUpdateProgress();
} // END - Set update progress data.


void LauncherEngine::btn_launch_clicked(Glib::ustring p_params){
	v_logger.debug("Launch Button pressed!");
	v_logger.appendDebug("Params: " + p_params);
	if( ref_engineData->installStatus != EngineInstallStatusEnum::ready ){
		instigateInstall();
	} else{
		v_launchBtnGrid.set_sensitive(false);
		
		v_engineBlockThread = new std::thread(&LauncherEngine::threadTask_launchEditor, this, p_params);
		v_engineBlockThread->detach();
		
	} // END - if/else install status is not 0 (ready)
} // END - button launch clicked.


void LauncherEngine::btn_options_clicked(){
	v_signalEngineProperties.emit( this );
} // END - Button options clicked.


void LauncherEngine::updateRepoManagerStatus(bool p_status){
	v_repoManagerBusy = p_status;
} // END - update repo manager status.


void LauncherEngine::updateLabelsFromData(){
	txt_labelEngineLabel.set_text(ref_engineData->label);
	txt_labelDescription.set_text(ref_engineData->description);
} // END - Update labels from data.


// Instigates the installation based on the current install status; does not invoke deletion!
void LauncherEngine::instigateInstall(){
	// Check to see if a task is already in progress before continuing...
	if(!getThreadIsBusy()){
		
		// Disbale button sensitivity:
		v_launchBtnArray[0]->set_sensitive(false);

		// Show the level & status bars.
		v_taskProgressbar.show();
		v_taskLevelbar.show();
		
		v_engineBlockThread = new std::thread(&LauncherEngine::threadTask_install, this);
		v_engineBlockThread->detach();

		// After instigating the install threadTask, connect an idle watcher to update progress.
		// This is for steps that are not directly watchable for their progress: in order to pulse the progress bar.
		Glib::signal_timeout().connect(sigc::mem_fun(*this, &LauncherEngine::idleWatchProgress), 500);
	
	}else{ // END - If thread task isn't already in progress.
		v_logger.warnMsg("Task already in progress! Unable to instigate install!");
	} // END - Ifelse/Thread task isn't already in progress

} // END - Instigate install.



bool LauncherEngine::getThreadIsBusy(){
	if(v_engineBlockThread != nullptr){
//		Spams output.  Uncomment when nessecary.
//		v_logger.warnErrMsg("TASK IN PROGRESS!");
		return true;
	} // END - if thread object exists.
	return false;
} // END - Get thread is busy.



void LauncherEngine::instigateRemoval(){
	// If engineBlockThread is NOT a nullptr, a task is already in progress.
	if( v_engineBlockThread != nullptr ){
		std::cerr << "TASK ALREADY IN PROGRESS!" << std::endl;
	} else{
		set_sensitive(false);
		v_engineBlockThread = new std::thread(&LauncherEngine::threadTask_removeEngine, this);
		v_engineBlockThread->detach();
	} // END if/else: engine block thread exists.
} // END - Instigate removal.


void LauncherEngine::instigateConfigRemoval(){
	// Remove engine ini file.
	std::remove(ref_engineData->configFile.c_str());
	
	notifyDeletionComplete();
} // END - Instigate CONFIG removal.

void LauncherEngine::threadTask_completed(){
	// If an error occured (and thus an error message set), display error message.
	if( !v_installErrorMessage.empty() ){
		txt_labelDescription.set_text( v_installErrorMessage );
	} // END - If an erorr occured.
	
	v_logger.debug("Thread task completed!");

	delete v_engineBlockThread;
	v_engineBlockThread = nullptr;
	
	if( ref_engineData->installStatus == EngineInstallStatusEnum::makeScriptRan ){
		v_logger.debug("Finalising install...");

		setInstallStatus(EngineInstallStatusEnum::ready);
		incrementTaskLevelBar();
	} // END - If install status is buildScriptRan, perform finalisation.
	v_launchBtnGrid.set_sensitive(true);
	updateGUIFromStatus();
} // END - Thread task: completed.


void LauncherEngine::threadTask_install(){
	// threadTask_Install, uses the installStatus enum to set the starting point.
	// Behaves similar to OLD instigateInstall, however the case does not break, and is required to be started within its own thread.
	
	// Variable to hold whether an error occured. If true, return out from the function.
	bool temp_errorOccured = false;
	
	
	switch ( ref_engineData->installStatus ){
		/* NEW BLOCK:
		* New engine settings configured by the user, but not yet started the install*/
		case (EngineInstallStatusEnum::newBlock) :
			temp_errorOccured = !threadTask_copySource();
			if( temp_errorOccured || v_errorOccuredDuringCopy ){
				notifyTaskCompleted();
				break;
			} // END - If error occured.

		/* SOURCE COPIED:
			* The source has been copied.
			* The next stage should be reverting to the commit.*/
		case EngineInstallStatusEnum::sourceCopied :
			temp_errorOccured = !threadTask_revertToCommit();
			if( temp_errorOccured ){
				notifyTaskCompleted();
				break;
			} // END - If error occured.

		/* REVERTED TO COMMIT:
			* The copied repo has been reverted to the user specified commit..
			* The next stage should be running the setup script.*/
		case EngineInstallStatusEnum::revertedToCommit :
			temp_errorOccured = !threadTask_runSetupScript();
			if( temp_errorOccured ){
				notifyTaskCompleted();
				break;
			} // END - If error occured.

		/* MAKE SCRIPT RAN:
			* Setup scripts has been ran.
			* Next step should be calling the make script.*/
		case EngineInstallStatusEnum::setupScriptRan :
			temp_errorOccured = !threadTask_runMakeScript();
			if( temp_errorOccured ){
				notifyTaskCompleted();
				break;
			} // END - If error occured.

		/* BUILD SCRIPT RAN:
			* Build script has been succesfully ran.
			* Next step is finalising the install, which includes obaining the engine version if not already set from tagged commit */
		case EngineInstallStatusEnum::makeScriptRan :
			// Call threadTask_completed (via notify for thread-safety), which will then check the installStatus to perform finalisation.
			notifyTaskCompleted();
		break;
		
		default:
		v_logger.errorMsg("INCORRECT INSTALL STATUS SET!  Resetting to [newBlock] and restarting.");
			ref_engineData->installStatus = EngineInstallStatusEnum::newBlock;
			threadTask_install();
		break;
	} // END - Switch
} // END - threadTask install.


bool LauncherEngine::threadTask_copySource(){
	v_logger.debug("THREAD TASK- Copying source from launcher repository.");
	// Set failed variable to false at start:
	v_errorOccuredDuringCopy = false;
	
	if(v_repoManagerBusy){
		threadTask_failed("Repository Manager is currently busy.", "WARN:	Repository Manager is currently busy.  Wait until it's finished then try again.");
		v_errorOccuredDuringCopy = true;
		return false;
	} // END - if repo manager is busy.

	// Int to hold the number of files processed
	double v_filesProcessed = 0;
	// Int to hold the total number of files.
	unsigned int v_filesTotal = LauncherUtils::getFileCount( LauncherSettings::launcherRepositoryDirectory );

	// The real/true path of the launcher repository.
	Glib::ustring v_repoTruePath = LauncherUtils::getRealPath( LauncherSettings::launcherRepositoryDirectory );
	// End position of the repo directory string in order to recreate the end path for copy "To".
	std::size_t v_repoPathEnd = v_repoTruePath.length();

	// Try & catch the creation of the install directory.
	try{
		v_logger.debug("Attempting to create installation directory...");
		stdFileSys::create_directory( ref_engineData->installDir.c_str() );
	}
	catch(const std::exception& temp_exception){
		Glib::ustring temp_message = "Failed to create install directory";
		temp_message += temp_exception.what();
		v_logger.errorMsg(temp_message);
	} // END try/catch install dir creation.
	
	// The start of the "to" path; the install directory true path.
//	Glib::ustring v_trueInstallDir = LauncherUtils::getRealPath(ref_engineData->installDir);
	
	// Check the directory lengths (after using realpath; returns "" if invalid).
	if( ref_engineData->installDir.empty() ){
		v_errorOccuredDuringCopy = true;
		threadTask_failed("Aborting copy. Install directory is invalid.", "ERROR:	Aborting copy of source.  Install directory is invalid.");
		return false;
	} // END - install dir truepath is invalid
	if(v_repoTruePath.empty() ){
		v_errorOccuredDuringCopy = true;
		threadTask_failed("Aborting Copy. Repo directory is invalid.", "ERROR:	Aborting copy of source: Repository directory is empty!");
		return false;
	} // END - Repo true path is invalid.
	
	// If the total files is less than 60k, the repository doesn't exist.
	if( v_filesTotal < 60000 ){
		v_errorOccuredDuringCopy = true;
		threadTask_failed("Aborting copy. Repository doesn't exist!", "ERROR:	Repo files do not exist.  Download the repository!");
		return false;
	} // END - repo doesn't exist File count check.
	
	
	for(stdFileSys::directory_entry iteratorIndex: stdFileSys::recursive_directory_iterator(v_repoTruePath.c_str())){

		// The current file path, as a string.
		std::string temp_path(iteratorIndex.path());
		// The short, relative path of the file, used to create the "to" path.
		Glib::ustring temp_shortPath = temp_path;
		temp_shortPath = temp_shortPath.erase(0, v_repoPathEnd);
		// End destination of the file.
		Glib::ustring temp_endPath = ref_engineData->installDir + temp_shortPath;
		
		/* First try to create the directory. Then attempt copy.
		MAKE SURE THAT THE DIRECTORY TO CERATE IS NOT A FILE ITSELF!
		Reliability of copy seems to alter depending on whether or not the directory exists for it to be copied into;
		thus, as a precaution (and to have cleaner warning/debugs), the directory is created seperately. */
		try{
			// Set endPathDir to be full end path.
			Glib::ustring temp_endPathDirectory = temp_endPath; 
			std::size_t temp_posOfLastSlash = temp_endPathDirectory.find_last_of("/");
			// Remove the remaining elements from the last dash.
			temp_endPathDirectory.erase(temp_posOfLastSlash, std::string::npos);
			stdFileSys::create_directory(temp_endPathDirectory.c_str());
		} // END - Try create directory.
		catch(const std::exception& temp_exception){
			Glib::ustring temp_message = "Failed to create directory: ";
			temp_message += temp_exception.what();
			v_logger.warnErrMsg(temp_message);
			++v_installErrorCount;
			/* Because the creation of a directory may fail due to existing files,
			 * the errorOccuredDuringCopy boolean is not modified. */
		} // Catch - create directory try/catch
		
		try{
			stdFileSys::copy(temp_path.c_str(), temp_endPath.c_str(), stdFileSys::copy_options::update_existing);
		} // END - Try copy file.
		
		catch(const std::exception& temp_exception){
			// Print message to cerror.
			Glib::ustring temp_message = "Failed to create copy file: ";
			temp_message += temp_exception.what();
			v_logger.warnErrMsg(temp_message);
			
			// Incrment error counter.
			++v_installErrorCount;
			
			// Set boolean to true, and set the error message above to the related variable.
			v_errorOccuredDuringCopy = true;
			std::string temp_exceptionMessage = temp_exception.what();
			v_installErrorMessage = "WARN: Failed to copy file | " + temp_exceptionMessage;
			if(v_installErrorCount > 1){
				v_installErrorMessage += "\n ... and [" +std::to_string(v_installErrorCount) + "] other errors occured.";
			} // END - if error count is more than one.
		} // END - Catch.
		
		++v_filesProcessed;
		double temp_percentComplete = (double)v_filesProcessed / v_filesTotal;
		setUpdateProgressData( "Copying files...", temp_percentComplete );
	} // END - Iterate over launcher repo directory.

	// Only update the install status if no error occured.
	if( !v_errorOccuredDuringCopy ){
	} // END - if no error occured.
	v_logger.appendDebug("Copy completed.");
	
	setInstallStatus(EngineInstallStatusEnum::sourceCopied);
	// Increments the task levelbar by 1.
	notifyIncrementTaskLevel();
	return true;
} // END - Threadtask: Copy source.


bool LauncherEngine::threadTask_revertToCommit(){
	if( ref_engineData->gitCommit.empty() ){
		v_logger.warnErrMsg("Git commit is empty: skipping revert step!");
		notifyIncrementTaskLevel();
		return false;
	} // END - if commit is empty.
	
	v_logger.debug("THREAD TASK - Reverting source to specified comit");
	
	setUpdateProgressData("Reverting to specified commit...", 0.1);
	
	setUpdateProgressData("Initialising Libgit...", 0.3);
	git_libgit2_init();
	
	// Git repository object:
	git_repository *v_repository;
	
	if( git_repository_open( &v_repository, LauncherSettings::launcherRepositoryDirectory.c_str() ) < 0 ){
		threadTask_failed( "Failed to open launcher repository!", "ERROR:	Failed to open the launcher repository!  Make sure it exists first!" );
		return false;
	} // END _ fails to open launcher repository.
	
	setUpdateProgressData("Getting object ID...", 0.6);
	
	// Git object & objectIDs:
	git_object *v_repoObject;
	git_oid v_objectID;
	
	if(git_oid_fromstr( &v_objectID, ref_engineData->gitCommit.c_str() ) < 0){
		threadTask_failed("Failed to obtain git object ID", "ERROR:	Failed to obtain git objectID from string");
		return false;
	} // END - Git oid from string.
	
	setUpdateProgressData("Looking up object...", 0.9);
	
	if( git_object_lookup(&v_repoObject, v_repository, &v_objectID, GIT_OBJ_ANY) < 0 ){
		threadTask_failed("Failed to lookup commit", "ERROR:	Failed to lookup commit");
		std::cerr << "		> " << giterr_last()->message << std::endl;
		return false;
	} // END - git object lookup.
	
	setUpdateProgressData("Resetting repository to chosen commit...", 0);
	if(git_reset( v_repository, v_repoObject, GIT_RESET_HARD, NULL ) < 0 ){
		threadTask_failed("Failed to revert to commit", "ERROR:	Failed to revert to commit");
		std::cerr << "		> " << giterr_last()->message << std::endl;
		return false;
	} // END - git reset failed.
	

	setInstallStatus(EngineInstallStatusEnum::revertedToCommit);
	notifyIncrementTaskLevel();
	
	return true;
} // END - Threadtask: revert to commit.


bool LauncherEngine::threadTask_runSetupScript(){
	v_logger.debug("THREAD TASK - Run setup scripts");
	
	// The combined string to pass.
	Glib::ustring v_commandString;
	Glib::ustring v_realPath = LauncherUtils::getRealPath(ref_engineData->installDir);
	
	setUpdateProgressData("Running setup. \nThis may take time as dependancies are downloaded.", 0);
	v_commandString = "'" + ref_engineData->installDir + "/Setup.sh'";
	if( system(v_commandString.c_str()) != 0 ){
		threadTask_failed("Setup script failed. \nSee log for more information.", "ERROR:	Failed to successfully run setup script. See above for more info.");
		return false;
	} // END - If setup script failed.


	setUpdateProgressData("Generating project files...", 0);
	v_commandString = "'" + v_realPath + "/GenerateProjectFiles.sh'";
	if( system(v_commandString.c_str()) != 0 ){
		threadTask_failed("Generate Project Files failed. \nSee log for more information.", "ERROR:	Failed to successfully run GenerateProjectFiles.sh. See above for more info.");
		return false;
	} // END - if generate script failed.
	
	setInstallStatus(EngineInstallStatusEnum::setupScriptRan);
	// Increments the task levelbar by 1.
	notifyIncrementTaskLevel();

	return true;
} // END - Thread task: run build script.


bool LauncherEngine::threadTask_runMakeScript(){
	v_logger.debug("THREAD TASK- Running make scripts.");
	
	// The combined string to pass.
	Glib::ustring v_commandString;
	Glib::ustring v_realPath = LauncherUtils::getRealPath(ref_engineData->installDir);
	
	// Modify install directory permissions first, else have permission denied.
	v_commandString = "chmod 777 -R " + v_realPath + "/Makefile";
	system(v_commandString.c_str());
	
	setUpdateProgressData("Making the engine.  This will take a while!", 0);
	
//	v_commandString = "cd '" + v_realPath + "' && make";
	v_commandString = "make -C " + v_realPath ;
	system(v_commandString.c_str());
	
	setInstallStatus(EngineInstallStatusEnum::makeScriptRan);
	notifyIncrementTaskLevel();
	return true;
} // END - Thread Task: run make script.


void LauncherEngine::threadTask_removeEngine(){
	v_logger.debug("THREAD TASK - Removing enigne");
	// Obtain file count so GUI progressbar is updated.
	v_deleteTotalFiles = LauncherUtils::getFileCount( ref_engineData->installDir );
//	std::cout << "	> Files to remove." << v_deleteTotalFiles << std::endl;
	// if files exist, remove them.
	if(v_deleteTotalFiles != 0){
		// Call the recursive function for deleting the files.
		threadTask_deleteEngineDir( ref_engineData->installDir );
	} // END - if files exist.
	
	// Remove engine ini file.
	std::remove(ref_engineData->configFile.c_str());
	// Remove launchOpt configs:
	threadTask_deleteEngineDir( LauncherUtils::removeElementFromString(ref_engineData->configFile, ".ini", "") + "/" );
	
	// Completed.
	notifyDeletionComplete();
//	threadTask_completed();
} // END - thread task: remove engine.


void LauncherEngine::threadTask_deleteEngineDir(Glib::ustring p_path){
	// Double, held outside of while loop so not constructed on each loop.
	double temp_updateDouble;
	
	DIR *v_directory = opendir(p_path.c_str());
	struct dirent *v_nextFile;
	char v_filePath[PATH_MAX];

	while( (v_nextFile = readdir(v_directory)) != NULL ){
		
		if( strcmp(v_nextFile->d_name, ".") && strcmp(v_nextFile->d_name, "..") ){
			sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

			// If next file is a directory, call function again
			if(v_nextFile->d_type == DT_DIR){
				threadTask_deleteEngineDir( v_filePath );
			} // END - if next file is a DIRECTORY
		++v_deleteFilesRemoved;
		} // END - if next file name.
		
		sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

		temp_updateDouble = double(v_deleteFilesRemoved) / v_deleteTotalFiles;
//		std::cout << "Progress: " << std::to_string(temp_updateDouble) << std::endl;
		setUpdateProgressData("Removing files...", temp_updateDouble);
		
		std::remove(v_filePath);
	} // END - while directory isn't null

	setUpdateProgressData("Removing files...", temp_updateDouble);

	closedir(v_directory);
	std::remove(p_path.c_str());
} // END - thread Task: delete engine directory.


void LauncherEngine::threadTask_launchEditor(Glib::ustring p_params){
	// Launch the editor via system call.
	std::string temp_command = ref_engineData->installDir; 
	temp_command += LauncherSettings::editorPath + " " + p_params;
	system(temp_command.c_str());

	notifyTaskCompleted();
} // END - threadtask launch editor.


void LauncherEngine::threadTask_failed(Glib::ustring p_guiDebugInfo, Glib::ustring p_consoleInfo){
	// Call update GUI.
	setUpdateProgressData(p_guiDebugInfo, 1);

	v_logger.errorMsg(p_consoleInfo);

	// Notify task completed, to delete thread.
	notifyTaskCompleted();
} // END - threadtask failed.


void LauncherEngine::updateGUIFromStatus(){
//	std::cout << "DEBUG:	Engine install status is: " << std::to_string(int(ref_engineData->installStatus)) << std::endl;

	// Show task progress items, and hide where nesseccary.
	v_taskProgressbar.show();
	v_taskLevelbar.show();

	switch ( ref_engineData->installStatus ){
		
		case EngineInstallStatusEnum::deleting :
			// Deletion instigation does NOT get called from here, as this is merely to update the GUI.
			// Set entire block to be insensitive so user is unable to interfere with deletion,
			// but is still able to see the process.
			set_sensitive(false);
			v_taskProgressbar.set_text("Deletion interupted.");
			v_taskLevelbar.set_max_value(1);
		break;

		case EngineInstallStatusEnum::newBlock :
			v_taskProgressbar.set_show_text(true);
			v_taskLevelbar.set_max_value(5);
			v_launchBtnArray[0]->setButtonLabel("Start installation");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
		break;


		case EngineInstallStatusEnum::ready :
			v_taskProgressbar.set_show_text(false);
			v_launchBtnArray[0]->setButtonLabel("Launch");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
			v_taskProgressbar.hide();
			v_taskLevelbar.hide();
		break;


		case EngineInstallStatusEnum::sourceCopied :
			v_taskProgressbar.set_show_text(true);
			v_launchBtnArray[0]->setButtonLabel("Continue installation...");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
			v_taskLevelbar.set_value(1);
		break;

		case EngineInstallStatusEnum::revertedToCommit :
			v_taskProgressbar.set_show_text(true);
			v_launchBtnArray[0]->setButtonLabel("Continue installation...");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
			v_taskLevelbar.set_value(2);
		break;

		case EngineInstallStatusEnum::setupScriptRan :
			v_taskProgressbar.set_show_text(true);
			v_launchBtnArray[0]->setButtonLabel("Continue installation...");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
			v_taskLevelbar.set_value(3);
		break;

		case EngineInstallStatusEnum::makeScriptRan :
			v_taskProgressbar.set_show_text(true);
			v_launchBtnArray[0]->setButtonLabel("Finalize installation...");
			v_launchBtnArray[0]->set_sensitive(true);
			btn_options.set_sensitive();
			v_taskLevelbar.set_value(4);
		break;
		
		default:
			std::cerr << "INCORRECT ENGINE INSTALL VALUE!" << std::endl;
		break;
	} // END - switch.
	
} // END- Update GUI from status.


bool LauncherEngine::idleWatchProgress(){
	// just update progress.
	updateProgress();
	
	// When threadobject isn't nullptr (active), return true (repeat); return false disconnects.
	return getThreadIsBusy();
} // END - idle watch progress.


void LauncherEngine::setInstallStatus(EngineInstallStatusEnum p_status){
	ref_engineData->installStatus = p_status;
	// Write status to ini file.
	ref_engineData->saveData();
} // END - Set status

void LauncherEngine::setInstallStatusAndUpdateGUI(EngineInstallStatusEnum p_status){
	// Run set Status to set the status.
	setInstallStatus(p_status);
	// Run UpdateGuiFromStatus to update the engine block GUI; 
	updateGUIFromStatus();
} // END - Set status & Update


EngineData* LauncherEngine::getEngineData(){
	return ref_engineData;
} // END - Get engine data.

void LauncherEngine::removeLaunchButtons(){
	LauncherPrinter v_logger;
	v_logger.debug("Removing launch buttons!");
	
	// Iterate over button array and delete pointers.
	for(unsigned int btnIndex = 0; btnIndex < v_launchBtnArray.size(); ++btnIndex){
		v_logger.appendDebug(std::to_string(btnIndex));
		delete v_launchBtnArray[btnIndex];
	} // END - For loop.

	// Resize array to 1, to truncate the empty spaces
//	v_launchBtnArray.resize(1);
	v_launchBtnArray.clear();
} // END - remove launch buttons.

void LauncherEngine::createLaunchButtons(){
	CSimpleIniCaseA v_ini(true, false, false);
	SI_Error v_iniError;
	
	for(unsigned int btnIndex = 0; btnIndex < ref_engineData->launchOptions.size(); ++btnIndex){
		// If it's the first button, OR, the file exists...
		if( btnIndex == 0 || LauncherUtils::fileExists( ref_engineData->launchOptions[btnIndex] )){
			// Create the button...
			LaunchEngineBtn* newButton = Gtk::manage(new LaunchEngineBtn( ref_engineData->launchOptions[btnIndex] ));
			// Connect button press signal...
			newButton->v_signalLaunchBtnPressed.connect(sigc::mem_fun(*this, &LauncherEngine::btn_launch_clicked));
			// Add button to grid & array...
			v_launchBtnGrid.add( *newButton );
			v_launchBtnArray.push_back( newButton );
			// Show all, so they actually appear...
			v_launchBtnScrWindow.show_all();
			// Load button config:
			v_iniError = v_ini.LoadFile( ref_engineData->launchOptions[btnIndex].c_str() );
			if(v_iniError < 0){
				v_logger.warnErrMsg("Lauch button not added for config: " + ref_engineData->launchOptions[btnIndex] + " | SIERR: " + std::to_string(v_iniError));
				continue; // Skip iteration.
			} // END - if error occured loading file.
			// Set button variables using file data:
			newButton->setButtonLabel( v_ini.GetValue("other", "label", "") );
			newButton->setLaunchString( v_ini.GetValue("other", "command", "") );
		} // END - if file exists
	} // END - For loop.
} // END - Update launch buttons.

void LauncherEngine::updateLaunchButtons(std::vector<Glib::ustring> &p_newLabels, std::vector<Glib::ustring> &p_newCommands){
	LauncherPrinter v_logger;
	v_logger.debug("Updating launch buttons!");
	
	removeLaunchButtons();
	createLaunchButtons();
	v_logger.appendDebug("Size of labels: " + std::to_string(p_newLabels.size()));
	v_logger.appendDebug("Size of cmmnds: " + std::to_string(p_newCommands.size()));
	v_logger.appendDebug("Size of BtnArr: " + std::to_string(v_launchBtnArray.size()));
	
	if(p_newLabels.size() != p_newCommands.size() || p_newLabels.size() != v_launchBtnArray.size()){
		v_logger.warnErrMsg("Unable to set labels & commands! Mismatching array size!");
		return;
	}
	
	for(unsigned int btnIndex = 0; btnIndex < ref_engineData->launchOptions.size(); ++btnIndex){
		if(v_launchBtnArray[btnIndex]){
			v_launchBtnArray[btnIndex]->setButtonLabel( p_newLabels[btnIndex] );
			v_launchBtnArray[btnIndex]->setLaunchString( p_newLabels[btnIndex] );
		} // END - If button exists at index
	} // END - for loop.
	
	updateGUIFromStatus(); // Sets the first button respectively.
} // END - Update launch buttons.