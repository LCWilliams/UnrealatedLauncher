#include "include/library/library_engine.h"

using namespace UnrealatedLauncher;

EngineLaunchOpts::EngineLaunchOpts(EngineData* p_dataRef, unsigned int p_optionIndex):
txt_optionLabel("Label:"),
txt_optionManual("Manual:"),
txt_outputLabel("Command:"),

v_expander_modes("MODES", false),
v_expander_url("URL PARAMS", false),
v_expander_dev("DEVELOPER", false),
v_expander_render("RENDERING", false),
v_expander_network("NETWORK", false),
v_expander_user("USER", false),
v_expander_server("SERVER", false),
v_expander_gameStats("GAME STATS", false),
v_expander_configFiles("CONFIG FILES", false),
v_expander_misc("MISC", false),


// BUTTON CONSTRUCTORS
// MODES
btn_modes_renderer("modes", "renderer", "Renderer"),
btn_modes_type("modes", "launchType", "Launch Type"),

// URL
btn_url_game("url", "game", "game=", "Game Info"),
btn_url_name("url", "name", "name=", "Player name"),
btn_url_listen("url", "listen", "-LISTEN", "Server is listener"),
btn_url_isLanMatch("url", "lanMatch", "-BISLANMATCH", "Lan Match"),
btn_url_isFromInvite("url", "invite", "-bISFROMINVITE", "Invited player"),
btn_url_spectatorOnly("url", "spectator", "-SPECTATORONLY", "Start in spectator"),

// DEV

btn_dev_log("dev", "log", "-LOG", "Log"),
btn_dev_allusers("dev", "allUsers", "-ALLUSERS", "All Users"),
btn_dev_auto("dev", "auto", "-AUTO", "Auto assume YES"),
btn_dev_autoCOPackages("dev", "autoCOPackages", "-AUTOCHECKOUTPACKAGES", "Auto Checkout packages"),
btn_dev_biasCompression("dev", "biasCompression", "-BIASCOMPRESSIONFORSIZE", "Bias compression for size"),
btn_dev_buildMachine("dev", "buildMachine", "-BUILDMACHINE", "Build Machine"),
btn_dev_bulkImportingSound("dev", "bulkImportSound", "-BULKIMPORTINGSOUNDS", "Bulk Import Sounds"),
btn_dev_nativeClassSize("dev", "nativeClassSize", "-CHECK_NATIVE_CLASS_SIZES", "Check native class size"),
btn_dev_coderMode("dev", "coderMode", "-CODERMODE", "Coder Mode"),
btn_dev_compatScale("dev", "compatScale", "-COMPATSCALE", "Manual PCCompat settings"),
btn_dev_cookDemo("dev", "cookDemo", "-COOKFORDEMO", "Cook for Demo"),
btn_dev_cookPakages("dev", "cookPackages", "-COOKPACKAGES", "Cook Packages"),
btn_dev_crashReports("dev", "crashReport", "-CRASHREPORTS", "Always report crashes"),
btn_dev_d3dDebug("dev", "d3dDebug", "-D3DDEBUG", "Use D3D debug device"),
btn_dev_devCon("dev", "devCon", "-DEVCON", "Developer Connection (unencrypted sockets)"),
btn_dev_dumpFileStats("dev", "dumpFileStats", "-DUMPFILEIOSTATS", "Track and log file IO stats"),
btn_dev_fixedSeed("dev", "fixedSeed", "-FIXEDSEED", "Fixed seed"),
btn_dev_fixupTangents("dev", "fixupTangents", "-FIXUPTANGENTS", "Fix legacy tangents"),
btn_dev_forceLogFlush("dev", "forceLogFlush", "-FORCELOGFLUSH", "Force log flush after each line"),
btn_dev_forcePVRTC("dev", "forcePVRTC", "-FORCEPVRTC", "Force PVRTC texture compression"),
btn_dev_forceSoundRecook("dev", "forceSoundRecook", "-FORCESOUNDRECOOK", "Force sound recook"),
btn_dev_genericBrowser("dev", "genericBrowser", "-GENERICBROWSER", "Use the generic browser"),
btn_dev_installed("dev", "installed", "-INSTALLED", "Run game as if installed"),
btn_dev_lightmassDebug("dev", "lightmassDebug", "-LIGHTMASSDEBUG", "Lightmass Debug"),
btn_dev_lightmassStat("dev", "lightmassStat", "-LIGHTMASSSTATS", "Force detailed lightmass stat logging"),
btn_dev_noConform("dev", "noConform", "-NOCONFORM", "Don't conform packages"),
btn_dev_noContentBrowser("dev", "noContentBrowser", "-NOCONTENTBROWSER", "Disable the content browser"),
btn_dev_noInnerException("dev", "noInnerException", "-NOINNEREXCEPTION", "Disable exception handler within native C++"),
btn_dev_noLoadStartupPackages("dev", "noLoadStartupPackages", "-NOLOADSTARTUPPACKAGES", "Force startup packages to NOT load"),
btn_dev_noPause("dev", "noPause", "-NOPAUSE", "Close log window on exit"),
btn_dev_noPauseOnSuccess("dev", "noPauseOnSuccess", "-NOPAUSEONSUCCESS", "Close log window only if no errors"),
btn_dev_noRC("dev", "noRC", "-NORC", "Disable remote control"),
btn_dev_noVerifyGC("dev", "noVerifyGC", "-NOVERIFYGC", "Do not verify garbage compiler assumptions"),
btn_dev_noWrite("dev", "noWrite", "-NOWRITE", "Disable output to log"),
btn_dev_seekFreeLoading("dev", "seekFreeLoading", "-SEEKFREELOADING", "Only use cooked data"),
btn_dev_sekFreePackageMap("dev", "seekFreePackageMap", "-SEEKFREEPACKAGEMAP", "Override package map with cooked version"),
btn_dev_seekFreeLoadingPC("dev", "seekFreeLoadingPC", "-SEEKFREELOADINGPCCONSOLE", "Only use cooked data for PC console mode"),
btn_dev_seekFreeLoadingServer("dev", "seekFreeLoadingServer", "-SEEKFREELOADINGSERVER", "Only use cooked data for server"),
btn_dev_setThreadNames("dev", "setThreadNames", "-SETTHREADNAMES", "Force thread names to be set"),
btn_dev_showMissingLoc("dev", "showMissingLoc", "-SHOWMISSINGLOC", "Return error instead of english text for missing localisations"),
btn_dev_silent("dev", "silent", "-SILENT", "Disable output and feedback"),
btn_dev_traceAnimUsage("dev", "traceAnimUsage", "-TRACEANIMUSAGE", "Trace animation usage"),
btn_dev_loadWarnAsErrors("dev", "loadWarnAsError", "-TREATLOADWARNINGSASERRORS", "Force load warnings to be treated as errors"),
btn_dev_unattended("dev", "unattended", "-UNATTENDED", "Disable anything requiring feedback from user"),
btn_dev_useUnpublished("dev", "useUnpub", "-USEUNPUBLISHED", "Force packages in unpublished folder to be used"),
btn_dev_vaDebug("dev", "vaDebug", "-VADEBUG", "Use Visual Studio debugger interface"),
btn_dev_verbose("dev", "verbose", "-VERBOSE", "Verbose compiler output"),
btn_dev_verifyGC("dev", "verifyGC", "-VERIFYGC", "Force garbage compiler assumptions to be verified"),
btn_dev_warnsAsErrors("dev", "warnAsErrors", "-WARNINGSASERRORS", "Treat warnings as errors"),

btn_dev_automatedMapBuild("dev", "autoMapBuild", "-AutomatedMapBuild=", "Perform automated build of map: "),
btn_dev_conformDir("dev", "conformDir", "-CONFORMDIR=", "Conforming Packages Dir: "),
btn_dev_cultureForCook("dev", "cultureForCook", "-CULTUREFORCOOKING=", "Language to use for cooking: "),

btn_dev_firewall("dev", "firewall", "Firewall"),
btn_dev_logTimes("dev", "logTimes", "LogTimes"), // Default, force on, force off.
btn_dev_installGE("dev", "installGE", "... from game explorer"), // Install or uninstall GE from game explorer.


// RENDERING
btn_render_consoleX("render", "consoleX", "-ConsoleX=", "Console X position"),
btn_render_consoleY("render", "consoleY", "-ConsoleY=", "Console Y position"),
btn_render_winX("render", "winX", "-WinX=", "Game window X position"),
btn_render_winY("render", "WinY", "-WinY=", "Game window Y position"),
btn_render_gameResX("render", "gameResX", "-ResX=", "Game X resolution"),
btn_render_gameResY("render", "gameResY", "-ResY=", "Game Y resolution"),
btn_render_benchmarkFPS("render", "benchmarkFPS", "-FPS", "Benchmark FPS"),
btn_render_seconds("render", "seconds", "-Seconds=", "Maximum tick time"),

btn_render_benchmark("render", "benchmark", "-BENCHMARK", "Enable Benchmark (Run game at fixed-step: does not skip frames)"),
btn_render_dumpMovie("render", "dumpMovie", "-DUMPMOVIE", "Dump rendered frames to file."),
	

btn_render_vsync("render", "vsync", "VSync"),
btn_render_windowMode("render", "windowMode", "Window Mode"),
	
btn_render_exec("render", "exec", "-EXEC=", "Execute specified file"),

// NETWORK
btn_network_lanPlay("network", "lanPlay", "-LANPLAY", "Lan Play"),
btn_network_limitClientTicks("network", "limitClientTicks", "-LIMITCLIENTTICKS", "Limit Client Ticks"),
btn_network_multiHome("network", "multiHome", "-MULTIHOME", "Use Multi-home address"),
btn_network_networkProfiler("network", "networkProfiler", "-NETWORKPROFILER", "Enable Network profiler tracking"),
btn_network_noSteam("network", "noSteam", "-NOSTEAM", "Do not use Steamworks"),
btn_network_primaryNet("network", "primaryNet", "-PRIMARYNET", "Primary Network"),
btn_network_port("network", "port", "-PORT", "Port Number: "),

// USER
btn_user_noHomeDir("user", "noHome", "-NOHOMEDIR", "Override use of default home directory"),
btn_user_noForceFeedback("user", "noForcefeedback", "-NOFORCEFEEDBACK", "Disable force feedback in engine"),
btn_user_noSound("user", "noSound", "-NOSOUND", "Disable sound output from engine"),
btn_user_noSplash("user", "noSplash", "-NOSPLASH", "Disable splash image when loading engine"),
btn_user_noTextureStream("user", "noTextureStream", "-NOTEXTURESTREAMING", "Disable texture streaming."),
btn_user_oneThread("user", "oneThread", "-ONETHREAD", "Run engine on a single thread only"),
btn_user_useAllAvailableCores("user", "useAllCores", "-USEALLAVAILABLECORES", "Use all available cores"),

btn_user_paths("user", "paths", "-PATHS=", "Set paths to use for wrangled content"),
btn_user_preferredProcessor("user", "prefProcessor", "-PREFERREDPROCESSOR=", "Set thread affinity for specific processor"),

// SERVER
btn_server_login("server", "login", "-LOGIN=", "Server Login"),
btn_server_pass("server", "pass", "-PASSWORD=", "Password Login"),

// GAME STATS
btn_gameStat_noDatabase("gameStats", "noDB", "-NODATABASE", "No Database"),
btn_gameStat_noLiveTags("gameStats", "noLiveTag", "NOLIVETAGS", "No Live tags"),

// CONFIGS
btn_config_englishLocal("config", "englishLocal", "-ENGLISHCOALESCED", "Revert to English coalesced ini if localized not found"),
btn_config_noAutoIniUpdate("config", "noAutoIniUpdate", "-NOAUTOINIUPDATE", "Disable prompts to update ini files"),
btn_config_noIni("config", "noIni", "-NOINI", "Disable updating of ini files"),
btn_config_regenerateInis("config", "regenIni", "-REGENERATEINIS", "Force all ini files to be regenerated"),

btn_config_iniDefaultEd("config", "defEditor", "-DEFEDITORINI=", "Default Editor Ini"),
btn_config_iniEditor("config", "editor", "-EDITORINI=", "Editor Ini"),
btn_config_iniDefaultEdSettings("config", "defEdSetting", "-DEFEDITORINI=", "Default Editor User Settings Ini"),
btn_config_iniEditorUserSettings("config", "EdUserSettings", "-EDITORUSERSETTINGSINI=", "Editor User settings Ini"),
btn_config_iniDefaultCompat("config", "defCompat", "-DEFCOMPATINI=", "Default Compat Ini"),
btn_config_iniCompat("config", "compat", "-COMPATINI=", "Compat ini"),
btn_config_iniDefaultLightmass("config", "defLightmass", "-DEFLIGHTMASSINI=", "Default Lightmass Ini"),
btn_config_iniLightmass("config", "lightmass", "-LIGHTMASSINI=", "Lightmass Ini"),
btn_config_iniDefaultEngine("config", "defEngine", "-DEFENGINEINI=", "Default Engine Ini"),
btn_config_iniEngine("config", "engine", "-ENGINEINI=", "Engine Ini"),
btn_config_iniDefaultGame("config", "defGame", "-DEFGAMEINI=", "Default Game Ini"),
btn_config_iniGame("config", "game", "-GAMEINI=", "Game Ini"),
btn_config_iniDefaultInput("config", "defInput", "-DEFINPUTINI=", "Default Input Ini"),
btn_config_iniInput("config", "input", "-INPUTINI=", "Input Ini"),
btn_config_iniDefaultUI("config", "defUI", "-DEFUIINI=", "Default UI Ini"),
btn_config_iniUI("config", "ui", "-UIINI=", "UI Ini"),

// DEBUGFLAGS:
btn_debug_cpuProfileTrace("misc", "cpuProfTrace", "-CPUPROFILETRACE", "Enable CPU Profile Tracing"),
btn_debug_statNamedEvents("misc", "statNamedEvents", "-STATNAMEDEVENTS", "Enable Stat Named Events"),
btn_debug_fileTrace("misc", "fileTrace", "-FILETRACE", "Enable Filetrace"),
btn_debug_traceHost("misc", "traceHost", "-TRACEHOST=", "Trace Host")

/* ENGINELAUNCHOPTS::EngineLaunchOpts */{
	// SET THE REFERENCE OR WATCH EVERYTHING BURN!
	ref_engineData = p_dataRef;
	v_configFile = ref_engineData->launchOptions[p_optionIndex];
//	assert(ref_engineData != NULL);
	
	add(v_mainScrollWindow);
	set_hexpand(true);
	set_vexpand(true);
	
	v_mainGrid.set_row_homogeneous(false);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_row_spacing(20);
	v_mainGrid.set_column_spacing(10);
	v_mainGrid.set_border_width(15);
	
	
	// Set scrollwindow options:
	v_mainScrollWindow.set_min_content_height(250);
	v_mainScrollWindow.set_min_content_width(250);
	v_mainScrollWindow.set_size_request(250, 250);
	
	// Add main grid to scrollwindow.
	v_mainScrollWindow.add(v_mainGrid);
	
	// Add outer options:
	v_mainGrid.attach(txt_optionLabel, 0, -5, 1, 1);
		txt_optionLabel.set_opacity(0.6);
		txt_optionLabel.set_halign(Gtk::ALIGN_END);
	v_mainGrid.attach(btn_optionLabel, 1, -5, 1, 1);
		btn_optionLabel.set_halign(Gtk::ALIGN_START);
		btn_optionLabel.set_can_focus(true);
		btn_optionLabel.set_can_default(true);
		btn_optionLabel.set_max_length(20);
		
	v_mainGrid.attach(txt_optionManual, 0, -4, 1, 1);
		txt_optionManual.set_opacity(0.6);
		txt_optionManual.set_halign(Gtk::ALIGN_END);
	v_mainGrid.attach(btn_optionManual, 1, -4, 1, 1);
		btn_optionManual.set_halign(Gtk::ALIGN_START);
		btn_optionManual.set_hexpand(true);
		
	v_mainGrid.attach(txt_outputLabel, 0, -3, 1, 1);
		txt_outputLabel.set_opacity(0.6);
		txt_optionLabel.set_halign(Gtk::ALIGN_END);
	v_mainGrid.attach(txt_command, 1, -3, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_command, Pango::ELLIPSIZE_NONE, true, 5, Gtk::ALIGN_START, 0, 0, 150);
	
	// Add expanders to main grid.
	v_mainGrid.attach(v_expander_modes, 0, 0, 2, 1);
	v_mainGrid.attach(v_expander_url, 0, 1, 2, 1);
	v_mainGrid.attach(v_expander_dev, 0, 2, 2, 1);
	v_mainGrid.attach(v_expander_render, 0, 3, 2, 1);
	v_mainGrid.attach(v_expander_network, 0, 4, 2, 1);
	v_mainGrid.attach(v_expander_user, 0, 5, 2, 1);
	v_mainGrid.attach(v_expander_server, 0, 6, 2, 1);
	v_mainGrid.attach(v_expander_gameStats, 0, 7, 2, 1);
	v_mainGrid.attach(v_expander_configFiles, 0, 8, 2, 1);
	v_mainGrid.attach(v_expander_misc, 0, 9, 2, 1);

	// Add grids to expanders.
	v_expander_modes.add(v_grid_modes);
	v_expander_url.add(v_grid_url);
	v_expander_dev.add(v_grid_dev);
	v_expander_render.add(v_grid_render);
	v_expander_network.add(v_grid_network);
	v_expander_user.add(v_grid_user);
	v_expander_server.add(v_grid_server);
	v_expander_gameStats.add(v_grid_gameStats);
	v_expander_configFiles.add(v_grid_configFiles);
	v_expander_misc.add(v_grid_misc);

	// Apply settings to grids.
	setGridSettings(&v_grid_modes);
	setGridSettings(&v_grid_url);
	setGridSettings(&v_grid_dev);
	setGridSettings(&v_grid_render);
	setGridSettings(&v_grid_network);
	setGridSettings(&v_grid_user);
	setGridSettings(&v_grid_server);
	setGridSettings(&v_grid_gameStats);
	setGridSettings(&v_grid_configFiles);
	setGridSettings(&v_grid_misc);

	// MODES
//	v_grid_modes.attach(btn_modes_type, 0, 0, 1, 1);
		btn_modes_type.addNewOption("Game", "-game");
		btn_modes_type.addNewOption("Server", "-server");
//	v_grid_modes.attach(btn_modes_renderer, 0, 1, 1, 1);
		btn_modes_renderer.addNewOption( "Vulkan", "-vulkan");
		btn_modes_renderer.addNewOption( "OpenGL", "-openGL" );
		btn_modes_renderer.addNewOption( "OpenGL 3", "-openGL3");
		btn_modes_renderer.addNewOption( "OpenGL 4", "-openGL4");
		v_arrayComboButtons.push_back(&btn_modes_type);
		v_arrayComboButtons.push_back(&btn_modes_renderer);
		
		
		
	// URL
	v_arrayEntryButtons.push_back(&btn_url_game);
	v_arrayEntryButtons.push_back(&btn_url_name);
	v_arrayToggleButtons.push_back(&btn_url_listen);
	v_arrayToggleButtons.push_back(&btn_url_isLanMatch);
	v_arrayToggleButtons.push_back(&btn_url_isFromInvite);
	v_arrayToggleButtons.push_back(&btn_url_spectatorOnly);

	// DEV
	v_arrayToggleButtons.push_back(&btn_dev_log);
	v_arrayToggleButtons.push_back(&btn_dev_allusers);
	v_arrayToggleButtons.push_back(&btn_dev_auto);
	v_arrayToggleButtons.push_back(&btn_dev_autoCOPackages);
	v_arrayToggleButtons.push_back(&btn_dev_biasCompression);
	v_arrayToggleButtons.push_back(&btn_dev_buildMachine);
	v_arrayToggleButtons.push_back(&btn_dev_bulkImportingSound);
	v_arrayToggleButtons.push_back(&btn_dev_nativeClassSize);
	v_arrayToggleButtons.push_back(&btn_dev_coderMode);
	v_arrayToggleButtons.push_back(&btn_dev_compatScale);
	v_arrayToggleButtons.push_back(&btn_dev_cookDemo);
	v_arrayToggleButtons.push_back(&btn_dev_cookPakages);
	v_arrayToggleButtons.push_back(&btn_dev_crashReports);
	v_arrayToggleButtons.push_back(&btn_dev_d3dDebug);
	v_arrayToggleButtons.push_back(&btn_dev_devCon);
	v_arrayToggleButtons.push_back(&btn_dev_dumpFileStats);
	v_arrayToggleButtons.push_back(&btn_dev_fixedSeed);
	v_arrayToggleButtons.push_back(&btn_dev_fixupTangents);
	v_arrayToggleButtons.push_back(&btn_dev_forceLogFlush);
	v_arrayToggleButtons.push_back(&btn_dev_forcePVRTC);
	v_arrayToggleButtons.push_back(&btn_dev_forceSoundRecook);
	v_arrayToggleButtons.push_back(&btn_dev_genericBrowser);
	v_arrayToggleButtons.push_back(&btn_dev_installed);
	v_arrayToggleButtons.push_back(&btn_dev_lightmassDebug);
	v_arrayToggleButtons.push_back(&btn_dev_lightmassStat);
	v_arrayToggleButtons.push_back(&btn_dev_noConform);
	v_arrayToggleButtons.push_back(&btn_dev_noContentBrowser);
	v_arrayToggleButtons.push_back(&btn_dev_noInnerException);
	v_arrayToggleButtons.push_back(&btn_dev_noLoadStartupPackages);
	v_arrayToggleButtons.push_back(&btn_dev_noPause);
	v_arrayToggleButtons.push_back(&btn_dev_noPauseOnSuccess);
	v_arrayToggleButtons.push_back(&btn_dev_noRC);
	v_arrayToggleButtons.push_back(&btn_dev_noVerifyGC);
	v_arrayToggleButtons.push_back(&btn_dev_noWrite);
	v_arrayToggleButtons.push_back(&btn_dev_seekFreeLoading);
	v_arrayToggleButtons.push_back(&btn_dev_sekFreePackageMap);
	v_arrayToggleButtons.push_back(&btn_dev_seekFreeLoadingPC);
	v_arrayToggleButtons.push_back(&btn_dev_seekFreeLoadingServer);
	v_arrayToggleButtons.push_back(&btn_dev_setThreadNames);
	v_arrayToggleButtons.push_back(&btn_dev_showMissingLoc);
	v_arrayToggleButtons.push_back(&btn_dev_silent);
	v_arrayToggleButtons.push_back(&btn_dev_traceAnimUsage);
	v_arrayToggleButtons.push_back(&btn_dev_loadWarnAsErrors);
	v_arrayToggleButtons.push_back(&btn_dev_unattended);
	v_arrayToggleButtons.push_back(&btn_dev_useUnpublished);
	v_arrayToggleButtons.push_back(&btn_dev_vaDebug);
	v_arrayToggleButtons.push_back(&btn_dev_verbose);
	v_arrayToggleButtons.push_back(&btn_dev_verifyGC);
	v_arrayToggleButtons.push_back(&btn_dev_warnsAsErrors);

	v_arrayEntryButtons.push_back(&btn_dev_automatedMapBuild);
	v_arrayEntryButtons.push_back(&btn_dev_conformDir);
	v_arrayEntryButtons.push_back(&btn_dev_cultureForCook);

	v_arrayComboButtons.push_back(&btn_dev_firewall);
		btn_dev_firewall.addNewOption("Install firewall Intergration", "-INSTALLFW");
		btn_dev_firewall.addNewOption("Uninstall Firewall intergration", "-UNINSTALLFW");
	v_arrayComboButtons.push_back(&btn_dev_logTimes);
		btn_dev_logTimes.addNewOption("Log times enabled", "-LOGTIMES");
		btn_dev_logTimes.addNewOption("Log times disabled", "-NOLOGTIMES");
	v_arrayComboButtons.push_back(&btn_dev_installGE); 
		btn_dev_installGE.addNewOption("Add game...", "-INSTALLGE");
		btn_dev_installGE.addNewOption("Remove game...", "-UNINSTALLGE");
	
// RENDER GRID
	v_arraySpinButtons.push_back(&btn_render_consoleX);
	v_arraySpinButtons.push_back(&btn_render_consoleY);
	v_arraySpinButtons.push_back(&btn_render_winX);
	v_arraySpinButtons.push_back(&btn_render_winY);
	v_arraySpinButtons.push_back(&btn_render_gameResX);
	v_arraySpinButtons.push_back(&btn_render_gameResY);
	v_arraySpinButtons.push_back(&btn_render_benchmarkFPS);
	v_arraySpinButtons.push_back(&btn_render_seconds);
	
	v_arrayToggleButtons.push_back(&btn_render_benchmark);
	v_arrayToggleButtons.push_back(&btn_render_dumpMovie);
	
	v_arrayComboButtons.push_back(&btn_render_vsync);
		btn_render_vsync.addNewOption("Vsync Enabled", "-VSYNC");
		btn_render_vsync.addNewOption("VSync Disabled", "-NOVSYNC");
	v_arrayComboButtons.push_back(&btn_render_windowMode);
		btn_render_windowMode.addNewOption("Windowed", "-WINDOWED");
		btn_render_windowMode.addNewOption("Full Screen", "-FULLSCREEN");
	
	v_arrayEntryButtons.push_back(&btn_render_exec);
	
// NETWORK
	v_arrayToggleButtons.push_back(&btn_network_lanPlay);
	v_arrayToggleButtons.push_back(&btn_network_limitClientTicks);
	v_arrayToggleButtons.push_back(&btn_network_multiHome);
	v_arrayToggleButtons.push_back(&btn_network_networkProfiler);
	v_arrayToggleButtons.push_back(&btn_network_noSteam);
	v_arrayToggleButtons.push_back(&btn_network_primaryNet);
	
	v_arraySpinButtons.push_back(&btn_network_port);


// USER
	v_arrayToggleButtons.push_back(&btn_user_noHomeDir);
	v_arrayToggleButtons.push_back(&btn_user_noForceFeedback);
	v_arrayToggleButtons.push_back(&btn_user_noSound);
	v_arrayToggleButtons.push_back(&btn_user_noSplash);
	v_arrayToggleButtons.push_back(&btn_user_noTextureStream);
	v_arrayToggleButtons.push_back(&btn_user_oneThread);
	v_arrayToggleButtons.push_back(&btn_user_useAllAvailableCores);
	
	v_arrayEntryButtons.push_back(&btn_user_paths);
	v_arrayEntryButtons.push_back(&btn_user_preferredProcessor);


// SERVER
	v_arrayEntryButtons.push_back(&btn_server_login);
	v_arrayEntryButtons.push_back(&btn_server_pass);

// STATS
	v_arrayToggleButtons.push_back(&btn_gameStat_noDatabase);
	v_arrayToggleButtons.push_back(&btn_gameStat_noLiveTags);
	
// CONFIGS
	v_arrayToggleButtons.push_back(&btn_config_englishLocal);
	v_arrayToggleButtons.push_back(&btn_config_noAutoIniUpdate);
	v_arrayToggleButtons.push_back(&btn_config_noIni);
	v_arrayToggleButtons.push_back(&btn_config_regenerateInis);
	
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultEd);
	v_arrayEntryButtons.push_back(&btn_config_iniEditor);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultEdSettings);
	v_arrayEntryButtons.push_back(&btn_config_iniEditorUserSettings);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultCompat);
	v_arrayEntryButtons.push_back(&btn_config_iniCompat);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultLightmass);
	v_arrayEntryButtons.push_back(&btn_config_iniLightmass);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultEngine);
	v_arrayEntryButtons.push_back(&btn_config_iniEngine);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultGame);
	v_arrayEntryButtons.push_back(&btn_config_iniGame);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultInput);
	v_arrayEntryButtons.push_back(&btn_config_iniInput);
	v_arrayEntryButtons.push_back(&btn_config_iniDefaultUI);
	v_arrayEntryButtons.push_back(&btn_config_iniUI);
	
// DEBUGGING
	v_arrayToggleButtons.push_back(&btn_debug_cpuProfileTrace);
	v_arrayToggleButtons.push_back(&btn_debug_statNamedEvents);
	v_arrayToggleButtons.push_back(&btn_debug_fileTrace);
	
	v_arrayEntryButtons.push_back(&btn_debug_traceHost);

	setupButtons();
	
	btn_optionLabel.signal_changed().connect(sigc::mem_fun(*this, &EngineLaunchOpts::btn_optionLabel_changed));
} // END - EngineLaunchOpts Constructor.

EngineLaunchOpts::~EngineLaunchOpts(){}// END - Destructor.

void EngineLaunchOpts::setGridSettings(Gtk::Grid* p_grid){
	p_grid->set_orientation(Gtk::ORIENTATION_VERTICAL);
	p_grid->set_row_homogeneous(false);
	p_grid->set_column_homogeneous(false);
	p_grid->set_row_spacing(10);
	p_grid->set_column_homogeneous(5);
	p_grid->set_border_width(5);
} // END - set grid settings.

void EngineLaunchOpts::setupButtons(){
	
	// 4 for loops required for each button type

	// TOGGLE
	for(EngineLaunchOptToggle* v_button : v_arrayToggleButtons){
		addButtonToGrid(&*v_button);
		v_button->setIniReference( &v_ini );
		v_signalLoadSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptToggle::readSettings));
		v_signalSaveSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptToggle::writeSettings));
		v_button->v_signalUpdateCommand.connect(sigc::mem_fun(*this, &EngineLaunchOpts::updateLaunchOptions));
//		newLaunchOpt->v_signalUpdateTabName.connect(sigc::mem_fun(*this, &EngineProperties::launchOpts_updateLabel));
	} // END for toggle buttons.
	
	// COMBOS
	for(EngineLaunchOptCombobox* v_button : v_arrayComboButtons){
		addButtonToGrid(&*v_button);
		v_button->setIniReference( &v_ini );
		v_signalLoadSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptCombobox::readSettings));
		v_signalSaveSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptCombobox::writeSettings));
		v_button->v_signalUpdateCommand.connect(sigc::mem_fun(*this, &EngineLaunchOpts::updateLaunchOptions));
	} // END for toggle buttons.

	for(EngineLaunchOptSpin* v_button : v_arraySpinButtons){
		addButtonToGrid(&*v_button);
		v_button->setIniReference( &v_ini );
		v_signalLoadSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptSpin::readSettings));
		v_signalSaveSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptSpin::writeSettings));
		v_button->v_signalUpdateCommand.connect(sigc::mem_fun(*this, &EngineLaunchOpts::updateLaunchOptions));
	} // END for toggle buttons.

	for(EngineLaunchOptEntry* v_button : v_arrayEntryButtons){
		addButtonToGrid(&*v_button);
		v_button->setIniReference( &v_ini );
		v_signalLoadSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptEntry::readSettings));
		v_signalSaveSettings.connect(sigc::mem_fun(*v_button, &EngineLaunchOptEntry::writeSettings));
		v_button->v_signalUpdateCommand.connect(sigc::mem_fun(*this, &EngineLaunchOpts::updateLaunchOptions));
	} // END for toggle buttons.

	// Manual button:
	btn_optionManual.signal_changed().connect(sigc::mem_fun(*this, &EngineLaunchOpts::updateLaunchOptions));
	
	v_isReadingData = true;
	// Read config file:
//	readConfigFile();
	v_isReadingData = false;
	updateLaunchOptions();
} // END - Add buttons to grids.

void EngineLaunchOpts::addButtonToGrid(EngineLaunchOptBase* p_button){
	
	Glib::ustring temp_header = p_button->getHeader();
	
	// Check if header is any of the current sections.
	//Placed in MISC if not.
	
	if( temp_header == "modes" ){
		v_grid_modes.add(*p_button);
		return;
	}
	
	if( temp_header == "url" ){
		v_grid_url.add(*p_button);
		return;
	}

	if( temp_header == "dev" ){
		v_grid_dev.add(*p_button);
		return;
	}
	
	if( temp_header == "render" ){
		v_grid_render.add(*p_button);
		return;
	}
	
	if( temp_header == "network" ){
		v_grid_network.add(*p_button);
		return;
	}
	
	if( temp_header == "user" ){
		v_grid_user.add(*p_button);
		return;
	}
	
	if( temp_header == "server" ){
		v_grid_server.add(*p_button);
		return;
	}

	if( temp_header == "gameStats" ){
		v_grid_gameStats.add(*p_button);
		return;
	}
	
	if( temp_header == "config" ){
		v_grid_configFiles.add(*p_button);
		return;
	}

// Removed misc header check, as it now acts as default.
//	if( temp_header == "misc" ){
		v_grid_misc.add(*p_button);
//		return;
//	}
	
} // END - Add button to grid

void EngineLaunchOpts::updateLaunchOptions(){
	// If readingData is true, return from function to avoid uneccesery calls.
	if(v_isReadingData) return;
	
	txt_command.set_text( getLaunchOptions() );
}

Glib::ustring EngineLaunchOpts::getConfigFile(){
	return v_configFile;
} // END- get config file.

Glib::ustring EngineLaunchOpts::getLaunchOptions(){
	Glib::ustring v_launchOptString;
	
	// Add manual entry to start; add space to the end as precaution.
	v_launchOptString = btn_optionManual.get_text() + " ";

	// TOGGLE
	for(EngineLaunchOptToggle* v_button : v_arrayToggleButtons){
			v_launchOptString += v_button->getCommand();
	} // END for toggle buttons.
	
	// COMBOS
	for(EngineLaunchOptCombobox* v_button : v_arrayComboButtons){
		v_launchOptString += v_button->getCommand();
	} // END for toggle buttons.

	for(EngineLaunchOptSpin* v_button : v_arraySpinButtons){
		v_launchOptString += v_button->getCommand();
	} // END for toggle buttons.

	for(EngineLaunchOptEntry* v_button : v_arrayEntryButtons){
		v_launchOptString += v_button->getCommand();
	} // END for toggle buttons.

	return v_launchOptString;
} // END - Get launch options.

Glib::ustring EngineLaunchOpts::getLaunchOptionsQuick(){
	return LauncherUtils::removeElementFromString( txt_command.get_text(), "Command: ", "" );
} // END - Get launch options QUICK.


Glib::ustring EngineLaunchOpts::getLaunchOptLabel(){
	return btn_optionLabel.get_text();
} // END - get launch options label

void EngineLaunchOpts::disableLabel(){
	btn_optionLabel.set_sensitive(false);
}

void EngineLaunchOpts::setDefaultFocus(){
	btn_optionLabel.grab_focus();
}

void EngineLaunchOpts::btn_optionLabel_changed(){
	v_signalUpdateTabName.emit(this, btn_optionLabel.get_text());
} // END - label changed.

void EngineLaunchOpts::readConfigFile(){
	LauncherPrinter v_logger;

	if( LauncherUtils::fileExists( v_configFile ) ){
		SI_Error temp_error = v_ini.LoadFile(v_configFile.c_str());
		if( temp_error != 0 ){
			v_logger.warnErrMsg("Failed to load launch option config file.");
			return;
		} // END- If error occured reading config file.
		v_logger.appendDebug("Emitting signal to load.");
		v_signalLoadSettings.emit();
		
		// Get label & Manual.
		// Only get label if button is not desensitised (due to being the default) and thus overwriting the value manually.
		if(btn_optionLabel.get_sensitive()){
			btn_optionLabel.set_text( v_ini.GetValue("other", "label", "") );
			btn_optionLabel_changed(); // Call manually; assuming it's not been connected.
		} // END - If button is sensitive.
		btn_optionManual.set_text( v_ini.GetValue("other", "manual", "") );
		
	} else{// END If file exists
		v_logger.warnErrMsg("Launch options configuration file does not exist. [Normal for new settings or unset defaults]");
		return;
	} // END - File doesn't exist

} // END - read config file.


void EngineLaunchOpts::writeConfigFile(){
	LauncherPrinter v_logger;
	v_logger.debug("Attempting to save engine launch configuration.");
	SI_Error temp_error = v_ini.LoadFile(v_configFile.c_str());
	std::string temp_dataString;
	
	v_signalSaveSettings.emit();
	
	// Set label & manual:
	v_ini.SetValue("other", "label", btn_optionLabel.get_text().c_str());
	v_ini.SetValue("other", "manual", btn_optionManual.get_text().c_str());
	// Set the command string, so other objects don't need to generate the string.
	v_ini.SetValue("other", "command", getLaunchOptionsQuick().c_str(), "");
	
	temp_error = v_ini.Save(temp_dataString);
	if(temp_error < 0){
		v_logger.errorMsg("Failed to save configuration data for file: " + v_configFile);
		return;
	} // END - if failed to save datastring.
	
	temp_error = v_ini.SaveFile(v_configFile.c_str());
	if(temp_error < 0){
		v_logger.errorMsg("Failed to save configuration file: " + v_configFile);
		return;
	} // END - if failed to save datastring.
	v_logger.appendDebug("Save complete!");
} // END - write config file.