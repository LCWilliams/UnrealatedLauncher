#include "include/launcher_library.h"

using namespace UnrealatedLauncher;

EngineProperties::EngineProperties():
//	btn_cancel("Cancel"),
//	btn_save("Confirm"),
	v_windowControls(false),
	txt_general_installStatusLabel("Install Status: "),
	txt_general_configFileLabel("Config File: "),
	txt_general_installDirLabel("Install Directory: "),
	txt_general_customImgDirLabel("Custom image: "),
	txt_general_labelLabel("Label: "),
	txt_general_descriptionLabel("Description: "),
	txt_general_gitCommitLabel("Commit: "),
	txt_general_versionLabel("Version: "),
	
	btn_launchOpts_addNew("Add New"),
	btn_launchOpts_remove("Remove"),
	btn_launchOpts_save("Save"),
	
	btn_manage_deleteConfig("Delete Config File","This removes the config file responsible for displaying the engine in the launcher.\nIt does NOT remove the engine from the disk!"),
	btn_manage_deleteEngine("Delete Engine", "THIS IS NOT REVERSABLE!  Confirming will instigate full removal of the engine from your disk!")
	
/*EngineProperties()*/ {
	// Settings variables
	float loc_labelOpacity = 0.4;

	// Add main grid to bin
	add(v_mainGrid);
	v_mainGrid.set_hexpand(true);
	v_mainGrid.set_vexpand(true);
	v_mainGrid.set_row_homogeneous(false);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_margin_top(5);
	
	// Attach notebook & buttons to main grid.
	v_mainGrid.attach(v_mainNotebook, 0, 0, 2, 1);
	v_mainGrid.attach(v_windowControls, 1, 1, 1, 1);
	v_windowControls.set_halign(Gtk::ALIGN_END);

	// Add pages to notebook.
	v_mainNotebook.append_page(v_pageGeneral, "_General", true);
	v_mainNotebook.append_page(v_pageLaunchOpts, "Launch _Options", true);
	v_mainNotebook.append_page(v_pagePlugins, "_Plugins", true);
	v_mainNotebook.append_page(v_pageManage, "_Manage", true);
	v_mainNotebook.set_hexpand(true);
	v_mainNotebook.set_vexpand(true);
	v_mainNotebook.set_tab_pos(Gtk::POS_LEFT);
	
	// GENERAL TAB.
	v_pageGeneral.set_row_homogeneous(false);
	v_pageGeneral.set_row_spacing(14);
	v_pageGeneral.set_column_homogeneous(false);
	v_pageGeneral.set_column_spacing(7);
	v_pageGeneral.set_border_width(7);
	v_pageGeneral.set_size_request(400, -1);
	
	// LABEL & DESCRIPTION
	v_mainGrid.attach( txt_general_labelLabel, 0, -3, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_labelLabel );
		txt_general_labelLabel.set_opacity(loc_labelOpacity);
	v_mainGrid.attach( v_general_label, 1, -3, 1, 1 );
		v_general_label.set_hexpand(true);
//		LauncherUtils::setLongLabelTextParameters(&txt_general_installStatus, Pango::ELLIPSIZE_NONE, true, 3);

	v_mainGrid.attach( txt_general_descriptionLabel, 0, -2, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_descriptionLabel );
		txt_general_descriptionLabel.set_opacity(loc_labelOpacity);
	v_mainGrid.attach( v_general_description, 1, -2, 1, 1 );
		v_general_description.set_hexpand(true);
//		LauncherUtils::setLongLabelTextParameters(&txt_general_installStatus, Pango::ELLIPSIZE_NONE, true, 3);

	Gtk::Separator *v_separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
	v_mainGrid.attach(*v_separator, 0, -1, 2, 1);
	v_separator->set_margin_top(5);
	v_separator->set_margin_bottom(5);
	
	v_pageGeneral.attach( txt_general_installStatusLabel, 0, 0, 1, 1);
		LauncherUtils::setLabelTextParameters( &txt_general_installStatusLabel);
		txt_general_installStatusLabel.set_opacity(loc_labelOpacity);
	v_pageGeneral.attach(img_general_installStatus, 1, 0, 1, 1);
		img_general_installStatus.set_halign(Gtk::ALIGN_START);
//		img_general_installStatus.set_hexpand(false);
	v_pageGeneral.attach( txt_general_configFileLabel, 0, 1, 1, 1 );
		LauncherUtils::setLabelTextParameters(&txt_general_configFileLabel );
		txt_general_configFileLabel.set_opacity(loc_labelOpacity);
	v_pageGeneral.attach( txt_general_installDirLabel, 0, 2, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_installDirLabel);
		txt_general_installDirLabel.set_opacity(loc_labelOpacity);
	v_pageGeneral.attach( txt_general_customImgDirLabel, 0, 3, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_customImgDirLabel );
		txt_general_customImgDirLabel.set_opacity(loc_labelOpacity);
	v_pageGeneral.attach( txt_general_gitCommitLabel, 0, 6, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_gitCommitLabel );
		txt_general_gitCommitLabel.set_opacity(loc_labelOpacity);
	v_pageGeneral.attach( txt_general_versionLabel, 0, 7, 1, 1);
		LauncherUtils::setLabelTextParameters(&txt_general_versionLabel );
		txt_general_versionLabel.set_opacity(loc_labelOpacity);
	
	// 2nd Column.
	v_pageGeneral.attach( txt_general_installStatus, 2, 0, 1, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_installStatus, Pango::ELLIPSIZE_NONE, true, 3, Gtk::ALIGN_START, 0, 0, 150);
		txt_general_installStatus.set_hexpand(true);
	v_pageGeneral.attach( txt_general_configFile, 1, 1, 2, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_configFile, Pango::ELLIPSIZE_NONE, true, 3, Gtk::ALIGN_START, 0, 0, 150);
	v_pageGeneral.attach( txt_general_installDir, 1, 2, 2, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_installDir, Pango::ELLIPSIZE_NONE, true, 3, Gtk::ALIGN_START, 0, 0, 150);
	v_pageGeneral.attach( txt_general_customImgDir, 1, 3, 2, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_customImgDir, Pango::ELLIPSIZE_NONE, true, 3);
	v_pageGeneral.attach( txt_general_gitCommit, 1, 6, 2, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_gitCommit, Pango::ELLIPSIZE_NONE, true, 3);
	v_pageGeneral.attach( txt_general_version, 1, 7, 2, 1 );
		LauncherUtils::setLongLabelTextParameters(&txt_general_version, Pango::ELLIPSIZE_NONE, true, 3);

	v_general_label.signal_changed().connect(sigc::mem_fun(*this, &EngineProperties::btn_general_label_updated));


// LAUNCH OPTS TAB
	v_pageLaunchOpts.attach(v_launchOpts_notebook, 0, 0, 3, 1);
	v_launchOpts_notebook.set_hexpand(true);
	v_launchOpts_notebook.set_vexpand(true);
	v_launchOpts_notebook.set_scrollable(true);
//	v_launchOpts_notebook.set_border_width(20);
	v_pageLaunchOpts.attach(btn_launchOpts_save, 0, 1, 1, 1);
	v_pageLaunchOpts.attach(btn_launchOpts_addNew, 1, 1, 1, 1);
	v_pageLaunchOpts.attach(btn_launchOpts_remove, 2, 1,1,1);
	
	btn_launchOpts_addNew.signal_clicked().connect(sigc::mem_fun(*this, &EngineProperties::btn_launchOpts_addNew_clicked));
	btn_launchOpts_remove.signal_clicked().connect(sigc::mem_fun(*this, &EngineProperties::btn_launchOpts_remove_clicked));
	
//	v_launchOpts_notebook.append_page(v_launchOpts_defaultOpts, "DEFAULT", "Default");


// MANAGE TAB
	v_pageManage.set_row_homogeneous(false);
	v_pageManage.set_row_spacing(14);
	v_pageManage.set_column_homogeneous(false);
	v_pageManage.set_column_spacing(7);
	v_pageManage.set_border_width(7);
//	v_pageManage.set_size_request(400, -1);

	v_pageManage.attach(btn_manage_deleteConfig, 0, 0, 1, 1);
	v_pageManage.attach(btn_manage_deleteEngine, 0, 1, 1, 1);
	
	
	btn_manage_deleteEngine.v_signalConfirm.connect(sigc::mem_fun(*this, &EngineProperties::btn_manage_deleteEngine_clicked));
	btn_manage_deleteConfig.v_signalConfirm.connect(sigc::mem_fun(*this, &EngineProperties::btn_manage_deleteConfig_clicked));
	

	// SETTINGS BUTTON SIGNALS.
	v_windowControls.v_signalCancel.connect(sigc::mem_fun(*this, &EngineProperties::btn_cancel_clicked));
	v_windowControls.v_signalConfirm.connect(sigc::mem_fun(*this, &EngineProperties::btn_save_clicked));
} // END - Engine properties constructor.

EngineProperties::~EngineProperties(){} // Destructor.


void EngineProperties::propogateData(EngineData* p_engineData){
	LauncherPrinter v_logger;
	
	
	// DON'T FORGET TO SET REFERENCES!!
	ref_engineData = p_engineData;
	// Set the GUI elements to data.
	// Actually make the install status human readable...
	switch (p_engineData->installStatus){
		case EngineInstallStatusEnum::ready:
			txt_general_installStatus.set_text( "Ready!" ); 
			img_general_installStatus.set_from_icon_name("emblem-default", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		
		// User should never be able to see this, but it's here for completeness.
		case EngineInstallStatusEnum::deleting:
			txt_general_installStatus.set_text( "Engine is being deleted!" ); 
			img_general_installStatus.set_from_icon_name("dialog-error", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		
		case EngineInstallStatusEnum::makeScriptRan:
			txt_general_installStatus.set_text( "NOT READY! \nLast Succesful step: Make script completed." ); 
			img_general_installStatus.set_from_icon_name("dialog-warning", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		case EngineInstallStatusEnum::revertedToCommit:
			txt_general_installStatus.set_text( "NOT READY! \nLast Succesful step: Reverted to commit." ); 
			img_general_installStatus.set_from_icon_name("dialog-warning", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		case EngineInstallStatusEnum::setupScriptRan:
			txt_general_installStatus.set_text( "NOT READY! \nLast Succesful step: Setup script completed." ); 
			img_general_installStatus.set_from_icon_name("dialog-warning", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		case EngineInstallStatusEnum::sourceCopied:
			txt_general_installStatus.set_text( "NOT READY! \nLast Succesful step: Source copied to install directory." ); 
			img_general_installStatus.set_from_icon_name("dialog-warning", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
		case EngineInstallStatusEnum::newBlock:
			txt_general_installStatus.set_text( "NOT READY! \nInstallation has not been started." ); 
			img_general_installStatus.set_from_icon_name("dialog-error", Gtk::ICON_SIZE_SMALL_TOOLBAR);
		break;
	} // END - Install Status switch
	
	txt_general_configFile.set_text( p_engineData->configFile );
	txt_general_installDir.set_text( p_engineData->installDir );
	txt_general_customImgDir.set_text( p_engineData->imageDir );
	v_general_label.set_text( p_engineData->label );
	v_general_description.set_text( p_engineData->description );
	txt_general_gitCommit.set_text( p_engineData->gitCommit );
	txt_general_version.set_text( std::to_string(p_engineData->version) + "." + std::to_string(p_engineData->hotfix) );
	
	// Clear the launch options here.  Doesn't work if done on cancel/save clicked.
	clearLaunchOpts();

	v_logger.debug("ENGINE DATA HOLDS: " + std::to_string(ref_engineData->launchOptions.size()) + " LAUNCH OPTS");
	for(unsigned int index_launchOpts = 0; index_launchOpts < p_engineData->launchOptions.size(); ++ index_launchOpts){
		Glib::ustring temp_configFile = p_engineData->launchOptions[index_launchOpts];
		EngineLaunchOpts *v_launchOpts = Gtk::manage(new EngineLaunchOpts( p_engineData, index_launchOpts ) );
		v_launchOpts->v_signalUpdateTabName.connect(sigc::mem_fun(*this, &EngineProperties::launchOpts_updateLabel));
		v_launchOpts_array.push_back(v_launchOpts);
		v_launchOpts_notebook.append_page( *v_launchOpts, "[Unsaved Settings]" );
		if(index_launchOpts == 0){
			v_launchOpts->disableLabel();
			v_launchOpts_notebook.set_tab_label_text(*v_launchOpts, "Default");
		} // END - if default
	} // END - LaunchOpt for loop.
	
	loadLaunchOpts();
	v_launchOpts_notebook.show_all();
	v_launchOpts_notebook.grab_focus(); // Stops the label entries from taking focus; ensures consistent behaviour.
	v_general_label.set_placeholder_text( std::to_string(ref_engineData->identifier) );
} // END - Propogate data.


void EngineProperties::btn_cancel_clicked(){
	v_closeSignal.emit();
	clearLaunchOpts();
} // END - Cancel button clicked.

void EngineProperties::btn_save_clicked(){
	// Save launch options.
	saveLaunchOpts();
	// Close properties.
	v_closeSignal.emit();
	// Set the Engine label & Description
	ref_engineData->label = v_general_label.get_text();
	ref_engineData->description = v_general_description.get_text();
	// Prompt save data.
	ref_engineData->saveData();
	// Update the launch buttons...
	// Generate the two vectors.
	std::vector<Glib::ustring> temp_labels, temp_commands;
	
	for(unsigned int index = 0; index < v_launchOpts_array.size(); ++index){
		temp_labels.push_back( v_launchOpts_array[index]->getLaunchOptLabel() );
		temp_commands.push_back( v_launchOpts_array[index]->getLaunchOptions() );
	} // END - Launch opt forloop.
	
	// Clear launch options.
	clearLaunchOpts();
	
	ref_engineData->ref_engineGUI->updateLaunchButtons(temp_labels, temp_commands);
	ref_engineData->ref_engineGUI->updateLabelsFromData();
	
} // END - save button clicked.

void EngineProperties::clearLaunchOpts(){
	int loc_pages = v_launchOpts_notebook.get_n_pages();
	
	LauncherPrinter v_logger;
	for(int pageIndex = 0; pageIndex < loc_pages; ++pageIndex){
		v_logger.debug("Removed launch option page: " + std::to_string(pageIndex+1) + "|" + std::to_string(loc_pages));
		v_launchOpts_notebook.remove_page(pageIndex);
	} // END - For loop to remove pages.

	// Clear array itself (doing this doesn't clean up the pointers.)
	v_launchOpts_array.clear();
} // END - Clear launch options.


void EngineProperties::btn_general_label_updated(){
	if( v_general_label.get_text().empty() ){
		v_windowControls.setConfirmBtnSensitive(false);
	} else{// END - If label entry is empty.
		v_windowControls.setConfirmBtnSensitive(true);
	} // END - If entry is NOT empty.
} // END - engine label updated



void EngineProperties::btn_manage_deleteConfig_clicked(){
	v_closeSignal.emit();
	ref_engineData->ref_engineGUI->instigateConfigRemoval();
} // END - Delete config button clicked.

void EngineProperties::btn_manage_deleteEngine_clicked(){
	v_closeSignal.emit();
	ref_engineData->ref_engineGUI->instigateRemoval();
} // END - delete engine clicked.



void EngineProperties::btn_launchOpts_addNew_clicked(){
	EngineLaunchOpts* newLaunchOpt = Gtk::manage(new EngineLaunchOpts(ref_engineData, ref_engineData->addNewLaunchOpt() ));
	// Add it to the array.
	v_launchOpts_array.push_back(newLaunchOpt);
	
	v_launchOpts_notebook.append_page(*newLaunchOpt, "new options");
	newLaunchOpt->v_signalUpdateTabName.connect(sigc::mem_fun(*this, &EngineProperties::launchOpts_updateLabel));
	// Makes sure it shows up!
	v_launchOpts_notebook.show_all();
	v_launchOpts_notebook.set_current_page(-1); // Swaps the notebook tab to the new tab.
	newLaunchOpt->setDefaultFocus(); // Sets the focus to be the label entry.
} // END - Add new launch opt.

void EngineProperties::btn_launchOpts_remove_clicked(){
	LauncherPrinter v_logger;
	// Int to store current page:
	int temp_page = v_launchOpts_notebook.get_current_page();
	if( temp_page != 0){
		// Check if config file text is valid:
		if( ref_engineData->launchOptions[temp_page].is_ascii() ){
			// Remove the ini file.
			if( !LauncherUtils::removeFile( v_launchOpts_array[temp_page]->getConfigFile() ) ){
				return;
			} // END - If file removal fails.
		} // END - If launchOption config file path is valid. 

		v_launchOpts_notebook.remove_page( temp_page );
	
		// Remove from array: get start element, then add pageCount.
		v_launchOpts_array.erase( v_launchOpts_array.begin() +temp_page);
		
		// Reconsturct array (doing it as above for whatever reason doesn't work).
		ref_engineData->launchOptions.clear();
		for(unsigned int optIndex = 0; optIndex < v_launchOpts_array.size(); ++optIndex ){
			ref_engineData->launchOptions.push_back( v_launchOpts_array[optIndex]->getConfigFile() );
		} // END - reconstruction forloop

		// Save the file:
		ref_engineData->saveData();

	v_logger.debug("Removed launch option!");
	v_logger.appendDebug("LaunchOpts : " + std::to_string( ref_engineData->launchOptions.size() ));
	v_logger.appendDebug("LOptsArray : " + std::to_string( v_launchOpts_array.size() ));

	} // END - if current page is not the first/default, remove it.
} // END - Remove launch option clicked


void EngineProperties::btn_launchOpts_save_clicked(){
	// Probably move this to inside the launch opt.
} // END - Save clicked.


void EngineProperties::launchOpts_updateLabel(Gtk::Widget* p_page, Glib::ustring p_newLabel){
	v_launchOpts_notebook.set_tab_label_text( *p_page, p_newLabel );
} // END - update tab label.


void EngineProperties::loadLaunchOpts(){
	LauncherPrinter v_logger;
	v_logger.debug("Loading launch options...");
	
	for(EngineLaunchOpts* currentOptions : v_launchOpts_array){
		currentOptions->readConfigFile();
	} // END - for loop.
	
//	for(unsigned int optIndex = 0; optIndex < v_launchOpts_array.size(); ++optIndex){
//		v_logger.appendDebug("Reading config file: " + v_launchOpts_array[optIndex]->getConfigFile());
//		v_launchOpts_array[optIndex]->readConfigFile();
//	} // END - For loop.
	
	
} // END - load launch opts.

void EngineProperties::saveLaunchOpts(){
	LauncherPrinter v_logger;
	v_logger.debug("Saving launch options...");
//	for(Glib::ustring currentIndex : ref_engineData->launchOptions){
//		v_logger.appendDebug(currentIndex);
//	}
	
	for(EngineLaunchOpts* currentOptions : v_launchOpts_array){
		currentOptions->writeConfigFile();
	} // END - for loop.
	v_logger.appendDebug("Completed");
} // END - save launch options.