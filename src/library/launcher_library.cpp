#include "include/launcher_library.h"

using namespace UnrealatedLauncher;

LauncherLibrary::LauncherLibrary():
v_enginesFrame("ENGINES"),
btn_addEngine("Add Engine")
{

	// Create objects:
	ref_repositoryManager = new LauncherRepositoryManager();
	v_repoManagerGUI = Gtk::manage(new LauncherRepoManagerGUI(ref_repositoryManager));
	ref_addEngineGUI = Gtk::manage(new LauncherAddEngine(ref_repositoryManager));
	ref_enginePropertiesGUI = Gtk::manage(new EngineProperties());
	
	// Set settings for library bin:
	set_hexpand(true);
	set_vexpand(true);
	
	// Add the main grid to the Library Bin.
	add(v_libraryStack);
	
	// Add padding to the main grid borders:
	set_border_width(3);
	
	// Attach the various pages to the stack:
	v_libraryStack.add(v_mainGrid);
//	v_libraryStack.add(*v_repoManagerGUI);
	v_libraryStack.add(*ref_addEngineGUI);
	v_libraryStack.add(*ref_enginePropertiesGUI);
	
	// Set stack options:
	v_libraryStack.set_transition_duration(350);
	v_libraryStack.set_transition_type(Gtk::STACK_TRANSITION_TYPE_SLIDE_UP);
	v_libraryStack.set_vexpand(true);
	v_libraryStack.set_hexpand(true);
	
	
// ENGINES:
	// Attach the engine elements to the main grid.
	v_mainGrid.attach(v_enginesFrame, 0, 0, 1, 1);
	v_enginesFrame.add(v_enginesGrid);
	v_enginesGrid.attach(v_enginesActionbar, 0, 1, 1, 1);
	
	// Set settings for frame:
	v_enginesFrame.set_hexpand(true);
	v_enginesFrame.set_vexpand(true);
	
	// Add flowbox & scroll window to engine grid:
	v_enginesScrollWindow.add(v_enginesFlowbox);
		v_enginesFlowbox.set_selection_mode(Gtk::SELECTION_NONE);
		v_enginesFlowbox.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
//		v_enginesFlowbox.set_max_children_per_line(10);
		v_enginesFlowbox.set_homogeneous(false);
		
	v_enginesGrid.attach(v_enginesScrollWindow, 0, 0, 1, 1);
	v_enginesScrollWindow.set_hexpand(true);
	v_enginesScrollWindow.set_vexpand(true);
	
	
	// Action bar packing & setup:
//	v_enginesActionbar.pack_start(btn_repoManager);
	v_enginesActionbar.pack_start(btn_addEngine);
	v_enginesActionbar.pack_end(v_repoManagerProgress);
		v_repoManagerProgress.set_size_request(300, -1);
		v_repoManagerProgress.set_show_text(true);
		v_repoManagerProgress.set_text("");
	v_enginesActionbar.set_valign(Gtk::ALIGN_START);
	
	
	// Signals for buttons:
	btn_addEngine.signal_clicked().connect(sigc::mem_fun(*this, &LauncherLibrary::btn_addEngine_clicked));
	v_repoManagerGUI->btn_closeManager.signal_clicked().connect(sigc::mem_fun(*this, &LauncherLibrary::returnToLibrary));
	ref_addEngineGUI->v_signalCloseWindow.connect(sigc::mem_fun(*this, &LauncherLibrary::returnToLibrary));
	ref_addEngineGUI->v_signalAddEngine.connect(sigc::mem_fun(*this, &LauncherLibrary::addNewEngine));
//	ref_repositoryManager->v_signalUpdateProgress.connect(sigc::mem_fun(*this, &LauncherLibrary::updateRepoManagerProgress));

	addExistingEngines();
	
} // END - Launcher Library Constructor.

LauncherLibrary::~LauncherLibrary(){}

void LauncherLibrary::addNewEngine(){
	// Call return to library to hide addEngine Gui window:
	EngineData *ref_newEngineData = ref_addEngineGUI->getEngineData();

	addEngine(ref_newEngineData);
	
	returnToLibrary();
} // END - Add new engine.

void LauncherLibrary::openAddNewEngine(){
	btn_addEngine_clicked();
} // END - OpenAddNewEngine.



void LauncherLibrary::btn_addEngine_clicked(){
//	if( !ref_repositoryManager->getRepoExists() ){
	if( LauncherSettings::firstRun ){
		v_logger.debug("Add Engine: First Run!");
		// Pre-set the visibility of the repository manager to guide the user.
		ref_addEngineGUI->setupFirstRunGUI();
	} // END - if launcher repository doesn't exist
	v_libraryStack.set_visible_child(*ref_addEngineGUI);
	
} // END - Button add engine clicked.

void LauncherLibrary::returnToLibrary(){
	v_libraryStack.set_visible_child(v_mainGrid);
} // END - Return to library.

void LauncherLibrary::updateRepoManagerProgress(Glib::ustring p_text, double p_progress){
	if(v_repoManagerProgress.get_visible()){
		v_repoManagerProgress.set_text(p_text);
		if(p_progress == 0){
			v_repoManagerProgress.pulse();
		} else{// else set fraction:
			v_repoManagerProgress.set_fraction(p_progress);
		}// END - ifelse progress is 0.
	} // END - If progress bar is visible.
} // END - Update repo manager progressbar.



void LauncherLibrary::addExistingEngines(){
	// config file directory.
	DIR *temp_configFolder = opendir( LauncherSettings::engineConfigs.c_str() );
	// next file directory entry
	dirent *temp_nextFile;
	
	while ( (temp_nextFile = readdir(temp_configFolder) ) != NULL ){
		if( strcmp(temp_nextFile->d_name, ".") && strcmp(temp_nextFile->d_name, "..") ){
			
			std::string::size_type temp_strSizeType; 
			Glib::ustring temp_fullPath = LauncherSettings::engineConfigs + temp_nextFile->d_name; // Path of the config dir AND filename.
			// Temp variable to house "temp-nextFile->d_name" as a string.
			Glib::ustring temp_nextFileAsStr = temp_nextFile->d_name;

			// Find "ini" in file name.  Assume if not found not a valid engine config.
			temp_strSizeType = temp_nextFileAsStr.rfind(".ini");
			
			if( temp_strSizeType != std::string::npos ){
				v_logger.debug("Adding existing engine: " + temp_nextFileAsStr);
				
				// Create new engineData object.
				EngineData *v_newDetails = new EngineData;
				v_newDetails->configFile = temp_fullPath;
				v_newDetails->loadData();

				if( v_newDetails->sanityCheck() ){
					addEngine(v_newDetails);
				}// END - Don't add enigne if sanity check failed on add existing.
			} // END - If ".ini" was found.
		} // END - if next file directory name is not directory.
	} // END - While next file is not null.
} // END - Add existing engines.



void LauncherLibrary::addEngine(EngineData* ref_engineData){
	// Create a new engine obect, using the provided engineData.
	LauncherEngine *v_newEngine = Gtk::manage(new LauncherEngine(ref_engineData));
	
	// Add the engine to the flowbox.
	v_enginesFlowbox.add(*v_newEngine);
	
	// Connect signals.
	v_newEngine->v_signalDeletionComplete.connect(sigc::mem_fun(*this, &LauncherLibrary::removeEngine));
	v_newEngine->v_signalEngineProperties.connect(sigc::mem_fun(*this, &LauncherLibrary::showEngineProperties));
	v_repoManagerGUI->v_signalManagerBusy.connect(sigc::mem_fun(*v_newEngine, &LauncherEngine::updateRepoManagerStatus));
	ref_enginePropertiesGUI->v_closeSignal.connect(sigc::mem_fun(*this, &LauncherLibrary::returnToLibrary ));
	

// Debug printing:
	v_logger.debug("ENGINE PROPERTIES:");
	v_logger.appendDebug("Install Dir: " + ref_engineData->installDir);
	v_logger.appendDebug("Sanity check: " + std::to_string(ref_engineData->isSane));

} // END - add engine.


void LauncherLibrary::removeEngine(LauncherEngine* ref_engineBlock){
	v_logger.debug("Removing engine GUI.");
	// Get the flowboxchild parent, and delete it.
	try{
		v_enginesFlowbox.remove( *ref_engineBlock->get_parent() );
//		ref_engineBlock->get_parent()->hide();
//		delete ref_engineBlock;
	} // END - try deleting.
	
	catch(std::exception& temp_error){
		Glib::ustring temp_message = "Failed to remove engine! ";
		temp_message += temp_error.what();
		v_logger.errorMsg(temp_message);
	}// END - Catch delete
	
} // END - remove engine.

void LauncherLibrary::showEngineProperties(LauncherEngine* p_engineGUI){
//	ref_enginePropertiesGUI->propogateData(p_dataRef);
	ref_enginePropertiesGUI->propogateData(p_engineGUI->getEngineData());
	v_libraryStack.set_visible_child(*ref_enginePropertiesGUI);
} // END - Show engine properties.