#include "include/library/library_repoManager.h"
#include "external/git2.h"

using namespace UnrealatedLauncher;

// CALLBACKS:

// Called when git credentials are required/incorrect.
int getCredentials(git_cred **output, const char *url, const char *username_from_url, unsigned int allowed_types, void *payload){
	// Create temp reference else suffer having "((LauncherRepositoryManager*)payload)" before each variable.
	LauncherRepositoryManager* temp_payloadRef = (LauncherRepositoryManager*)payload;

	LauncherPrinter v_logger;

	v_logger.debug("Credentials called!");
//	std::cout
//		<<	"		> URL:			" << url << std::endl
//		<<	"		> Username:		" << username_from_url << std::endl
//		<< 	"		> AllowedTypes: " << allowed_types << std::endl;
	
	// Check if details given by first login have been used:
	if( ((LauncherRepositoryManager*)payload)->v_triedFirstDetails ){
		
	
		// If git authentification failed...
		if( !temp_payloadRef->v_gitAuthPassed ){
			v_logger.warnMsg("Auth failed! Incorrect credentials given!");

			// Set value to true.
			temp_payloadRef->v_waitingOnUserInput = true;
			temp_payloadRef->v_promptUserInputCalled = false;
			
			/* Wait for user to input credentials.  
			User clicking LOGIN will set parameter to true.
			First time going through the loop will set 2nd parameter to FALSE.
			*/
			while(	temp_payloadRef->v_waitingOnUserInput == true){
				// Emit signal to prompt user.
				if(		temp_payloadRef->v_promptUserInputCalled == false){
					temp_payloadRef->notifyPromptLogin();
				} // END - if promptuserinputcalled is false 
			} // END - While waiting on user input.
			v_logger.appendDebug("Continuing credentials callback...");
		} // END - If git auth DIDN'T pass (failed).
	} // END - tried details given on first login.
	
	// Set triedFirstDetails to true, so on concurrent callbacks it will enter the IF.
	temp_payloadRef->v_triedFirstDetails = true;
	
	// IF SSH login is used:
	if( temp_payloadRef->getUseSSH() ){
		Glib::ustring temp_Message;
		temp_Message = "Git SSH Public key.... " + LauncherSettings::sshAuthPubKey;
		v_logger.appendDebug(temp_Message);
		temp_Message = "Git SSH Private key... " + LauncherSettings::sshAuthPrivKey;
		v_logger.appendDebug(temp_Message);
		return git_cred_ssh_key_new(output, 
									username_from_url,
									LauncherSettings::sshAuthPubKey.c_str(),
									LauncherSettings::sshAuthPrivKey.c_str(), "");
	} else{
	// IF HTTPS login is used:
		return git_cred_userpass_plaintext_new(	output, 
												temp_payloadRef->getUserEmail().c_str(), 
												temp_payloadRef->getUserPassword().c_str());
	} // END - else: using HTTPS login
} // END - get credentials.


// callback for fetching progress.
int getProgress(const git_transfer_progress *stats, void *payload){
	// Create temp reference else suffer having "((LauncherRepositoryManager*)payload)" before each variable.
	LauncherRepositoryManager* temp_payloadRef = (LauncherRepositoryManager*)payload;

	// If this callback is called, Auth has passed and should not prompt user.
	temp_payloadRef->v_gitAuthPassed = true;
	
	int temp_fetchpercent = (100 * stats->received_objects) / stats->total_objects;
	double temp_fetchAsDouble = double(stats->received_objects) / stats->total_objects;
	
	int temp_indexingPercent = (100 * stats->indexed_objects) / stats->total_objects;
	double temp_indexAsDouble = double(stats->indexed_objects) / stats->total_objects;
	
	double temp_totalProgress = (temp_fetchAsDouble + temp_indexAsDouble) * 0.5;
	
	Glib::ustring temp_updateText = "Download: " + std::to_string(temp_fetchpercent) + "% | Indexing: " + std::to_string(temp_indexingPercent) +"%";
	
	temp_payloadRef->setThreadComm_taskData(false, temp_totalProgress, temp_updateText);
	
	return 0;
} // END - get progress

// Callback for checkout progress.
void getCheckoutProgress(const char *path, size_t completed_steps, size_t total_steps, void *payload){
	// Create temp reference else suffer having "((LauncherRepositoryManager*)payload)" before each variable.
	LauncherRepositoryManager* temp_payloadRef = (LauncherRepositoryManager*)payload;
	
	// If this callback is called, Auth has passed and should not prompt user.
	temp_payloadRef->v_gitAuthPassed = true;
	
	// To avoid floating point exception, check to ensure total steps is NOT 0.
	if(total_steps != 0){
		unsigned int temp_percentAsInt = completed_steps / (100 * total_steps);
		double temp_percentAsDouble	= double(completed_steps) / total_steps;
		Glib::ustring temp_progressText = "Checking out..." + std::to_string(temp_percentAsInt) + "% complete.";
		temp_payloadRef->setThreadComm_taskData(false, temp_percentAsDouble, temp_progressText);
	} else {
		temp_payloadRef->setThreadComm_taskData(false, 0, "No steps required.");
	} // END - if/else total steps is not 0.
	
} // END - Get checkout progress

// Callback for repository "for-each-tag" loop.
int forEachTag_populateLists(const char *name, git_oid *oid, void *payload){

		// Create temp reference else suffer having "((LauncherRepositoryManager*)payload)" before each variable.
		LauncherRepositoryManager* temp_payloadRef = (LauncherRepositoryManager*)payload;
		LauncherRepositoryManager::typeListStore temp_listStoreRef = temp_payloadRef->getCommitTreeModel(false); // False to get TAG treemodel.
		
	// SPLICE UP NAME INTO STRINGS.
		// Obtain Release type.  Find first hypen, then remove all text prior.
		Glib::ustring temp_buffer = name;
		std::size_t temp_position = temp_buffer.find_first_of("-");
		Glib::ustring temp_type = temp_buffer.erase(0, temp_position+1);
		

		// Obtain version.
		temp_buffer = LauncherUtils::removeElementFromString( name, "refs/tags/4.", "").c_str();
		temp_position = temp_buffer.find_first_of("-");
		std::size_t temp_endPosition = temp_buffer.length();
		temp_buffer.erase(temp_position, temp_endPosition);
		int temp_version = std::stod(temp_buffer);


		// Obtain Hotfix.
		Glib::ustring temp_Hotfixbuffer = LauncherUtils::removeElementFromString( name, "refs/tags/4.", "").c_str();
		
		Gtk::TreeModel::Row temp_row = *(temp_listStoreRef->append());
		
		if( temp_buffer.find_first_of(".") != std::string::npos ){
			temp_endPosition = temp_buffer.find_first_of(".", 0);

			// +1 end position to remove period.
			temp_buffer = temp_buffer.erase(0, temp_endPosition+1);
			unsigned int temp_hotfix = std::stod(temp_buffer);
			
			temp_row[temp_payloadRef->v_repoCommitColumns.v_tagHotfix] = temp_hotfix;
		} // END - if "." exists within string.
		
		temp_row[temp_payloadRef->v_repoCommitColumns.v_commitID] = git_oid_tostr_s(oid);
		temp_row[temp_payloadRef->v_repoCommitColumns.v_tagType] = temp_type;
		temp_row[temp_payloadRef->v_repoCommitColumns.v_tagVersion] = temp_version;
		
		temp_payloadRef->setThreadComm_taskData(false, 0, "Getting tags...");
		
		return 0;
} // END - for each tag populate lists.

// END - CALLBACKS.

UnrealatedLauncher::LauncherRepositoryManager::LauncherRepositoryManager(){
	v_repoManagerThread = nullptr;
	v_taskInProgress = false;
} // END - Launcher Repo manager constructor.

UnrealatedLauncher::LauncherRepositoryManager::~LauncherRepositoryManager(){}


void LauncherRepositoryManager::notifyUpdateProgress(){
	dispatcherUpdateProgress.emit();
} // END - notify update progress.

void LauncherRepositoryManager::notifyPromptLogin(){
	dispatcherPromptLogin.emit();
} // END - notify prompt login.

void LauncherRepositoryManager::notifyUpdateLists(){
	dispatcherUpdatedLists.emit();
} // END - Notify update lists.

void LauncherRepositoryManager::notifyTaskFinished(){
	dispatcherTaskEnded.emit();
	closeThread();
} // END - notify task finished


void LauncherRepositoryManager::closeThread(){
	// SAFETY CHECK: Ensure thread object exists.
	// Else repeated calls to function will segfault.
	if( v_repoManagerThread && v_repoManagerThread->joinable() ){
		v_repoManagerThread->join();
	} // END - if thread is joinable.
	// Delete thread.
	deleteThread();
} // END - Close thread.


void LauncherRepositoryManager::deleteThread(){
	delete v_repoManagerThread;
	v_repoManagerThread = nullptr;
} // END - Delete thread.


void LauncherRepositoryManager::getThreadComm_taskData(Glib::ustring& p_message, double& p_percentage){
	// Try and lock mutex, if unsucessful, GUI will update using blank message & percent.
	if(threadComm_mutex_taskData.try_lock()){
		p_message = threadComm_taskMessage;
		p_percentage = threadComm_taskPercent;
		threadComm_mutex_taskData.unlock();
	} 
} // END - Get Threadcomm Task Data.

void LauncherRepositoryManager::setThreadComm_taskData(bool p_failed, double p_percent, Glib::ustring p_message){
	threadComm_mutex_taskData.lock();
	threadComm_taskFailed = p_failed;
	threadComm_taskPercent = p_percent;
	threadComm_taskMessage = p_message;
	threadComm_mutex_taskData.unlock();
	
	// Notify dispatcher so GUI gets updated:
	notifyUpdateProgress();
} // END - Set threadcomm task data.


Glib::ustring LauncherRepositoryManager::getUserEmail(){
	return v_gitHTTPEmail;
} // END - Get user email.


Glib::ustring LauncherRepositoryManager::getUserPassword(){
	return v_gitHTTPPass;
} // END - get user password.


bool LauncherRepositoryManager::getUseSSH(){
//	return v_SSHLogin; // Redundant- use value from settings.
	// Check to make sure public & private keys exist as precaution.
	// Set temp variables for sanity to avoid repetition
	bool temp_sshPubKeyExists = LauncherUtils::fileExists( LauncherSettings::sshAuthPubKey );
	bool temp_sshPrivKeyExists = LauncherUtils::fileExists( LauncherSettings::sshAuthPrivKey );
	
	if( LauncherSettings::useSSHAuth && temp_sshPubKeyExists && temp_sshPrivKeyExists ){
		return LauncherSettings::useSSHAuth;
	} else{
		// Send debug message to say private/public keys don't exist:
		if( !temp_sshPrivKeyExists ){
			v_logger.warnErrMsg( "Use SSH Enabled, but private key doesn't exist!" );
			v_logger.warnErrMsg( "Private Key: " + LauncherSettings::sshAuthPrivKey );
		} // END - if private key doesn't exist.
		
		if( !temp_sshPubKeyExists ){
			v_logger.warnErrMsg( "Use SSH Enabled, but private key doesn't exist!" );
			v_logger.warnErrMsg( "Public Key: " + LauncherSettings::sshAuthPubKey );
		} // END - If public key doesn't exist
	} // END - if public & private SSH keys don't exist.
	
	return false;
} // END - Get UseSSH



void LauncherRepositoryManager::btn_userLoggedIn(){
	v_waitingOnUserInput = false;
	v_logger.debug("User input complete");
} // END - btn user logged in.



bool LauncherRepositoryManager::getRepoExists(){
	v_logger.debug("Checking launcher repository exists...");

	if(git_libgit2_init() < 0){
		v_logger.errorMsg("Failed to initialise libgit2");
		return false;
	}// END - If fail to load libgit2.
	
	if(git_repository_open(&v_repoMan_repository, LauncherSettings::launcherRepositoryDirectory.c_str()) < 0 ){
		v_logger.appendDebug("Launcher repository DOES NOT exist!");
		return false;
	} else{
		v_logger.appendDebug("Launcher repository DOES exist!");
		return true;
	} // END - git repo open.
	
} // END - get repo exists.



void LauncherRepositoryManager::setHTTPLoginCredentials(Glib::ustring p_email = "", Glib::ustring p_password = ""){
	v_gitHTTPEmail = p_email;
	v_gitHTTPPass = p_password;
	v_waitingOnUserInput = false;
	v_waitingOnUserInput = false;
	v_logger.debug("HTTP login credentials set!");
} // END - Set HTTP login credentials.



void LauncherRepositoryManager::taskFailed(Glib::ustring p_failedInfo, Glib::ustring p_debugInfo, bool p_isGitRelated){
	// Set bool so future tasks can run.
	v_taskInProgress = false;
	
	Glib::ustring temp_messageToPrint = "TASK FAILED!	" + p_debugInfo;
	
	if(p_isGitRelated){
		// Create error object:
		const git_error *temp_error = giterr_last();
		// append git error to message:
		temp_messageToPrint += " | GIT ERROR: "; 
		// Check to see if temp_error is NOT a null pointer before appending message.  Causes segfault if not.
		if(temp_error!=NULL){
			temp_messageToPrint += temp_error->message;
		} // END - temp error not null
		
		// Shutdown libgit2.
		git_libgit2_shutdown();
	}
	// Print out error.
	v_logger.errorMsg(temp_messageToPrint);
	
	setThreadComm_taskData(true, 0, temp_messageToPrint);
	
	// Emit task finished:
	notifyTaskFinished();

} // END - Task failed.



int LauncherRepositoryManager::gitTaskFailed(){
	v_logger.errorMsg("GIT task failed!");
	const git_error *temp_error = giterr_last();
	if(temp_error != NULL){
		// Print out error for convenience (and debuggging!)
		Glib::ustring temp_message;
		temp_message = "GIT ERR:"; 
		temp_message += std::to_string(temp_error->klass);
		temp_message += "	|	MESSAGE:"; 
		temp_message += temp_error->message;
		v_logger.errorMsg(temp_message);
		
		return temp_error->klass;
	} // END - If error isn't null.
	else{
		v_logger.errorMsg("GIT ERR was null.");
		return 1;
	} // END - tempError was null.
} // END - Git task failed.


void LauncherRepositoryManager::setAuthMethod(bool p_useSSH){
	v_SSHLogin = p_useSSH;
	if(p_useSSH){		
		// Check to ensure SSH keys exist
		if( LauncherUtils::fileExists( LauncherSettings::sshAuthPubKey ) && LauncherUtils::fileExists( LauncherSettings::sshAuthPrivKey ) ){
			v_repoURL = v_repoURLSSH;
		} else{
			v_logger.warnErrMsg("Use SSH enabled, but public and/or private key does not exist! Using HTTPS instead.");
			setAuthMethod(false);
		} // END - if public & private SSH keys don't exist.
	} else{
		v_repoURL = v_repoURLHTTP;
	}
} // END - Set Auth Method.



void LauncherRepositoryManager::instigateDownloadRepository(){
	if(v_repoManagerThread){
		v_logger.warnMsg("TASK ALREADY IN PROGRESS!");
	} else{
		// Set task in progress to be true.
		v_taskInProgress = true;
		v_lastThreadTask = lastThreadTask::download;
		v_repoManagerThread = new std::thread(&LauncherRepositoryManager::downloadRepository, this);
		v_repoManagerThread->detach();
	}
} // END - Instigate download repository.



void LauncherRepositoryManager::instigateUpdateRepository(){
	if(v_repoManagerThread){
		v_logger.warnMsg("TASK ALREADY IN PROGRESS!");
	}else{
		// REDUNDANT: to remove.
		//Set task in progress to be true:
		v_taskInProgress=true;
		v_lastThreadTask = lastThreadTask::updateRepo;
		v_repoManagerThread = new std::thread(&LauncherRepositoryManager::updateRepository, this);
		v_repoManagerThread->detach();
	} // END - if task not in progress.
} // END - instigate update repository.



void LauncherRepositoryManager::instigateRepoDeletion(){
	if(v_repoManagerThread){
		v_logger.warnMsg("TASK ALREADY IN PROGRESS!");
	}else{
		//Set task in progress to be true:
		v_taskInProgress = true;
		v_lastThreadTask = lastThreadTask::deleteRepo;
		v_repoManagerThread = new std::thread(&LauncherRepositoryManager::deleteRepository, this);
		v_repoManagerThread->detach();
	} // END - if task not in progress.
}// END - Instigate repo deletion.


void LauncherRepositoryManager::instigateUpdateRepoLists(){
	if(v_repoManagerThread){
		v_logger.warnMsg("TASK ALREADY IN PROGRESS!");
	}else{
		//Set task in progress to be true:
		v_taskInProgress = true;
		v_lastThreadTask = lastThreadTask::updateLists;
		v_repoManagerThread = new std::thread(&LauncherRepositoryManager::updateRepoLists, this);
		v_repoManagerThread->detach();
	} // END - if task not in progress.
} // END - Instigate repo list update.


// THREAD FUNCTIONS		 THREAD FUNCTIONS		 THREAD FUNCTIONS		 THREAD FUNCTIONS		 THREAD FUNCTIONS		 THREAD FUNCTIONS

void LauncherRepositoryManager::downloadRepository(){
	// Set the authentification method at the start: prevents settings modification from causing issues with already running processes.
	setAuthMethod(LauncherSettings::useSSHAuth);
	
	v_logger.debug("Downloading repository!");
	
	setThreadComm_taskData(false, 0, "Initialising Libgit2...");


	if(git_libgit2_init() < 0){
		taskFailed("Failed to initialise Libgit2!", "LIBGIT FAILED TO INIT", true);
		return;
	}// END - If fail to load libgit2.
	
	v_logger.appendDebug("Libgit2 Initialised.");
	setThreadComm_taskData(false, 0, "Libgit2 Initialised; Setting clone options...");

	// Setup git options:
	git_clone_options temp_gitCloneOptions = GIT_CLONE_OPTIONS_INIT;
	temp_gitCloneOptions.fetch_opts.callbacks.credentials = getCredentials;
	temp_gitCloneOptions.fetch_opts.callbacks.transfer_progress = getProgress;
	temp_gitCloneOptions.fetch_opts.callbacks.payload = this;
	temp_gitCloneOptions.fetch_opts.download_tags = GIT_REMOTE_DOWNLOAD_TAGS_ALL;
	// Set branch to MASTER, so all tags can be downloaded, and ALL commits are seen.
	temp_gitCloneOptions.checkout_branch = "master"; 

	v_logger.appendDebug("Clone options set.");
	v_logger.appendDebug("Attempting creation of remote repo object using URL: " + v_repoURL );
	setThreadComm_taskData(false, 0, "Clone options set; creating remote...");

	// Create a remote object.
	if(git_remote_create_anonymous(&v_repoMan_remote, v_repoMan_repository, v_repoURL.c_str()) < 0 ){
		taskFailed("Failed to create remote.", "REMOTE CREATION FAILED.", true);
		return;
	} // END - create anonymous remote.
	

	setThreadComm_taskData(false, 0, "Remote set; attempting to open repository...");

	// Check to see if repository already exists. Cancel operation if it is.
	int temp_openRepoError = git_repository_open(&v_repoMan_repository, LauncherSettings::launcherRepositoryDirectory.c_str());
	if( !temp_openRepoError ){
		// REPOSITORY EXISTS AND IS OPEN: run UPDATE instead.
		v_logger.appendDebug("Repository already found!  Running update instead!" );
		// Shutdown libgit2, then run update function.
		v_logger.appendDebug("Shutting Down Libgit2.");
		git_libgit2_shutdown(); 
		updateRepository();
		// Return from download operation so things don't explode!
		return;
	} // END - If tempError !=0
	
	setThreadComm_taskData(false, 0, "Attempting clone...");

	// Attempt Clone:
	std::cout << "Attempting clone." << std::endl;
	if(git_clone( &v_repoMan_repository, v_repoURL.c_str(), LauncherSettings::launcherRepositoryDirectory.c_str(), &temp_gitCloneOptions ) < 0){
		taskFailed("Failed to clone repository", "Failed to clone launcher repository", true);
		return;
	} // END - git clone.
	
	setThreadComm_taskData(false, 1, "Repository downloaded successfully!");
	
	std::cout << "Releasing repository." << std::endl;
	git_repository_free(v_repoMan_repository);
	
	if(git_libgit2_shutdown() < 0){
		taskFailed("Libgit2 failed to shutdown.", "Libgit2 failed to shutdown!", true);
	} // END - Shutdown.
	
	
	notifyTaskFinished();
	
	v_taskInProgress = false;
	std::cout << "	>> >> DOWNLOAD COMPLETED" << std::endl;
	
} // END - Download Repository.



void LauncherRepositoryManager::updateRepository(){
	// Set the authentification method at the start: prevents settings modification from causing issues with already running processes.
	setAuthMethod(LauncherSettings::useSSHAuth);

	std::cout << "\n\nTASK START: Updating repository." << std::endl;
	
	
	if(git_libgit2_init() < 0){
		taskFailed("Failed to initialise Libgit2!", "LIBGIT FAILED TO INIT", true);
		return;
	}// END - If fail to load libgit2.
	
	std::cout << "Libgit 2 Initialised." << std::endl;
	setThreadComm_taskData(false, 0, "Libgit2 Initialised.");
	
	std::cout << "Checking if repository exists..." << std::endl;
	if( git_repository_open(&v_repoMan_repository, LauncherSettings::launcherRepositoryDirectory.c_str()) < 0) {
		
		// If the git error is -3 (object not found) or 6 (buffer not big enough?), run download instead.
		if(gitTaskFailed() == GIT_ENOTFOUND || 6){
			std::cout << "ERR:	Local repository not found! Downloading instead!" << std::endl;
			downloadRepository();
			return;
		} else{
			taskFailed("Failed to open repository! Consider clearing.", "REPO DIDN'T OPEN!	Return ERR not -3: consider clearing and re-downloading.", true);
			return;
		} // END - error is not -3.
	} // END - If open repo failed.
	
	std::cout << "Repo exists, creating remote." << std::endl;
	
	// Create remote repository
	if(git_remote_create_anonymous(&v_repoMan_remote, v_repoMan_repository, v_repoURL.c_str()) < 0 ){
		taskFailed("Failed to create remote.", "REMOTE CREATION FAILED.", true);
		return;
	} // END - create anonymous remote.
	
	std::cout << "Remote created. Setting fetch options." << std::endl;
	setThreadComm_taskData(false, 0, "Remote created, setting fetch options.");
//	v_signalUpdateProgress.emit("Created remote!, Setting options...", 0);

	git_fetch_options temp_fetchOptions = GIT_FETCH_OPTIONS_INIT;
	temp_fetchOptions.callbacks.credentials = getCredentials;
	temp_fetchOptions.callbacks.transfer_progress = getProgress;
	temp_fetchOptions.callbacks.payload = this;
	temp_fetchOptions.download_tags = GIT_REMOTE_DOWNLOAD_TAGS_ALL;
	
	std::cout << "Attempting Fetch operation" << std::endl;
	setThreadComm_taskData(false, 0, "Attempting to fetch changes...");
	
	
	if( git_remote_fetch(v_repoMan_remote, NULL, &temp_fetchOptions, NULL) < 0){
		taskFailed("Failed to fetch changes.", "FAILED TO FETCH CHANGES!", true);
		return;
	} // END - if Fetch failed.
	
	std::cout << "Fetch complete, creating head objects..." << std::endl;
	setThreadComm_taskData(false, 0, "Fetch complete! Looking up heads...");
	
	// Head lookup:
	const git_remote_head **temp_head = NULL;
	size_t temp_headCount = 0;
	
	std::cout << "Attempting remote lookup..." << std::endl;
	if(git_remote_ls( &temp_head, &temp_headCount, v_repoMan_remote ) < 0){
		taskFailed("Failed to look up heads!", "Failed to lookup remote heads!", true);
		return;
	} // END - if remote lookups fail.
	
	std::cout << "Remote lookup complete. Checking latest commit..." << std::endl;
	setThreadComm_taskData(false, 0, "Lookup complete, checking latest commit...");
	
	// Create git objects before looking up latest commit:
	git_oid temp_objectID = temp_head[0]->oid;
	git_annotated_commit *temp_mergeHead[1];
	
	if(git_annotated_commit_lookup(&temp_mergeHead[0], v_repoMan_repository, &temp_objectID) < 0 ){
		taskFailed("Failed to lookup current head.", "FAILED TO LOOKUP CURRENT REMOTE HEAD", true);
		return;
	} // END - If commit lookup fails.
	
	
	std::cout << "Remote head found.  Setting merge and Checkout options..." << std::endl;
	setThreadComm_taskData(false, 0, "Found remote head! Setting up options...");
	
	git_merge_options temp_mergeOptions = GIT_MERGE_OPTIONS_INIT;
	temp_mergeOptions.file_favor = GIT_MERGE_FILE_FAVOR_THEIRS;
	
	git_checkout_options temp_checkoutOptions = GIT_CHECKOUT_OPTIONS_INIT;
	temp_checkoutOptions.progress_cb = getCheckoutProgress;
	temp_checkoutOptions.progress_payload = this;
	temp_checkoutOptions.checkout_strategy = GIT_CHECKOUT_USE_THEIRS;
	
	std::cout << "Attempting merge..." << std::endl;
	setThreadComm_taskData(false, 0, "Attempting merge...");
	
	if(git_merge( v_repoMan_repository, (const git_annotated_commit **)(&temp_mergeHead), 1, &temp_mergeOptions, &temp_checkoutOptions ) < 0){
		std::cout << "Merge failed, attempting a reset instead." << std::endl;
		
		git_object *temp_gitObject;

		git_object_lookup(&temp_gitObject, v_repoMan_repository, &temp_objectID, GIT_OBJ_ANY);
		setThreadComm_taskData(false, 0, "Attempting reset...");
		
		std::cout << "Git object found, attempting reset." << std::endl;
		if( git_reset(v_repoMan_repository, temp_gitObject, GIT_RESET_HARD, &temp_checkoutOptions) < 0 ){
			
			switch (gitTaskFailed()){
				case 1:{
					git_repository_state_cleanup(v_repoMan_repository);
					setThreadComm_taskData(false, 0, "Repository is already up to date!");
					std::cout << "REPOSITORY ALREADY UP TO DATE!" << std::endl;
				}
				break;

				case 10:{
					setThreadComm_taskData(false, 0, "Index file locked.\nDeleting then attempting again...");
					Glib::ustring temp_lockFile;
					temp_lockFile = LauncherSettings::launcherRepositoryDirectory + "/.git/index.lock";
					std::cout << "Attempting removal of: " << temp_lockFile << std::endl;
					std::remove(temp_lockFile.c_str());
					// Re-run function.
					updateRepository();
				}
				break;

				default:{
					git_repository_state_cleanup(v_repoMan_repository);
					taskFailed("Failed to reset!", "FAILED TO RESET TO ORIGIN\nSorry, this feature is a little finicky right now.", true);
				}
				break;
			} // END - git task failed switch.
		} // END - git reset failed.
	} // END - merge failed.
	git_repository_state_cleanup(v_repoMan_repository);
	
	
	std::cout << "Shutting down Libgit2." << std::endl;
	if(git_libgit2_shutdown() < 0){
		taskFailed("Libgit2 failed to shutdown.", "Libgit2 failed to shutdown!", true);
	} // END - Shutdown.
	
	
	setThreadComm_taskData(false, 1, "Repository up to date!");
	notifyTaskFinished();
	
	v_taskInProgress = false;
	std::cout << "\n	>> >> UPDATE COMPLETE!\n" << std::endl;
	
} // END - Update repository.



void LauncherRepositoryManager::deleteRepository(){
	std::cout << "TASK STARTED: DELETE REPOSITORY\n\n" << std::endl;
	Glib::ustring temp_fullPath = LauncherUtils::getRealPath(LauncherSettings::launcherRepositoryDirectory);
	
	setThreadComm_taskData(false, 0, "Detecting files for deletion...");

	std::cout << "DETECTING FILES FOR DELETION..." << std::endl;
	// Reset files found to 0 before detecting files.
	v_launcherRepoFilesFound = 0;
	v_launcherRepoFilesRemoved = 0;
	// Detect files...
	v_launcherRepoFilesFound = launcherRepoFileCount( temp_fullPath );
	std::cout << "Found " << std::to_string(v_launcherRepoFilesFound) << "for deletion." << std::endl; 
	
	if(v_launcherRepoFilesFound){
		std::cout << "\n\nDELETING FILES..." << std::endl;
		launcherRepoDeleteFiles( temp_fullPath );
	} else{
		std::cout << "		>> NO FILES FOUND!" << std::endl;
	} // END if/else no files found.
	
	std::cout << "Removed: " <<  std::to_string(v_launcherRepoFilesRemoved) << " of " <<  std::to_string(v_launcherRepoFilesFound) << std::endl;

	// Task ended.
	setThreadComm_taskData(false, 1, "Files removed.");
	notifyTaskFinished();
	v_taskInProgress = false;
	std::cout << "\n		>> DELETION TASK COMPLETE!\n" << std::endl;
	
} // END - Delete repository.



void LauncherRepositoryManager::updateRepoLists(){
	std::cout << "TASK STARTED: UPDATE REPO LISTS\n\n" << std::endl;

	// Both models (commit & tag) are reset outside of this function.

	std::cout << "Initialising Libgit2" << std::endl;
	setThreadComm_taskData(false, 0, "Initialisng Libgit2...");

	
	if(git_libgit2_init() < 0){
		taskFailed("Failed to initialise Libgit2!", "LIBGIT FAILED TO INIT", true);
		notifyUpdateLists();
		return;
	}// END - If fail to load libgit2.
	
	std::cout << "Libgit 2 Initialised." << std::endl;
	setThreadComm_taskData(false, 0, "Libgit2 Initialised; opening repository...");

//	std::cout << "Opening launcher repository" << std::endl;forEachTag_populateLists;
	if( git_repository_open(&v_repoMan_repository, LauncherSettings::launcherRepositoryDirectory.c_str()) < 0){
		taskFailed("Failed to open repository!", "FAILED TO OPEN LAUNCHER REPOSITORY", true);
		notifyUpdateLists();
		return;
	} // END - If git repo opened failed.
	
	std::cout << "		> Repo opened. Continuing task." << std::endl;
	setThreadComm_taskData(false, 0, "Repository Opened! Creating revision walker...");
	
	git_revwalk *v_gitRevwalker = nullptr;
	if(git_revwalk_new(&v_gitRevwalker, v_repoMan_repository) < 0 ){
		taskFailed("Creating revwalker failed!", "FAILED TO CREATE REVWALKER", true);
		notifyUpdateLists();
		return;
	} // END - if creating revwalker failed.

	std::cout << "Revwalker Created. Pushing head to walker..." << std::endl;
	setThreadComm_taskData(false, 0, "Walker created, pushing head...");
	
	
	if( git_revwalk_push_head(v_gitRevwalker) < 0 ){
		taskFailed("Failed to push head to revwalker.", "FAILED TO PUSH HEAD TO REVWALKER!", true);
		notifyUpdateLists();
		return;
	} // END - if pushing head to revwalker failed.
	
	std::cout << "		>> Head pushed. Beginning loop..." << std::endl;
	setThreadComm_taskData(false, 0, "Head pushed! Beginning walk...");

// GIT OBJECTS FOR THE LIST CREATION...

// CREATE COMMIT LIST:
	git_oid v_gitObjectID;
	git_commit *temp_commit = nullptr;
	v_listAll = Gtk::ListStore::create(v_repoCommitColumns);

	while ( !git_revwalk_next(&v_gitObjectID, v_gitRevwalker) ){
		setThreadComm_taskData(false, 0, "Getting commits...");
		
		git_commit_lookup(&temp_commit, v_repoMan_repository, &v_gitObjectID);
		
		Gtk::TreeModel::Row temp_row = *(v_listAll->append());
		temp_row[v_repoCommitColumns.v_commitID] = git_oid_tostr_s(&v_gitObjectID);
		temp_row[v_repoCommitColumns.v_commitTitle] = git_commit_summary(temp_commit);

		git_commit_free(temp_commit);
	} // END - While no next item.
	
	

// CREATE TAG LIST:
	v_listTags = Gtk::ListStore::create(v_repoCommitColumns);
	
	if( git_tag_foreach( v_repoMan_repository, forEachTag_populateLists, this ) < 0){
		std::cerr << "Failed to output list of tags." << std::endl;
	} // END - for each tag.
	
	std::cout << "\nFreeing Revwalker & Repo." << std::endl;
	
	git_revwalk_free(v_gitRevwalker);
	git_repository_free( v_repoMan_repository );
	
	std::cout << "Libgit2 Shutdown: " << git_libgit2_shutdown() << std::endl;
	

	// Task ended.
	notifyTaskFinished();
	v_taskInProgress = false;
	notifyUpdateLists();
	std::cout << "\n		>> UPDATE LISTS TASK COMPLETE!\n" << std::endl;
} // END - Update repo list thread function




unsigned int LauncherRepositoryManager::launcherRepoFileCount(Glib::ustring p_path){
//	std::cout << "DEBUG:	Path given for file count: " << p_path << std::endl;
	unsigned int temp_totalFilesFound = 0;
	
	DIR *v_directory = opendir(p_path.c_str());
	struct dirent *v_nextFile;
	char v_filePath[PATH_MAX];
	
//	std::cout << "DEBUG:	" << v_filePath << " Exists: " << LauncherUtils::fileExists(v_filePath) << std::endl;
	setThreadComm_taskData(false, 0, "Detecting files for deletion...");
	
	while( (v_nextFile = readdir(v_directory)) != NULL ){
		
		if( strcmp(v_nextFile->d_name, ".") && strcmp(v_nextFile->d_name, "..") ){
			sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

			// If next file is a directory, call function again
			if(v_nextFile->d_type == DT_DIR){
				temp_totalFilesFound += launcherRepoFileCount( v_filePath );
			} // END - if next file is a DIRECTORY
		} // END - if next file name.

		setThreadComm_taskData(false, 0, "Detecting files for deletion...");
		
		++temp_totalFilesFound;
		sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);
//		v_signalUpdateProgress.emit("Finding files to remove...", 0);
		
		
	} // END - while directory isn't null
	closedir(v_directory);
	
	return temp_totalFilesFound;
	
} // END - Launcher repo file count.


unsigned int LauncherRepositoryManager::launcherRepoDeleteFiles(Glib::ustring p_path){
	// Double, held outside of while loop so not constructed on each loop.
	double temp_updateDouble;
	
	DIR *v_directory = opendir(p_path.c_str());
	struct dirent *v_nextFile;
	char v_filePath[PATH_MAX];

	while( (v_nextFile = readdir(v_directory)) != NULL ){
		
		if( strcmp(v_nextFile->d_name, ".") && strcmp(v_nextFile->d_name, "..") ){
			sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

			// If next file is a directory, call function again
			if(v_nextFile->d_type == DT_DIR){
				launcherRepoDeleteFiles( v_filePath );
			} // END - if next file is a DIRECTORY
		} // END - if next file name.

		
		++v_launcherRepoFilesRemoved;
		sprintf(v_filePath, "%s/%s", p_path.c_str(), v_nextFile->d_name);

		temp_updateDouble = double(v_launcherRepoFilesRemoved) / v_launcherRepoFilesFound;
		std::cout << "Progress: " << std::to_string(temp_updateDouble) << std::endl;
		setThreadComm_taskData(false, temp_updateDouble, "Removing files...");
		
		std::remove(v_filePath);
	} // END - while directory isn't null

	setThreadComm_taskData(false, temp_updateDouble, "Removing files...");

	closedir(v_directory);
	std::remove(p_path.c_str());
	
	return v_launcherRepoFilesRemoved;
} // END - Launcher repo delete files.


LauncherRepositoryManager::typeListStore LauncherRepositoryManager::getCommitTreeModel(bool p_returnCommits){
	if(p_returnCommits){
		// RETURN COMPLETE TREEMODEL :
		return v_listAll;
	}else{
		// RETURN TAG/RELEASE TREEMODEL:
		return v_listTags;
	} // END if/else return tags.
} // END - END Get commit 
