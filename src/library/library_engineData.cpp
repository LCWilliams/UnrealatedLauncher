#include "include/library/library_engine.h"
#include "external/simpleIni/SimpleIni.h"

using namespace UnrealatedLauncher;

bool EngineData::sanityCheck(){
	LauncherPrinter v_logger;
	v_logger.debug("Checking engine data sanity.");
	// Create a temp bool value to return at the end.  
	// If any value is insane it is set to false.
	bool temp_isSane = true;
	
	// Reset the message if it was set prior:
	notSaneMessage = "";
	
	/* IDENTIFIER CHECKS.
		* Identifier is created using std::time to get the time since Epoch.
		* The Identifier should only be 0 on initialisation, and any value less than 1000000000 is considered invalid.
		* While the chance of a bad identifier affecting anything is slim.  It's still there for sanity. */
	if(identifier == 0 || identifier < 1000000000){
		temp_isSane = false;
		notSaneMessage += "\n[BAD] Engine Identifier is an illegal value.";
		v_logger.warnErrMsg("Engine identifier is an illegal value!");
	} // END - Identifier check.
	
	// Engine Label: if the engine is installed/ready, and no label was given- it is updated to be the version + hotfix.
	if(label.empty() && installStatus == EngineInstallStatusEnum::ready){
		temp_isSane = false;
		notSaneMessage += "\n[BAD] Label should not be empty!";
		v_logger.warnErrMsg("Label should not be empty!");
	} // END - label check.
	
	/* INSTALLATION DIRECTORY.
		* Checks the installation directory for any invalid directories.
		*/ 
	if(installDir.empty()){
		notSaneMessage += "\n[BAD] Install directory is empty.";
		v_logger.warnErrMsg("Installation directory is empty!");
		temp_isSane = false;
	} // END - Empty install dir.
	
	
	/* INSTALL STATUS
		* The status should never be lower than -2 or higher than 4: subject to updates. */
	if( int(installStatus) < -2 || int(installStatus) > 4){
		notSaneMessage += "\n[BAD] Install status is illegal!";
		v_logger.warnErrMsg("Installation status is illegal!");
		temp_isSane = false;
	} // END - Install Status check.


	// Check if install status is ready but the engine isn't actually ready.
	if( installStatus == EngineInstallStatusEnum::ready){
		if ( !LauncherUtils::fileExists( installDir + "/Engine/Binaries/Linux/UE4Editor" ) ){
			notSaneMessage += "\n[BAD] UE4 Editor doesn't exist at directory!";
			v_logger.warnErrMsg("UE4 Editor does not exist at directory!");
			temp_isSane = false;
		} // END - File doesn't exist.
		
	} // END - Install status is ready
	
	if( configFile.empty() ){
		notSaneMessage += "\n[WARN] Config file value is empty. Assumed debugging entry.";
		v_logger.warnErrMsg("Config file value is empty.  Assumed debugging entry.");
	} // END - Config file directory is empty.
	
	// Set isSane bool to the value of sanityCheck.
	isSane = temp_isSane;
	return temp_isSane;
} // END - Sanity Check.



void EngineData::loadData(){
	LauncherPrinter v_logger;

	if(configFile.empty()){
		v_logger.errorMsg("Failed to load data for engine: " + std::to_string(identifier));
		return;
	} // END - If config file is empty, return.

	v_logger.debug("Loading engine configuration data.");

	CSimpleIniCaseA v_ini(true, true, true);
	SI_Error v_iniError;
	CSimpleIniCaseA::TNamesDepend v_launchOptKeys;
	
	v_iniError = v_ini.LoadFile(configFile.c_str());
	if(v_iniError < 0){
		v_logger.warnErrMsg("Unable to add engine.");
	} // END - load file failed.

	configFile = v_ini.GetValue("Internal", "configFile", "");
	identifier = v_ini.GetLongValue("Internal", "identifier");
	installStatus = static_cast<EngineInstallStatusEnum>( v_ini.GetLongValue("Internal", "installStatus") );
	// Get external data
	label = v_ini.GetValue("External", "label", "");
	installDir = v_ini.GetValue("External", "installDir", "");
	imageDir = v_ini.GetValue("External", "imageDir", "");
	description = v_ini.GetValue("External", "description", "");
	gitCommit = v_ini.GetValue("External", "gitCommit", "");

	v_ini.SetMultiKey(true);
	v_ini.GetAllValues("External", "launchOptions", v_launchOptKeys);
	// Sort items by order.
	v_launchOptKeys.sort(CSimpleIniCaseA::Entry::LoadOrder());
	// Clear vector to remove any residual data:
	launchOptions.clear();
	v_logger.debug("Launch Opt Keys size: " + std::to_string(v_launchOptKeys.size()));
	// Transfer launchOptKeys (pointers) to the launchOpt vector so they can be used after this function (or watch the world burn!).
	for(CSimpleIniCaseA::TNamesDepend::const_iterator keyIndex = v_launchOptKeys.begin();  keyIndex != v_launchOptKeys.end(); ++keyIndex){
		Glib::ustring temp_data = keyIndex->pItem;
		launchOptions.push_back(temp_data);
		v_logger.debug("Engine has another launchOpt");
	} // END - For loop
	v_logger.appendDebug("Launch Opt Size: " + std::to_string(launchOptions.size()));

	version = v_ini.GetLongValue("External", "version", -1);
	hotfix = v_ini.GetLongValue("External", "hotfix", -1);
	if(launchOptions.empty()){
		v_logger.warnErrMsg("Launch options empty: Default is missing from file!  [Save engine settings to regenerate]");
		// Launch options are empty: tampered ini file.  Add default with string.
		launchOptions.push_back( LauncherUtils::removeElementFromString(configFile, ".ini", "/") +  "default.ini" );
		v_logger.appendDebug("Default config will be saved at: " + launchOptions[0]);
	} // END - If empty
} // END - Load data.

void EngineData::saveData(){
	LauncherPrinter v_logger;
	v_logger.debug("Attempting to save engine configuration data.");
	// Simple ini objects:
	CSimpleIniCaseA v_configIni(true, true, true);
	SI_Error v_iniError;
	// Data string.
	std::string temp_dataString;
	// If path of file is empty, assume new engine/data and create the filepath using identifier.
	if(configFile.empty()){
		configFile = LauncherSettings::engineConfigs + std::to_string(identifier) + ".ini";
	} // END - Config file string is empty.
	
	v_configIni.SetMultiKey(true);

	// Write Interal data...
	v_configIni.SetLongValue("Internal", "identifier", identifier, ";Internal identifier for the engine.");
	v_configIni.SetValue("Internal", "configFile", configFile.c_str(), ";The location of this config file.");
	v_configIni.SetLongValue("Internal", "installStatus", int(installStatus), ";The status of the installation.");
	
	// Write external data...
	v_configIni.SetValue("External", "label", label.c_str(), ";The displayed engine label.");
	v_configIni.SetValue("External", "installDir", installDir.c_str(), ";The installation directory");
	v_configIni.SetValue("External", "imageDir", imageDir.c_str(), ";The path to the custom image.");
	v_configIni.SetValue("External", "description", description.c_str(), ";The description for the engine.");
	v_configIni.SetValue("External", "gitCommit", gitCommit.c_str(), ";The Git commit to use, if specified.");
	for(unsigned int launchOptIndex = 0; launchOptIndex < launchOptions.size(); ++launchOptIndex){
		if( !launchOptions[launchOptIndex].empty() && launchOptions[launchOptIndex].is_ascii() ){
			v_configIni.SetValue("External", "launchOptions", launchOptions[launchOptIndex].c_str(), "; Custom launch options.");
		} else if( launchOptIndex == 0) { // END - only save the value if it is NOT empty.
			// If value is 0, save as default.
			Glib::ustring temp_file = LauncherUtils::removeElementFromString(configFile, ".ini", "/") + "default.ini";
			v_configIni.SetValue("External", "launchOptions", temp_file.c_str(), "; The default launch options.");
		}else{ 
			v_configIni.DeleteValue("External", "launchOptions", launchOptions[launchOptIndex].c_str()); // Remove empty values left in document.
		} // END - else.
	} // END - launch options For loop
	v_configIni.SetLongValue("External", "version", version, ";The main version of the engine.");
	v_configIni.SetLongValue("External", "hotfix", hotfix, ";The hotfix version of the engine.");
	
	// Attempt save:
	v_iniError = v_configIni.Save(temp_dataString);
	if(v_iniError < 0){
		v_logger.errorMsg("Failed to save datastring! " + std::to_string(v_iniError));
		return; 
	} // END - If save datastring failed.
	
	v_iniError = v_configIni.SaveFile( configFile.c_str() );
	if(v_iniError < 0){
		v_logger.errorMsg("Failed to save file! " + std::to_string(v_iniError));
		return; 
	} // END - If save datastring failed.
	
	v_logger.appendDebug("Data saved successfully!");
} // END- Save data.


unsigned int EngineData::addNewLaunchOpt(){
	LauncherPrinter v_logger;
	Glib::ustring temp_fileName, temp_filePath;
	
	// Filepath is configFile path, minus the .ini extension.
	temp_filePath = LauncherUtils::removeElementFromString(configFile, ".ini", "") + "/";
	
	if(launchOptions.empty()){
		// Default entry!
		temp_fileName = "default.ini";
	}else{
		// In order to avoid issues of users not labeling their launch options, and for general easiness;
		// follow the naming style of the engines: using a time based name generation.
		temp_fileName = std::to_string( std::time(0) ) + ".ini";
	} // END - if/else launchOpts is empty
	
	// Append to strings together.
	temp_filePath.append(temp_fileName);
	// Add path to launchOpt array.
	launchOptions.push_back( temp_filePath );

	v_logger.debug("New launch option config file: " + temp_filePath);
	// Programatically starts at 0; thus size-1 = the last(this) entry

	return launchOptions.size() -1;
} // END - add new launch option