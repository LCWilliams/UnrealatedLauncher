#include "include/library/library_repoManager.h"

using namespace UnrealatedLauncher;

LauncherRepoManagerGUI::LauncherRepoManagerGUI(LauncherRepositoryManager* p_repoManagerData):
	btn_closeManager("Close Manager"),

	btn_gitHTTPLogin("Login", false),
	
	btn_downloadRepo("_Download Repository", true),
	btn_updateRepo("_Update Repository", true),
	btn_clearRepo("_Clear Repository", true),
	btn_confirmDelete("Confirm clear...", false),
	
	v_controlsFrame("Controls"),
	v_statusFrame("Info"),
	v_connectFrame("Connect"),
	
	btn_closeStatus("Okay")
	{
	// Set the reference, otherwise things explode!
	ref_repoManager = p_repoManagerData;

	// Set bins options:
	set_hexpand(true);
	set_vexpand(true);
	set_halign(Gtk::ALIGN_CENTER);
	set_valign(Gtk::ALIGN_CENTER);
	
	// Add overlay to the bin, then main grid to overlay.
	add(v_overlay);
	v_overlay.add(v_mainGrid);
	
	// Set main grid options:
	v_mainGrid.set_row_spacing(7);
	v_mainGrid.set_column_spacing(10);
	
	// Add grids to frames/revealers, and attach to main grid.
	v_overlay.add_overlay(v_connectGrid);
	v_connectGrid.set_name("overlay_1");
	v_connectGrid.set_halign(Gtk::ALIGN_CENTER);
	v_connectGrid.set_valign(Gtk::ALIGN_CENTER);
	
	v_controlsFrame.add(v_controlsGrid);
	v_mainGrid.attach(v_controlsFrame, 1, 0, 1, 1);
	v_statusRevealer.add(v_statusFrame);
	v_statusFrame.add(v_statusGrid);
	v_mainGrid.attach(v_statusRevealer, 2, 0, 1, 1);
	
	// Attach close button to grid:
	v_mainGrid.attach(btn_closeManager, 0, 1, 3, 1);
		btn_closeManager.set_margin_top(50);
		btn_closeManager.set_hexpand(true);
		btn_closeManager.set_tooltip_text("Closing the manager does NOT stop any current task!\nThey'll continue in the background.");
	
	// Connect Grid:
	v_connectGrid.set_column_spacing(10);
	v_connectGrid.set_border_width(10);
	v_connectGrid.attach(v_loginRevealer, 1, 0, 1, 3);
	v_loginRevealer.add(v_connectHTTPLoginGrid);
		v_connectHTTPLoginGrid.set_column_spacing(7);
		v_connectHTTPLoginGrid.set_row_spacing(5);
	// Create labels for entries:
	Gtk::Label* v_gitEmailLabel = Gtk::manage(new Gtk::Label("GitHub Email: "));
	v_connectHTTPLoginGrid.attach(*v_gitEmailLabel, 0, 0, 1, 1);
	Gtk::Label* v_gitPasswordLabel = Gtk::manage(new Gtk::Label("GitHub Password: "));
	v_connectHTTPLoginGrid.attach(*v_gitPasswordLabel, 0, 1, 1, 1);
	
	v_connectHTTPLoginGrid.attach(btn_gitAccountEmail, 1, 0, 1, 1);
		btn_gitAccountEmail.set_placeholder_text("yourGit@email.com");
	v_connectHTTPLoginGrid.attach(btn_gitAccountPass, 1, 1, 1, 1);
		btn_gitAccountPass.set_placeholder_text("yourGitPassword");
		btn_gitAccountPass.set_visibility(false);
	v_connectHTTPLoginGrid.attach(btn_gitHTTPLogin, 0, 3, 2, 1);
	
	// Controls grid:
	v_controlsGrid.set_column_spacing(10);
	v_controlsGrid.set_border_width(10);
	v_controlsGrid.attach(btn_downloadRepo, 0, 1, 1, 1);
		btn_downloadRepo.set_tooltip_text("Download the launchers repository for UE4");
	v_controlsGrid.attach(btn_updateRepo, 0, 2, 1, 1);
		btn_updateRepo.set_tooltip_text("Update the launchers repository. This does not update your installed engines!");
	v_controlsGrid.attach(btn_clearRepo, 0, 3, 1, 1);
		btn_clearRepo.set_tooltip_text("Clear the launchers repository from disk. Do this if you're experiencing issues with the repository.");
	v_controlsGrid.attach(btn_confirmDelete, 0, 4, 1, 1);
		btn_confirmDelete.set_tooltip_text("WARNING: This button will instigate the deletion, only press if you're certain!");
		btn_confirmDelete.set_no_show_all(true);
		btn_confirmDelete.hide();
	
	// Revealer Options:
	v_statusRevealer.set_transition_duration(350);
	v_statusRevealer.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
	v_loginRevealer.set_transition_duration(350);
	v_loginRevealer.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
	
	// Status Grid:
	v_statusGrid.set_valign(Gtk::ALIGN_CENTER);
	v_statusGrid.set_row_spacing(10);
	v_statusGrid.set_border_width(15);
	v_statusGrid.attach(v_statusSpinner, 0, 0, 1, 1);
	v_statusGrid.attach(txt_activeTask, 1, 0, 1, 1);
	v_statusGrid.attach(v_statusProgressBar, 0, 1, 2, 1);
	v_statusProgressBar.set_show_text(true);
	v_statusGrid.attach(btn_closeStatus, 0, 2, 2, 1);
		btn_closeStatus.set_no_show_all(true);
	
	// Button signals:
	btn_gitHTTPLogin.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_gitHTTPLogin_clicked));
	
	btn_downloadRepo.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_downloadRepo_clicked));
	btn_updateRepo.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_updateRepo_clicked));
	btn_clearRepo.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_clearRepo_clicked));
	btn_confirmDelete.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_confirmDelete_clicked));

	btn_closeStatus.signal_clicked().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_closeStatus_clicked));
	
	// Set the "activate" signal of entry to same as btn_gitHTTPLogin.
	btn_gitAccountPass.signal_activate().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::btn_gitHTTPLogin_clicked));


	// Create signal for repo data signals.
	ref_repoManager->dispatcherTaskEnded.connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::taskFinished));
	ref_repoManager->dispatcherPromptLogin.connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::promptLogin));
	ref_repoManager->dispatcherUpdateProgress.connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::notifyUpdateProgress));

	// Update controls to current state of launcher repo:
	updateControls();
} // END - Launcher Repo manager GUI.

LauncherRepoManagerGUI::~LauncherRepoManagerGUI(){}


LauncherRepoManagerGUI::typeSignalUserLoggedIn LauncherRepoManagerGUI::signal_userLoggedIn(){
	return v_signalUserLoggedIn;
} // END - signal user logged in.

bool LauncherRepoManagerGUI::idleUpdateProgress(){
	notifyUpdateProgress();
	return ref_repoManager->v_taskInProgress;
}

void LauncherRepoManagerGUI::notifyUpdateProgress(){
	// Create temporary variables that will be passed to repoManager to get task data in a threadsafe way.
	Glib::ustring temp_message;
	double temp_percent;
	ref_repoManager->getThreadComm_taskData(temp_message, temp_percent);
	updateProgress(temp_message, temp_percent);
} // END - Notify update progress.


void LauncherRepoManagerGUI::updateProgress(Glib::ustring p_text, double p_progress){
	// Only update progress if the bar is visible!
	if(v_statusProgressBar.is_visible() ){
		// If progress is 0, but update progress is called, pulse the progress bar.
		if(p_progress == 0){
			v_statusProgressBar.pulse();
		} // END - if progress is 0, pulse bar.
		else{
			v_statusProgressBar.set_fraction(p_progress);
		} // END - If progress is above 0.
		v_statusProgressBar.set_text(p_text);
	} // END - if progress bar is visible.
} // END - update progress.


void LauncherRepoManagerGUI::taskFinished(){
	v_controlsFrame.set_sensitive(true);
	v_connectFrame.set_sensitive(true);
	txt_activeTask.set_text("");
	v_statusSpinner.stop();
	updateControls();
	btn_closeStatus.show();
	// Emit busy signal (false), so any connected items are updated.
	v_signalManagerBusy.emit(false);
} // END - task finished.


void LauncherRepoManagerGUI::updateControls(){
	// Check if repo exists.
	if(ref_repoManager->getRepoExists()){
		// Repo exists, disable download & enable update.
		btn_downloadRepo.set_sensitive(false);
		btn_updateRepo.set_sensitive(true);
	} else{
		// repo doesn't exist, enable download & disable update.
		btn_downloadRepo.set_sensitive(true);
		btn_updateRepo.set_sensitive(false);
	} // END - Check repo exists.
} // END - Update controls.



void LauncherRepoManagerGUI::promptLogin(){
	v_loginRevealer.set_reveal_child(true);
	// Set opacity of controls grid to low value: stops obfuscating view of overlaid login widgets, regardless of themes used. 
	v_controlsFrame.set_opacity(0.2);
	v_statusFrame.set_opacity(0.2);
	// Set bool to state the user has been prompted!
	ref_repoManager->v_promptUserInputCalled = true;
	
	// If the email field is empty, make it grab focus, else assume incorrect password.
	if(btn_gitAccountEmail.get_text().empty()){
		btn_gitAccountEmail.grab_focus();
	}else{
		btn_gitAccountPass.grab_focus();
	}// END -if/else email entry is empty.
} // END - prompt login.


void LauncherRepoManagerGUI::btn_gitHTTPLogin_clicked(){
	// Check if entries are empty:
	if( !btn_gitAccountEmail.get_text().empty() || !btn_gitAccountPass.get_text().empty() ){
		
		// Hide login revealer.
		v_loginRevealer.set_reveal_child(false);
		
		// Set controls grid opacity back to 1.
		v_controlsFrame.set_opacity(1);
		v_statusFrame.set_opacity(1);
		
		// Set GIT URL/Auth method to Https
		ref_repoManager->setAuthMethod(false);
		
		ref_repoManager->setHTTPLoginCredentials( btn_gitAccountEmail.get_text(), btn_gitAccountPass.get_text() );
//		ref_repoManager->btn_userLoggedIn();
	} // END - If entries are NOT empty.
	
} // END - Button https login clicked.


void LauncherRepoManagerGUI::btn_downloadRepo_clicked(){
	// Emit busy signal (true), so any connected items are updated.
	v_signalManagerBusy.emit(true);
	
	Glib::signal_timeout().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::idleUpdateProgress), 250);

	v_statusRevealer.set_reveal_child(true);
	txt_activeTask.set_text("Downloading the UE4 repository.");
	ref_repoManager->instigateDownloadRepository();

	
	// Disable the controls:
	v_controlsFrame.set_sensitive(false);
	v_connectFrame.set_sensitive(false);
	
	// Hide the button incase user didn't click after last operation.
	btn_closeStatus.hide();
	
	// Start spinner animation:
	v_statusSpinner.start();
	
} // END - Button download repo clicked.



void LauncherRepoManagerGUI::btn_updateRepo_clicked(){
	// Emit busy signal (true), so any connected items are updated.
	v_signalManagerBusy.emit(true);

	Glib::signal_timeout().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::idleUpdateProgress), 250);

	v_statusRevealer.set_reveal_child(true);
	txt_activeTask.set_text("Updating the UE4 repository.");
	ref_repoManager->instigateUpdateRepository();
	// Disable the controls:
	v_controlsFrame.set_sensitive(false);
	v_connectFrame.set_sensitive(false);
	
	// Hide the button incase user didn't click after last operation.
	btn_closeStatus.hide();
	
	// Start spinner animation:
	v_statusSpinner.start();
	
} // END - Button update repo clocked.



void LauncherRepoManagerGUI::btn_clearRepo_clicked(){
	btn_confirmDelete.set_visible( btn_clearRepo.get_active() );
} // END - Clear repo clicked.



void LauncherRepoManagerGUI::btn_confirmDelete_clicked(){
	// Emit busy signal (true), so any connected items are updated.
	v_signalManagerBusy.emit(true);
	
	Glib::signal_timeout().connect(sigc::mem_fun(*this, &LauncherRepoManagerGUI::idleUpdateProgress), 250);
	
	// Set active to false, hides confirm button.
	btn_clearRepo.set_active(false);
	
	v_statusRevealer.set_reveal_child(true);
	txt_activeTask.set_text("Clearing the launcher repository!");
	ref_repoManager->instigateRepoDeletion();
	// Disable the controls:
	v_controlsFrame.set_sensitive(false);
	v_connectFrame.set_sensitive(false);
	
	// Hide the button incase user didn't click after last operation.
	btn_closeStatus.hide();
	
	// Start spinner animation:
	v_statusSpinner.start();
	
} // END - Confirm delete clicked.


void LauncherRepoManagerGUI::btn_closeStatus_clicked(){
	v_statusProgressBar.set_text("");
	v_statusProgressBar.set_fraction(0);
	v_statusRevealer.set_reveal_child(false);
	btn_closeStatus.hide();
} // END - Close status clicked.
