#include "include/library/library_engine.h"

using namespace UnrealatedLauncher;

EngineLaunchOptBase::EngineLaunchOptBase(){
	set_column_homogeneous(false);
	set_column_spacing(5);
	set_hexpand(true);
	set_halign(Gtk::ALIGN_FILL);
//	set_size_request(500, -1);
} // END - Constructor.

EngineLaunchOptBase::~EngineLaunchOptBase(){} // Destructor.

void EngineLaunchOptBase::setIniReference(CSimpleIniCaseA* p_ref){
	ref_ini = p_ref;
}

void EngineLaunchOptBase::writeStrSetting(Glib::ustring p_setting){
	if(ref_ini){
		ref_ini->SetValue( v_settingHeader.c_str(), v_settingName.c_str(), p_setting.c_str() );
		v_stringSetting = p_setting;
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini String value not set: iniObject reference empty!");
	} // END - if/else ref_ini is set.
} // END - write STRING setting.

void EngineLaunchOptBase::writeIntSetting( int p_setting ){
	if( ref_ini ){
		ref_ini->SetLongValue( v_settingHeader.c_str(), v_settingName.c_str(), p_setting );
		v_intSetting = p_setting;
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini Int value not set: iniObject reference empty!");
	} // END - if/else ref_ini is set.
} // END - write INTEGER setting.

void EngineLaunchOptBase::writeBoolSetting(bool p_setting){
	if(ref_ini){
		ref_ini->SetBoolValue(v_settingHeader.c_str(), v_settingName.c_str(), p_setting);
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini Bool value not set: iniObject reference empty!");
	} // END - if/else ref_ini is set.
} // END - Write Bool setting.

Glib::ustring EngineLaunchOptBase::readStrSetting(){
	if(ref_ini){
		v_stringSetting = ref_ini->GetValue( v_settingHeader.c_str(), v_settingName.c_str(), "" );
		return v_stringSetting;
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini String value not read: iniObject reference empty!");
		return "";
	} // END - if/else ref_ini is set.
} // END - Read string setting.

int EngineLaunchOptBase::readIntSetting(){
	if(ref_ini){
		v_intSetting = ref_ini->GetLongValue( v_settingHeader.c_str(), v_settingName.c_str(), 0 );
		return v_intSetting;
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini Int value not read: iniObject reference empty!");
		return 0;
	} // END - if/else ref_ini is set.
} // END - Read INTEGER setting.

bool EngineLaunchOptBase::readBoolSetting(){
	if(ref_ini){
		return ref_ini->GetBoolValue(v_settingHeader.c_str(), v_settingName.c_str());
	}else{
		LauncherPrinter v_logger;
		v_logger.errorMsg("Ini Bool value not read: iniObject reference empty!");
		return false;
	} // END - if/else ref_ini is set.
} // END - Read bool setting.

void EngineLaunchOptBase::readSettings(){} // END - read settings.

void EngineLaunchOptBase::writeSettings(){}

Glib::ustring EngineLaunchOptBase::getCommand(){ return "";}

void EngineLaunchOptBase::buttonUpdated(){
	if(!v_bDontUpdate){
		v_signalUpdateCommand.emit();
	} // END - if don'tUpdate is false
} // END - button updated


EngineLaunchOptToggle::EngineLaunchOptToggle( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_boolCommand, Glib::ustring p_displayedLabel ):
btn_check(p_displayedLabel)
/*EngineLaunchOptToggle*/{
	v_settingHeader = p_header;
	v_settingName = p_entryName;
	
	attach( btn_check, 1, 0, 1, 1 );
	v_stringSetting = p_boolCommand;
	set_halign(Gtk::ALIGN_FILL);
	
	btn_check.signal_clicked().connect(sigc::mem_fun(*this, &EngineLaunchOptToggle::buttonUpdated));
} // END - Launch Opt Toggle 

EngineLaunchOptToggle::~EngineLaunchOptToggle(){}

void EngineLaunchOptToggle::readSettings(){
	btn_check.set_active( readBoolSetting() );
} // END - read TOGGLE settings.

void EngineLaunchOptToggle::writeSettings(){
	writeBoolSetting( btn_check.get_active() );
} // END- write TOGGLE settings

Glib::ustring EngineLaunchOptToggle::getCommand(){
	if(!btn_check.get_active()){
		return "";
	}else{
		// Add space at end of command.
		return v_stringSetting + " ";
	} // END - If button is active.
} // END - get toggle command.


EngineLaunchOptCombobox::EngineLaunchOptCombobox( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_displayedLabel ):
txt_displayedLabel(p_displayedLabel) {
	attach( txt_displayedLabel, 0, 0, 1, 1 );
	attach( btn_option, 1, 0, 1, 1);
	
	v_settingHeader = p_header;
	v_settingName = p_entryName;
	
	txt_displayedLabel.set_halign(Gtk::ALIGN_END);
	LauncherUtils::setLongLabelTextParameters(&txt_displayedLabel, Pango::ELLIPSIZE_NONE, true, 2, Gtk::ALIGN_FILL, 0, 0, 30);
		txt_displayedLabel.set_size_request(50,-1);
	

	// Add default option.
	btn_option.append( "", "Default" );
	btn_option.set_hexpand(true);
	btn_option.set_halign(Gtk::ALIGN_FILL);
	btn_option.set_active(0); // Sets default as active.
	btn_option.signal_changed().connect(sigc::mem_fun(*this, &EngineLaunchOptCombobox::btn_option_changed));
} // END - Launch Combobox constructor.

EngineLaunchOptCombobox::~EngineLaunchOptCombobox(){}

void EngineLaunchOptCombobox::btn_option_changed(){
	v_stringSetting = btn_option.get_active_id();
	buttonUpdated();
} // END - option updated.

void EngineLaunchOptCombobox::addNewOption( Glib::ustring p_displayedLabel,Glib::ustring p_command ){
	btn_option.append( p_command, p_displayedLabel );
} // END - Alt add new option

void EngineLaunchOptCombobox::readSettings(){
	btn_option.set_active( readIntSetting() );
} // END - Read COMBOBOX settings

void EngineLaunchOptCombobox::writeSettings(){
	writeIntSetting( btn_option.get_active_row_number() );
} // END - Write COMBOBOX settings.

Glib::ustring EngineLaunchOptCombobox::getCommand(){
	if(btn_option.get_active_row_number() != 0){
		// The command is stored in a row's ID.
		// Space is added at the end of the command.
		return btn_option.get_active_id() + " ";
	}else{
		return "";
	}
} // END - Get combobox command.


EngineLaunchOptSpin::EngineLaunchOptSpin( Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_command, Glib::ustring p_displayedLabel ):
txt_displayedLabel(p_displayedLabel){
	
	v_settingHeader = p_header;
	v_settingName = p_entryName;
	
	
	attach( txt_displayedLabel, 0, 0, 1, 1 );
//		txt_displayedLabel.set_halign(Gtk::ALIGN_END);
		LauncherUtils::setLongLabelTextParameters(&txt_displayedLabel, Pango::ELLIPSIZE_NONE, true, 2, Gtk::ALIGN_END, 0, 0, 30);
		txt_displayedLabel.set_size_request(200,-1);
	attach(btn_spinEntry, 1, 0, 1, 1 );
		btn_spinEntry.set_halign(Gtk::ALIGN_START);
		btn_spinEntry.set_range(0,9999);
		btn_spinEntry.set_increments(1,5);
		btn_spinEntry.signal_value_changed().connect(sigc::mem_fun(*this, &EngineLaunchOptSpin::buttonUpdated));
		
	// Complete command will be string plus int string.
	v_stringSetting = p_command;
} // END - Launch Opt SPIN constructor

EngineLaunchOptSpin::~EngineLaunchOptSpin(){}

void EngineLaunchOptSpin::readSettings(){
	btn_spinEntry.set_value( readIntSetting() );
} // END - read SPIN settings.

void EngineLaunchOptSpin::writeSettings(){
	writeIntSetting( btn_spinEntry.get_value_as_int() );
} // END - write SPIN settings.

Glib::ustring EngineLaunchOptSpin::getCommand(){
	if(btn_spinEntry.get_value_as_int() != 0){
		return v_stringSetting + std::to_string(btn_spinEntry.get_value_as_int()) + " ";
	}else{
		return "";
	} // END- if/else
} // END - Get spin command.


EngineLaunchOptEntry::EngineLaunchOptEntry(Glib::ustring p_header, Glib::ustring p_entryName, Glib::ustring p_command, Glib::ustring p_displayedLabel):
txt_displayedLabel(p_displayedLabel){
	v_settingHeader = p_header;
	v_settingName = p_entryName;

	attach(txt_displayedLabel, 0, 0, 1, 1);
		LauncherUtils::setLongLabelTextParameters(&txt_displayedLabel, Pango::ELLIPSIZE_NONE, true, 2, Gtk::ALIGN_END, 0, 0, 30);
		txt_displayedLabel.set_size_request(50,-1);
	attach(btn_entry, 1, 0, 1, 1);
		btn_entry.set_halign(Gtk::ALIGN_FILL);
		btn_entry.set_hexpand(true);
		btn_entry.signal_changed().connect(sigc::mem_fun(*this, &EngineLaunchOptEntry::buttonUpdated));
	
	v_preCommand = p_command;
} // END - ENTRY launch opt constructor.

EngineLaunchOptEntry::~EngineLaunchOptEntry(){}

void EngineLaunchOptEntry::readSettings(){
	btn_entry.set_text( readStrSetting() );
} // - read ENTRY setting.

void EngineLaunchOptEntry::writeSettings(){
	writeStrSetting( btn_entry.get_text() );
} // END - Write ENTRY setting.


Glib::ustring EngineLaunchOptEntry::getCommand(){
	if( !btn_entry.get_text().empty() ){
		return v_preCommand + btn_entry.get_text() + " ";
	}else{
		return "";
	}
} // END - Entry getCommand.
