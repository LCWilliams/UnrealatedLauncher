#include "include/launcher_miniGUI.h"

using namespace UnrealatedLauncher;

LaunchEngineBtn::LaunchEngineBtn(Glib::ustring p_configFile){
	// Set initial variables:
	v_configFile = p_configFile;
	// Signal connection:
	v_signalButtonPressed.connect(sigc::mem_fun(*this, &LaunchEngineBtn::btn_selfPressed));
} // END - Launch Engine btn constructor.

LaunchEngineBtn::~LaunchEngineBtn(){} // Launch engine btn destructor.

void LaunchEngineBtn::btn_selfPressed(){
	v_signalLaunchBtnPressed.emit(v_launchString);
} // END - Button (self) pressed.

void LaunchEngineBtn::setLaunchString(Glib::ustring p_string){
	v_launchString = p_string;
} // END - Update launch string.