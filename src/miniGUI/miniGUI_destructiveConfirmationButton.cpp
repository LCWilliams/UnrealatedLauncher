#include "include/launcher_miniGUI.h"


using namespace UnrealatedLauncher;

DestructiveConfirmationButton::DestructiveConfirmationButton(Glib::ustring p_buttonLabel, Glib::ustring p_warningText):
btn_firstStage(p_buttonLabel),
btn_confirm("CONFIRM", false),
btn_cancel("_CANCEL", true)
//txt_warningLabel(p_warningText) // set with markdown instead of standard text.
/*DestructiveConfirmationButton*/{
// Add main grid to bin.
	add(v_mainGrid);
//	set_halign(Gtk::ALIGN_START);
	set_hexpand(true);
	
	// Add button & revealer to main grid.
	v_mainGrid.attach(btn_firstStage, 0, 0, 1, 1);
		btn_firstStage.set_hexpand(true);
	v_mainGrid.set_halign(Gtk::ALIGN_FILL);
	v_mainGrid.set_column_homogeneous(false);
	v_mainGrid.set_row_homogeneous(false);
	v_mainGrid.attach(v_revealer, 0, 1, 1, 1);
	v_mainGrid.set_hexpand(true);
	
	// Add subgrid to revealer.
	v_revealer.add(v_revealerGrid);
//		v_revealer.set_hexpand(true);
	v_revealer.set_halign(Gtk::ALIGN_CENTER);
	v_revealerGrid.attach(v_timerBar, 0, 1, 2, 1);
	v_revealerGrid.attach(txt_warningLabel, 0, 0, 2, 1);
		v_revealerGrid.set_column_homogeneous(false);
		v_revealerGrid.set_hexpand(true);
		txt_warningLabel.set_markup( txt_warningLabelPretext + "\n" + p_warningText );
		LauncherUtils::setLongLabelTextParameters(&txt_warningLabel, Pango::ELLIPSIZE_NONE, true, 3, Gtk::ALIGN_START, 10, 10, 50);
	v_revealerGrid.attach(btn_cancel, 0, 2, 1, 1);
		btn_cancel.set_halign(Gtk::ALIGN_FILL); // Makes cancel button take up the most available space and thus be the most promonent button.
		btn_cancel.set_hexpand(true);
		btn_cancel.set_can_focus(true); // Ensures the button can be focused with code.
	v_revealerGrid.attach(btn_confirm, 1, 2, 1, 1);
		btn_confirm.set_halign(Gtk::ALIGN_END);
	// Set revealer options.
	v_revealer.set_reveal_child(false);
	v_revealer.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_DOWN);
	v_revealer.set_transition_duration(250);
	
	
	btn_firstStage.signal_clicked().connect(sigc::mem_fun(*this, &DestructiveConfirmationButton::btn_firstStage_clicked));
	btn_confirm.signal_clicked().connect(sigc::mem_fun(*this, &DestructiveConfirmationButton::btn_confirm_clicked));
	btn_cancel.signal_clicked().connect(sigc::mem_fun(*this, &DestructiveConfirmationButton::btn_cancel_clicked));
	
} // END - DestructiveConfirmationButton constructor.

DestructiveConfirmationButton::~DestructiveConfirmationButton(){} // Destructor.


void DestructiveConfirmationButton::btn_firstStage_clicked(){
	// Set bar to full.
	v_timerBar.set_fraction(1);
	// Display revealer contents.
	v_revealer.set_reveal_child(true);
	// Set button sensitivity to false.
	btn_firstStage.set_sensitive(false);
	// Confirm button should be insensitive until after a time period.
	btn_confirm.set_sensitive(false);
	
	// Sets the focus of the cancel button: 
	// In the event a user accidentally mis-clicks Enter repeatedly, the operation is cancelled instead of confirmed.
	// While the timeout prevents this anyway, it's precautionary.
	btn_cancel.grab_focus();
	
	// Start idle timeout.
	Glib::signal_timeout().connect(sigc::mem_fun(*this, &DestructiveConfirmationButton::idle_timerUpdate), 25, 0);
	
} // END - first stage button clicked.

void DestructiveConfirmationButton::btn_confirm_clicked(){
	// Hide revealer.
	v_revealer.set_reveal_child(false);
	
	// Set bar percent to 0 in order to end timeout signal.
	v_timerBar.set_fraction(0);
	
	// Emit signal.
	v_signalConfirm.emit();
} // END - Confirm button clicked.

void DestructiveConfirmationButton::btn_cancel_clicked(){
	v_timerBar.set_fraction(0); // Sets timerbar percent to 0 in order to end idle timerUpdate.
	v_revealer.set_reveal_child(false);
	btn_firstStage.set_sensitive(true);
}// END - Cancel button clicked.

bool DestructiveConfirmationButton::idle_timerUpdate(){
	double temp_currentPercent = v_timerBar.get_fraction();
	
	// Determine if percent is still counting down.
	if(temp_currentPercent > 0){
		// Set CONFIRM sensitivity to true only after the timer has reached halfway.
		if(temp_currentPercent < 0.75){
			btn_confirm.set_sensitive(true);
		} // IF Current percent is below percent.
		
		v_timerBar.set_fraction( temp_currentPercent - 0.001 );
		return true;
	} // END - If currentPercent is greater than 0.
	
	// User has not confirmed, reset.
	v_revealer.set_reveal_child(false);
	btn_firstStage.set_sensitive(true);
	
	return false;
} // END - Timer update function.

