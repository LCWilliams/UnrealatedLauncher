#include "include/launcher_miniGUI.h"

using namespace UnrealatedLauncher;

DialogueControls::DialogueControls(bool p_hasDefaults):
btn_cancel("_Cancel", true),
btn_defaults("_Defaults", true),
btn_confirm("C_onfirm", true){
	pack_start(btn_cancel, true, true, 2);
	if(p_hasDefaults){
		pack_start(btn_defaults, true, true, 2);
	} // END - If has defaults is enabled.
	pack_start(btn_confirm, true, true, 2);
	
	btn_cancel.signal_clicked().connect(sigc::mem_fun(*this, &DialogueControls::btn_cancel_clicked));
	btn_defaults.signal_clicked().connect(sigc::mem_fun(*this, &DialogueControls::btn_defaults_clicked));
	btn_confirm.signal_clicked().connect(sigc::mem_fun(*this, &DialogueControls::btn_confirm_clicked));
	
} // Dialogue Controls constructor.

DialogueControls::~DialogueControls(){} // Destructor.

void DialogueControls::btn_cancel_clicked(){
	v_signalCancel.emit();
}// END - Cancel button clicked.

void DialogueControls::btn_defaults_clicked(){
	v_signalDefaults.emit();
} // END - Default button clickd.

void DialogueControls::btn_confirm_clicked(){
	v_signalConfirm.emit();
}// END - Confirm button clicked.

void DialogueControls::setConfirmBtnSensitive(bool p_sensitive = true){
	btn_confirm.set_sensitive(p_sensitive);
} // END - Set Confirm button sensitive.