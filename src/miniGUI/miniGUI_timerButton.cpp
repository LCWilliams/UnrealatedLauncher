#include "include/launcher_miniGUI.h"

using namespace UnrealatedLauncher;

TimerButton::TimerButton(){
	setup();
} // Default constructor.

TimerButton::TimerButton(Glib::ustring p_label){
	btn_main.set_label(p_label);
	setup();
} // END - Constructor with label.

TimerButton::~TimerButton(){} // Destructor


void TimerButton::setup(){
	set_hexpand(true);
	set_vexpand(true);
	// Add main overlay to the bin.
	add(v_mainOverlay);
	// Set the main button as the overlay's base.
	v_mainOverlay.add(btn_main);
	v_mainOverlay.set_hexpand(true);
	v_mainOverlay.set_vexpand(true);
	// Add cancel &  timer to overlay.
	v_mainOverlay.add_overlay(btn_cancel);
		btn_cancel.set_halign(Gtk::ALIGN_END);
		btn_cancel.set_vexpand(true);
		btn_cancel.set_valign(Gtk::ALIGN_FILL);
		btn_cancel.set_label("Cancel");
		btn_cancel.set_can_focus(true);
		btn_cancel.set_no_show_all(true); // Stops the button from being made visible from calls to show_all!
	
	v_mainOverlay.add_overlay(v_timerBar);
		v_timerBar.set_halign(Gtk::ALIGN_FILL);
		v_timerBar.set_valign(Gtk::ALIGN_END);
		v_timerBar.set_no_show_all(true);
		
	setOverlayVisibility(false);
		
	// Signals.
	btn_main.signal_clicked().connect(sigc::mem_fun(*this, &TimerButton::btn_main_clicked));
	btn_cancel.signal_clicked().connect(sigc::mem_fun(*this, &TimerButton::btn_cancel_clicked));
} // END - Setup.

void TimerButton::btn_main_clicked(){
	if(LauncherSettings::timedButtonsEnabled){
		setOverlayVisibility(true);
		v_timerBar.set_fraction(1);
		v_cancelled = false;
		Glib::signal_timeout().connect(sigc::mem_fun(*this, &TimerButton::idleCountdown), 25);
		btn_cancel.grab_focus();
	} else{ // END - If timed buttons are ENABLED.
		v_signalButtonPressed.emit();
	} // END - Else: timed buttons are DISABLED
} // END - main button clicked.


void TimerButton::btn_cancel_clicked(){
	setOverlayVisibility(false);
	
	v_cancelled = true;
	
	// If not set, repeated pressing of main & cancel prompts some odd behaviour.
	v_timerBar.set_fraction(0);
} // END - Cancel button clicked.


void TimerButton::setOverlayVisibility(bool p_isVisible){
	 // Set overlay items to be bool:
	 btn_cancel.set_visible(p_isVisible);
	 v_timerBar.set_visible(p_isVisible);
	 
	 // Set main button sensitivity to be the oposite of overlay visibility.
	 btn_main.set_sensitive(!p_isVisible);
} // END - Set overlay Visibility.


bool TimerButton::idleCountdown(){
	if( v_timerBar.get_fraction() > 0 ){
		v_timerBar.set_fraction( v_timerBar.get_fraction() - 0.01);
		return true;
	} else{
		if(!v_cancelled){
			v_signalButtonPressed.emit();
		} // END - If not cancelled.
		btn_cancel_clicked(); // Convenience- act as if cancel was clicked to reset button.
		return false;
	} // END - If timerbar is above 0.
} // END - Idle countdown.


void TimerButton::setButtonLabel(Glib::ustring p_buttonLabel = ""){
	btn_main.set_label(p_buttonLabel);
} // END - Set button label.