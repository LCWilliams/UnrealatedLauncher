#include "include/launcher_settings.h"


	bool LauncherSettings::firstRun = true;
	int	LauncherSettings::settingsFileStatus = 0;

	// General:
	Glib::ustring	LauncherSettings::launcherTheme = "";
	int LauncherSettings::defaultPage = 0;
	bool LauncherSettings::timedButtonsEnabled = true;
	Glib::ustring LauncherSettings::marketAssetDownloadDir = "./market";
	
	
	Glib::ustring LauncherSettings::launcherRepositoryDirectory = "./launcherRepo";
	Glib::ustring LauncherSettings::defaultInstallDirectory = "./engines";
	bool LauncherSettings::rememberGitLogin = false;
	
	// Directories are detected and set independantly from the read/write to file functions: during "SETUP SETTINGS".
	Glib::ustring LauncherSettings::settingsDirectory = "";	// The directory where the settings are stored.
	Glib::ustring LauncherSettings::settingsFile = "";		// The file where the global config settings are stored.
	Glib::ustring LauncherSettings::engineConfigs = "";		// The directory where engine configs are stored.
	Glib::ustring LauncherSettings::projectConfigs = "";	// The directory where project configs are stored.
	
	
// REPOSITORY MANAGER:
	bool LauncherSettings::useSSHAuth = false;
	Glib::ustring LauncherSettings::sshAuthPubKey = "";
	Glib::ustring LauncherSettings::sshAuthPrivKey = "";
	bool LauncherSettings::refreshListsAfterRepoManTask = true;
	bool LauncherSettings::automaticRepoUpdate = false;
	int LauncherSettings::autoRepoUpdateInterval = 30;

	
// LIBRARY:
	bool LauncherSettings::enginesVisible = true;
	bool LauncherSettings::projectsVisible = true;
	bool LauncherSettings::engineProjectPairs = true;
	bool LauncherSettings::verticalView = false;
	Glib::ustring LauncherSettings::editorPath = "/Engine/Binaries/Linux/UE4Editor";