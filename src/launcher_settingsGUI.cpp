#include "include/launcher_settingsGUI.h"

#include "include/launcher_miniGUI.h"

UnrealatedLauncher::LauncherSettingsGUI::LauncherSettingsGUI():
v_windowControls(true),// Has default

txt_themeLabel("Theme: "),
txt_defaultPageLabel("Default Page: "),
txt_general_defaultInstallDir("Default install Dir: "),
txt_general_marketDownloadDir("Market Asset Download Dir: "),
btn_general_timedButtonEnabled("Enable timed buttons"),

// REPO MANAGER
txt_repoMan_directory("Directory: "),
txt_repoMan_authMethod("Authentification method: "),
txt_repoMan_sshPublicDir("Public SSH Key: "),
txt_repoMan_sshPrivDir("Private SSH Key: "),
txt_repoMan_autoRepoUpdInterval("Interval: "),

btn_repoMan_authMethod("Use SSH Authentification"),
btn_repoMan_autoRefreshLists("Auto refresh lists"),
btn_repoMan_autoRepoUpdate("Auto update repository")
{
// Main attachment:
	add(v_mainGrid);

//	set_halign(Gtk::ALIGN_CENTER);
//	set_valign(Gtk::ALIGN_CENTER);
	set_hexpand(true);
	set_vexpand(true);

	v_mainGrid.attach(v_settingsNotebook, 0, 0, 4, 1);
		v_settingsNotebook.set_hexpand(true);
		v_settingsNotebook.set_vexpand(true);
		v_settingsNotebook.set_margin_top(10);
	v_mainGrid.attach(v_windowControls, 3, 1, 1, 1);
	v_mainGrid.attach(v_settingsHelpSpinner, 0, 1, 1, 1);
	v_mainGrid.attach(txt_settingsHelp, 1, 1, 1, 1);
		txt_settingsHelp.set_hexpand();
		txt_settingsHelp.set_halign(Gtk::ALIGN_START);


// GENERAL TAB:
	v_settingsNotebook.append_page(v_settingsGeneral_scrolledWindow, "_General", "General", true);
	//Gtk::Grid 
//	v_settings_general = Gtk::manage(new Gtk::Grid);
	v_settings_general.set_hexpand();
	v_settingsGeneral_scrolledWindow.add(v_settings_general);
	v_settings_general.set_border_width(15);
	v_settings_general.set_hexpand(true);
	v_settings_general.set_column_homogeneous(false);
	v_settings_general.set_row_homogeneous(false);
	v_settings_general.set_row_spacing(10);
	v_settings_general.set_column_spacing(7);
	
	
	v_settings_general.attach(txt_defaultPageLabel, 0, 0, 1, 1);
		txt_defaultPageLabel.set_halign(Gtk::ALIGN_END);
	v_settings_general.attach(btn_defaultPage, 1, 0, 1, 1);
		btn_defaultPage.set_tooltip_text("The default page to be displayed when opening the launcher.");
	v_settings_general.attach(txt_themeLabel, 0, 1, 1, 1);
		txt_themeLabel.set_halign(Gtk::ALIGN_END);
	v_settings_general.attach(btn_theme, 1, 1, 1, 1);
		btn_theme.set_tooltip_text("Which theme the launcher will use.");
	v_settings_general.attach(txt_general_defaultInstallDir, 0, 2, 1, 1);
		txt_general_defaultInstallDir.set_halign(Gtk::ALIGN_END);
	v_settings_general.attach(btn_general_defaultInstallDir, 1, 2, 1, 1);
		btn_general_defaultInstallDir.set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
		btn_general_defaultInstallDir.set_create_folders(true);
		btn_general_defaultInstallDir.set_tooltip_text("The default location for new engine installations.");
	v_settings_general.attach(btn_general_timedButtonEnabled, 0, 3, 2, 1);
		btn_general_timedButtonEnabled.set_tooltip_text("Enable or Disable timed buttons."
		"\nTimed buttons put a delay on some buttons, allowing you to cancel before the task starts."
		"\n\nThis setting does NOT apply to destructive behaviour, which will always have a timer.");
	
	// A timed button to provide an example.
	TimerButton *temp_exampleButon = Gtk::manage(new TimerButton());
		v_settings_general.attach(*temp_exampleButon, 0, 4, 2, 1);
		temp_exampleButon->setButtonLabel("Timed button example");
		temp_exampleButon->set_hexpand(false); // Set hex/vexpand to false otherwise it fills the space and fucks the formatting.
		temp_exampleButon->set_vexpand(false);
		temp_exampleButon->set_tooltip_text("An example of a timed button. Settings must be saved for a true representation of changes.");
	
	v_settings_general.attach(txt_general_marketDownloadDir, 0, 5, 1, 1);
	v_settings_general.attach(btn_general_marketDownloadDir, 1, 5, 1, 1);
		btn_general_marketDownloadDir.set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
		btn_general_marketDownloadDir.set_create_folders(true);
		btn_general_marketDownloadDir.set_tooltip_text("Where downloaded marketplace assets are stored.");
	
	
	
	btn_defaultPage.insert(0, "Library");
	btn_defaultPage.insert(1, "Marketplace");
	btn_defaultPage.insert(2, "Community");
	btn_theme.insert(0, "System theme");
	
	
	
	
// REPOSITORY MANAGER TAB.
	v_settingsNotebook.append_page(v_settingsRepoMan_scrWindow, "_Repository Manager", "RepoMan", true);
	v_settingsRepoMan_scrWindow.add(v_settings_repoMan);
	
	v_settings_repoMan.set_border_width(15);
	v_settings_repoMan.set_hexpand(true);
	v_settings_repoMan.set_column_homogeneous(false);
	v_settings_repoMan.set_row_homogeneous(false);
	v_settings_repoMan.set_row_spacing(10);
	v_settings_repoMan.set_column_spacing(7);
	
	v_settings_repoMan.attach(txt_repoMan_directory, 0, 0, 1, 1);
		v_settings_repoMan.attach(btn_repoman_repoDir, 1, 0, 1, 1);
		btn_repoman_repoDir.set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
		btn_repoman_repoDir.set_create_folders(true);
		btn_repoman_repoDir.set_hexpand(false);
	
//	v_settings_repoMan.attach(txt_repoMan_authMethod, 0, 1, 1, 1);
		v_settings_repoMan.attach(btn_repoMan_authMethod, 0, 1, 2, 1);
		btn_repoMan_authMethod.set_tooltip_text("If enabled, the SSH keys specified below are used during Git operations.");

	v_settings_repoMan.attach(v_repoMan_sshRevealer, 0, 2, 2, 1);
	v_repoMan_sshRevealer.add(v_repoMan_sshGrid);
	v_repoMan_sshGrid.set_row_spacing(10);
	v_repoMan_sshGrid.set_column_spacing(7);
	v_repoMan_sshGrid.set_border_width(3);
	v_repoMan_sshGrid.set_halign(Gtk::ALIGN_FILL);

	v_repoMan_sshGrid.attach(txt_repoMan_sshPublicDir, 0, 2, 1, 1);
	v_repoMan_sshGrid.attach(btn_repoMan_sshPublicDir, 1, 2, 1, 1);
		btn_repoMan_sshPublicDir.set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);
		btn_repoMan_sshPublicDir.set_tooltip_text("The PUBLIC SSH key.");
	
	v_repoMan_sshGrid.attach(txt_repoMan_sshPrivDir, 0, 3, 1, 1);
		v_repoMan_sshGrid.attach(btn_repoMan_sshPrivDir, 1, 3, 1, 1);
		btn_repoMan_sshPrivDir.set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);
		btn_repoMan_sshPrivDir.set_tooltip_text("The PRIVATE SSH key.");
	
	v_repoMan_sshGrid.attach(btn_repoMan_autoRefreshLists, 0, 5, 2, 1);
	v_repoMan_sshGrid.attach(btn_repoMan_autoRepoUpdate, 0, 6, 2, 1);
	v_repoMan_sshGrid.attach(txt_repoMan_autoRepoUpdInterval, 0, 7, 1, 1);
	v_repoMan_sshGrid.attach(btn_repoMan_autoRepoUpdateInterval, 1, 7, 1, 1);
	
	

	if(LauncherSettings::readFromFile() < 0){
		txt_settingsHelp.set_text("Error reading settings file. Defaults will be used.");
		v_logger.warnErrMsg("Error reading settings file during settings GUI initialiser. Defaults will be used instead.");
	} else{
//		txt_settingsHelp.set_text("Updated GUI with values from settings file.");
		updateGuiFromSettings();
	} // END - If read from file has no errors.
	
	
	v_windowControls.v_signalConfirm.connect(sigc::mem_fun(*this, &LauncherSettingsGUI::btn_confirm_clicked));
	v_windowControls.v_signalCancel.connect(sigc::mem_fun(*this, &LauncherSettingsGUI::btn_cancel_clicked));
	v_windowControls.v_signalDefaults.connect(sigc::mem_fun(*this, &LauncherSettingsGUI::btn_defaults_clicked));
	btn_repoMan_authMethod.signal_clicked().connect(sigc::mem_fun(*this, &LauncherSettingsGUI::btn_repoMan_authMethod_changed));
	
} // END - Launcher Settings Gui Constructor.

UnrealatedLauncher::LauncherSettingsGUI::~LauncherSettingsGUI(){
} // END - Launcher Settings GUI Destructor.

void UnrealatedLauncher::LauncherSettingsGUI::updateGuiFromSettings(){

// General:
	btn_theme.set_active(0);
	if( !LauncherSettings::launcherTheme.empty() ) btn_theme.set_active_text( LauncherSettings::launcherTheme );
	btn_defaultPage.set_active( LauncherSettings::defaultPage );
	btn_general_defaultInstallDir.set_uri( "file://" + LauncherUtils::getRealPath(LauncherSettings::defaultInstallDirectory) );
	btn_general_timedButtonEnabled.set_active( LauncherSettings::timedButtonsEnabled );
	btn_general_marketDownloadDir.set_uri("file://" + LauncherUtils::getRealPath(LauncherSettings::marketAssetDownloadDir));
	
// REPO MANAGER:
	// Add "file://" to the start of the strings to prevent the buttons from exploding to disproportionate lengths with the full path (RE: to behave normally).
	btn_repoman_repoDir.set_uri( "file://" + LauncherUtils::getRealPath(LauncherSettings::launcherRepositoryDirectory) );
	btn_repoMan_authMethod.set_active( LauncherSettings::useSSHAuth );
		v_repoMan_sshRevealer.set_reveal_child( LauncherSettings::useSSHAuth );
	btn_repoMan_sshPublicDir.set_uri( "file://" + LauncherSettings::sshAuthPubKey );
	btn_repoMan_sshPrivDir.set_uri( "file://" + LauncherSettings::sshAuthPrivKey );
	btn_repoMan_autoRefreshLists.set_active(LauncherSettings::refreshListsAfterRepoManTask);
	btn_repoMan_autoRepoUpdate.set_active( LauncherSettings::automaticRepoUpdate );
	btn_repoMan_autoRepoUpdateInterval.set_value( LauncherSettings::autoRepoUpdateInterval );
	
} // END - Update GUI from settings.

void UnrealatedLauncher::LauncherSettingsGUI::updateSettingsFromGui(){
	// Copy values from GUI and apply to LauncherSettings::[value].
	
// GENERAL
	
	LauncherSettings::defaultPage = btn_defaultPage.get_active_row_number();
	LauncherSettings::defaultInstallDirectory = LauncherUtils::removeElementFromString( btn_general_defaultInstallDir.get_uri() ,"file://", "");
	LauncherSettings::timedButtonsEnabled = btn_general_timedButtonEnabled.get_active();
	LauncherSettings::marketAssetDownloadDir =  LauncherUtils::removeElementFromString(btn_general_marketDownloadDir.get_uri(), "file://", "" );

// REPO MANAGER
	LauncherSettings::launcherRepositoryDirectory = LauncherUtils::removeElementFromString( btn_repoman_repoDir.get_uri(), "file://", "");
	LauncherSettings::useSSHAuth = btn_repoMan_authMethod.get_active();
	LauncherSettings::sshAuthPubKey =  LauncherUtils::removeElementFromString( btn_repoMan_sshPublicDir.get_uri(), "file://", "");
	LauncherSettings::sshAuthPrivKey =  LauncherUtils::removeElementFromString( btn_repoMan_sshPrivDir.get_uri(), "file://", "");
	LauncherSettings::refreshListsAfterRepoManTask = btn_repoMan_autoRefreshLists.get_active();
	LauncherSettings::automaticRepoUpdate = btn_repoMan_autoRepoUpdate.get_active();
	LauncherSettings::autoRepoUpdateInterval = btn_repoMan_autoRepoUpdateInterval.get_value_as_int();
	
} // END - update settings from gui.


void UnrealatedLauncher::LauncherSettingsGUI::btn_cancel_clicked(){
	v_signalCloseSettingsGUI.emit();
} // END - btn cancel clicked.


void UnrealatedLauncher::LauncherSettingsGUI::btn_confirm_clicked(){
	// Apply GUI settings to LauncherSettings::
	updateSettingsFromGui();
	LauncherSettings::writeSettingsToFile();
	
	v_signalCloseSettingsGUI.emit();
	
} // END - Btn confirm clicked.

void UnrealatedLauncher::LauncherSettingsGUI::btn_defaults_clicked(){
	// Set the settings to be default.
	LauncherSettings::setDefaults();
	// Update the GUI from the values.
	updateGuiFromSettings();
	// Sets the underlaying values back to their saved/original values.
	LauncherSettings::readFromFile();
	
}// END - Button default clicked.

void UnrealatedLauncher::LauncherSettingsGUI::btn_repoMan_authMethod_changed(){
	v_repoMan_sshRevealer.set_reveal_child( btn_repoMan_authMethod.get_active() );
} // END - auth method changed.
