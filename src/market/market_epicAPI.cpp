#include "market/market_epicInterface.h"

using namespace UnrealatedLauncher;

LauncherEpicInterface::LauncherEpicInterface(){
	
} // END - Launcher Epic Interface constructor.


LauncherEpicInterface::~LauncherEpicInterface(){} // Destructor.

void LauncherEpicInterface::setUserCredentials(Glib::ustring p_email, Glib::ustring p_password){
	v_printer.debug("Setting user credentials");
	
	v_username = p_email;
	v_password = p_password;
	
	// Check to see if waiting on login prompt; if true, set bool to false to allow continuation.
	if(v_waitingOnLoginInput){
		v_waitingOnLoginInput = false;
	} // END - If waiting on login prompt.
} // END - Set User Credentials.

Glib::ustring LauncherEpicInterface::getCredentialsUser(){
	return v_username;
} // END - Get User credential - Username

Glib::ustring LauncherEpicInterface::getCredentialsPass(){
	return v_password;
} // END - Get user Credential - Password

bool LauncherEpicInterface::authenticateUser(){
	// Set waiting to true, so future logic behaves accordingly.
	v_waitingOnLoginInput = true;
	v_userCancelledAuth = false; // Re/Set cancelled Auth bool.
	
	// Emit signal to prompt login, then enter loop to wait until login is commenced (or cancelled).
	sig_promptLogin.emit();
	
	while(v_waitingOnLoginInput){
		// Hold thread.
		// If user has cancelled auth login, exit.
		if(v_userCancelledAuth){
			v_printer.debug("Cancelled authentication!");
			return false;
		} // END - If user cancelled auth.
	} // END - Waiting on login input loop.
	
	return true;
} // END - Authenticate user


void LauncherEpicInterface::LEI_TESTFUNC(){
	v_printer.debug("Launcher epic interface test func.");
	
	setUserCredentials("", "");
	
	
//	cURLpp::Cleanup v_cleanup;
	curlpp::Cleanup v_cleanup;
	
//	v_easyCurl.add<CURLOPT_URL>(v_url_loginForm.c_str());
//	v_easyCurl.add<CURLOPT_FOLLOWLOCATION>(1L);
//	
//	v_easyCurl.perform();
	
} // END _ TEST FUNCTION.
