#include "include/launcher_market.h"

using namespace UnrealatedLauncher;

LauncherMarket::LauncherMarket():
v_webView("https://www.unrealengine.com/marketplace"){
	add(v_mainGrid);
	
//	v_mainGrid.add(v_webView);
	v_mainGrid.attach(v_itemScrollWin, 0, 0, 1, 1);
	v_mainGrid.attach(v_fullViewRevealer, 1, 0, 1, 1);

// TESTY STUFF
	v_mainGrid.attach(btn_testStuff, 0, 1, 1, 1);
	btn_testStuff.set_label("TEST");
	btn_testStuff.signal_clicked().connect(sigc::mem_fun(*this, &LauncherMarket::btn_testStuff_clicked));
// END OF TESTY STUFF

	v_fullViewRevealer.add(v_fullViewScrollWin);
	
	v_itemScrollWin.add(v_itemFlowbox);
		v_itemScrollWin.set_vexpand(true);
		v_itemScrollWin.set_hexpand(true);
		
	v_fullViewRevealer.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
	v_fullViewRevealer.set_transition_duration(250);
	
	MarketItemData *temp_data = new MarketItemData;
	LauncherMarketItem *temp_devItem = Gtk::manage(new LauncherMarketItem(temp_data));
	v_itemFlowbox.add( *temp_devItem);
	
		LauncherMarketItem *temp_devItem2 = Gtk::manage(new LauncherMarketItem(temp_data));
	v_itemFlowbox.add( *temp_devItem2);
	
		LauncherMarketItem *temp_devItem3 = Gtk::manage(new LauncherMarketItem(temp_data));
	v_itemFlowbox.add( *temp_devItem3);
	
	
} // END - Constructor.

LauncherMarket::~LauncherMarket(){} // Deconstructor.

void LauncherMarket::btn_testStuff_clicked(){
	
	LauncherEpicInterface temp_epicInterface;
	
	temp_epicInterface.LEI_TESTFUNC();
	
} // END - testy stuff.
