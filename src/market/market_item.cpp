#include "include/launcher_market.h"

using namespace UnrealatedLauncher;

LauncherMarketItem::LauncherMarketItem(MarketItemData* p_itemDataRef) {
	// Set references!  Else watch the world burn!
	ref_itemData = p_itemDataRef;
	
	// Add main grid to bin.
	add(v_mainGrid);
	
	v_mainGrid.set_hexpand(true);
	v_mainGrid.set_vexpand(true);
	v_mainGrid.set_size_request(250,250);
	v_mainGrid.set_halign(Gtk::ALIGN_CENTER);
	v_mainGrid.set_valign(Gtk::ALIGN_CENTER);
	v_mainGrid.set_row_homogeneous(false);
	v_mainGrid.set_column_homogeneous(false);

	v_mainGrid.attach(v_previewImage, 0, 2, 4, 1);
	v_mainGrid.attach(txt_title, 0, 0, 1, 1);
	v_mainGrid.attach(v_ratingBar, 0, 1, 1, 1);
	v_mainGrid.attach(txt_description, 0, 5, 2, 2);
	v_mainGrid.attach(btn_quickDownload, 3, 5, 1, 1);
//	v_mainGrid.attach(child, left, top, wid, hei);

	// Set rating(level) bar up to represent ratings:
	v_ratingBar.set_mode(Gtk::LEVEL_BAR_MODE_DISCRETE);
	v_ratingBar.set_max_value(5);
	v_ratingBar.set_min_value(0);
	v_ratingBar.set_size_request(100, -1);

	v_previewImage.set("/mnt/1TB/Projects/UnrealatedLauncher/ProjectFiles/noPreview.png");
	
	
} // END - Constructor.

LauncherMarketItem::~LauncherMarketItem(){} // Deconstructor.