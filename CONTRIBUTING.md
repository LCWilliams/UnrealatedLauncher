		UNREALATED LAUNCHER NAMING & CODING CONVENTION

		The convention is to be adhered too.
		It is designed to promote readability and understandability over "speed".

# TERMS:
 - SHOULD: Not a requirement, but heavily recommended.
 - MAY: Not a requirement, use discretion.
 - MUST: A requirement.
 - MUST NOT: A prohbitation.
 
# GENERAL:
 - Cammel Case MUST be used everywhere, except where mentioned.
 - All names MUST start lower case, except where mentioned.
 	* Prefixes MUST be lower case & follow cammel case.
 - Shorthand names MUST NOT be used.
 - Opening brackets MUST be on the same line.
 	* Constructor initialisation: `/*ExampleConstructor::ExampleConstructor()*/ {`
 - Indents MUST be set to tabs, 4 collumns per tab.
 - Indents should not be excessive, and used to allow logical reading of code.
 
# NAMESPACES:
 - All name spaces MUST start capitalised.
 - Code MUST NOT use `using namespace`, exceptions:
    * `using namespace UnrealatedLauncher`.
 - Complex namespaces MAY use aliases.
 - Alises MUST NOT be used in headers.
 
# CLASSES:
 - Class names MUST be short, simple and to the point.
 - Class names MUST start capitalised.
 - Members MUST NOT share the same name as a class with different capitalisation.
 
# MEMBERS:

## VARIABLES:
 - All variables MUST be prefixed, except...
 	* Structs: but SHOULD be named with readability in mind.
 	* Global variables: MUST be within a namespace.
 - When listing variables, they MUST be multi-lined and indented.
 	* Exception: Variables that are directly, or closely correlated (Ex: Username, UserPassword).
 - Any text used with markup MUST have its own variable and markup tags added seperately (Localisation).
 - Booleans SHOULD ask or answer a question/statement.
 
### PREFIXES:

 * `v_`	      : general variable (default for anything not covered).
 * `temp_`	      : Temporary variable. Can be used for local variables if preferred.
 * `txt_` 	      : Chars, Strings, Gtk::Label
 * `btn_`	      : Buttons (of any sort).
 * `threadComm_` : variables which are used between threads for inter-thread communication.
 * `menu_`        : Menu containers.
 * `menuItem_`    : A Menu item of any type (link/checkbox/etc).
 * `sig_`		 : Signals.
 * `p_`			 : Function parameter.

## FUNCTIONS:
 - Functions relating to buttons MUST use the buttons full name as a prefix.
 - Function parameters MUST ONLY be prefixed with `p_`.

# MISC:

## LOOPS:
 - Loop indexes MUST be named appropriately: they should summarise what the indexed item is.
 - Index names MUST be prefixed or affixed with `index`.
 
## THREADS:
- Mutexes MUST be prefixed with `threadComm_mutex_` and named appropriately.
- Variables that are used cross-threads SHOULD be prefixed `threadComm_`.

## COMMENTS:
 - All closing brackets MUST have a comment denoting the end of its parent.
	* Syntax: `} // END - functionName/ClassName/loopName/etc`.
 - Comments SHOULD be used to promote readability and section code where possible.
 - Comments SHOULD explain the code where not immediately understandable or complex.
 	* Explainations SHOULD address "What is going on?" AND, where appropriate "Why is it done this way?"
